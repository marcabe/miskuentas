$(document).ready(function () {
	medioatencion = 0;


	$(document).on("click", "#motivo", function () {
		$(this).val();
		$(this).css({"background": "white"});
		$("#msj_motivo").fadeOut(1500);
	});

	$("#perdido").on("click", function () {
		let contenido = `
			<div class="container-fluid">
				<div class="row mt-3">
					<div class="col-md-12 text-center">
						<label><i class="fas fa-comment-dots"></i> Motivo de la perdida</label>
					</div>
					<div class="col-md-8 offset-md-2 text-center">
						<textarea rows="3" class="form-control" id="motivo"></textarea>
						<small id="msj_motivo" class="msj_formulario"></small>
					</div>
				</div>
			</div>`;
		$.confirm({
			title: '<i class="fas fa-robot"></i> Mensaje del sistema',
			content: contenido,
			draggable: true,
			buttons: {
				aceptar: {
					text: '<i class="far fa-thumbs-up"></i> Aceptar', // text for button
					btnClass: 'btn-success success-modal',
					action: function () {
						let avanza = 1;
						if ($("#motivo").val() == '') {
							$("#msj_motivo").html("Debe ingresar el motivo de la perdida");
							$("#motivo").css({"background": "#EFD3D2"});
							$("#msj_motivo").show();
							avanza = 0;
						}
						if (avanza == 1) {
							$.ajax({
								url: '../update_prospecto/' + $("#idprospecto").val(),
								data: {
									motivo: $("#motivo").val(),
									status: 3,
									fechaperdido : $("#hoy").val()
								},
								type: "POST",
								success: function () {
									$.confirm({
										title: '<i class="fas fa-robot"></i> Mensaje del sistema',
										content: 'El prospecto paso a un estatus perdido',
										draggable: true,
										buttons: {
											aceptar: {
												text: '<i class="far fa-thumbs-up"></i> Aceptar', // text for button
												btnClass: 'btn-success success-modal',
												action: function () {
													window.location = "../"+$("#pa").val();
												}
											}
										}
									});
								}
							})
						} else {
							return 0;
						}
					}
				},
				cerrar: {
					text: '<i class="fas fa-times"></i> Cancelar', // text for button
					btnClass: 'btn-danger', // multiple classes.
					action: function () {
					}
				}
			}
		});
	});

	$("#regresar").on("click", function () {
		window.location = "../"+$("#pa").val();
	});

	$("#fechaalerta").on("click", function () {
		$(this).val();
		$(this).css({"background": "white"});
		$("#msj_reagendado").fadeOut(1500);
	});

	$("#comentario").on("click", function () {
		$(this).val();
		$(this).css({"background": "white"});
		$("#msj_comentario").fadeOut(1500);
	});
	$(".opcioncomentario").on("click", function () {
		$(this).val();
		$(this).css({"background": "white"});
		$("#msj_atencion").fadeOut(1500);
	});
	$(".opcioncomentario").on("click", function () {
		medioatencion = $(this).val();
		if (medioatencion == 4) {
			$("#fechacontenedor").show();
		} else {
			$("#fechacontenedor").hide();
		}
	});

	$("#gcomentario").on("click", function () {
		let hoy = $("#hoy").val();
		let medioa = '';
		let avanza = 1;
		let usuario = $("#usu").val();
		let comentario = $("#comentario").val();
		let fechaalerta = '';
		if (comentario == '') {
			$("#msj_comentario").html("El comentario no puede ir vacio");
			$("#msj_comentario").show();
			avanza = 0;
		}
		if (medioatencion == 0) {
			$("#msj_atencion").html("Debe seleccionar el medio de antencion con el que contacto al cliente");
			$("#msj_atencion").show();
			avanza = 0;
		}
		if (medioatencion == 4) {
			fechaalerta = $("#fechaalerta").val();
			if (fechaalerta == '') {
				$("#msj_reagendado").html("Debe ingresar una nueva fecha de alerta");
				$("#msj_reagendado").show();
				avanza = 0;
			}
		}
		console.log(medioatencion);
		switch(medioatencion){
			case "1":
				medioa="WhatsApp";
				break;
			case "2":
				medioa="Teléfono";
				break;
			case "3":
				medioa="E-mail";
				break;
			case "4":
				medioa="Reagendar";
				break;
		}

		if (avanza == 1) {
			$.ajax({
				url: "../Comentarios/insert",
				type: "POST",
				data: {
					idprospecto: $("#idprospecto").val(),
					comentario: comentario,
					medioatencion: medioatencion,
					nombre: $("#nprospecto").val()
				},
				success: function (response) {
					if (response != 0) {
						if (medioatencion == 4) {
							$.ajax({
								url: '../update_prospecto/' + $("#idprospecto").val(),
								data: {
									fechaalerta: fechaalerta,
									horaalerta: $("#horaalerta").val()
								},
								type: 'POST',
								success: function (response) {
									let mensaje =
										`
											<li>
												<div class="block">
													<div class="tags">
														<a href="" class="tag">
															<span>${hoy}</span>
														</a>
													</div>
													<div class="block_content">
														<h2 class="title">
															<a>${usuario}</a>
														</h2>
														<div class="byline">
															<span>Medio de contacto: </span> ${medioa}
														</div>
														<p class="excerpt">${comentario}</p>
													</div>
												</div>
											</li>
										`;
									$("#listamensajes").append(mensaje);
								}
							});
						}else{
							$.ajax({
								url: '../Prospectos/alerta_prospecto/' + $("#idprospecto").val(),
								data: {},
								type: 'POST',
								success: function (response) {
									let mensaje =
										`
											<li>
												<div class="block">
													<div class="tags">
														<a href="" class="tag">
															<span>${hoy}</span>
														</a>
													</div>
													<div class="block_content">
														<h2 class="title">
															<a>${usuario}</a>
														</h2>
														<div class="byline">
															<span>Medio de contacto: </span> ${medioa}
														</div>
														<p class="excerpt">${comentario}</p>
													</div>
												</div>
											</li>
										`;
									$("#listamensajes").append(mensaje);
								}
							});
						}
						$.confirm({
							title: '<i class="fas fa-robot"></i> Mensaje del sistema',
							content: 'Comentario guardado correctamente',
							draggable: true,
							buttons: {
								aceptar: {
									text: '<i class="far fa-thumbs-up"></i> Aceptar', // text for button
									btnClass: 'btn-success success-modal',
									action: function () {
										$("#comentario").val('');
										$("#msj_comentario").hide();
									}
								}
							}
						});
					}
				}
			});

		}
	});

});
