$(document).ready(function () {
	avanzaclave = 1;

	$(".verhistorial").on("click", function () {
		window.location = "../usuarios/historial/"+$(this).attr("data-content");
	});


	$('#tblgeneral').DataTable({
		"language": {
			"url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
		}
	});

	$("#clave").on("click", function () {
		$(this).val();
		$(this).css({"background": "white"});
		$("#msj_clave").fadeOut(1500);
	});

	$("#clave1").on("click", function () {
		$(this).val();
		$(this).css({"background": "white"});
		$("#msj_clave1").fadeOut(1500);
	});


	$(document).on("keyup","#clave", function () {
		if ($(this).val().length < 6) {
			$("#msj_clave").html('La clave debe ser mayor a 6 caracteres');
			$("#msj_clave").show();
		} else {
			$("#msj_clave").html('');
		}
	});
	$(document).on("keyup","#clave1", function () {
		if ($(this).val() != $("#clave").val()) {
			$("#msj_clave1").html('Las claves no coiciden');
			$("#msj_clave1").show();
			avanzaclave = 0;
		} else {
			$("#msj_clave1").html('');
			avanzaclave = 1;
		}
	});

	$(".cambiarclave").on("click", function () {
		let correo = $(this).attr("data-content");
		let contenido = `
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12 text-center">
						Cambio de contraseña
					</div>
				</div>
					<div class="form-group row text-right mt-3">
						<label for="inputPassword" class="col-md-3 col-form-label">Contraseña: </label>
						<div class="col-md-9">
							<div class="input-group input-group-sm">
								<div class="input-group-prepend">
									<div class="input-group-text"><i class="fa fa-lock"></i></div>
								</div>
								<input type="password" class="form-control form-control-sm" id="clave">
							</div>
							<div class="text-left">
								<small id="msj_clave" class="msj_clave"></small>
							</div>
						</div>
					</div>
					<div class="form-group row text-right">
						<label for="inputPassword" class="col-md-3 col-form-label">Confirmar contraseña: </label>
						<div class="col-md-9">
							<div class="input-group input-group-sm">
								<div class="input-group-prepend">
									<div class="input-group-text"><i class="fa fa-lock"></i></div>
								</div>
								<input type="password" class="form-control form-control-sm" id="clave1">
							</div>
							<div class="text-left">
								<small id="msj_clave1" class="msj_clave1"></small>
							</div>
							</div>
						</div>
					</div>`;

		$.confirm({
			title: '<i class="fas fa-robot"></i> Mensaje del sistema',
			content: contenido,
			draggable: true,
			buttons: {
				aceptar: {
					text: '<i class="far fa-thumbs-up"></i> Aceptar', // text for button
					btnClass: 'btn-success success-modal',
					action: function () {
						let avanza = 1;
						let clave = $("#clave").val();
						let clave1 = $("#clave1").val();

						if (clave == '') {
							$("#msj_clave").html("Debe ingresar clave del contacto");
							$("#clave").css({"background": "#EFD3D2"});
							$("#msj_clave").show();
							avanza = 0;
						}

						if (clave1 == '') {
							$("#msj_clave1").html("Debe ingresar clave del contacto");
							$("#clave1").css({"background": "#EFD3D2"});
							$("#msj_clave1").show();
							avanza = 0;
						}
						if (avanza == 1 && avanzaclave == 1) {
							$.ajax({
								url: '../update_usuario/' + correo,
								data: {
									clave: $("#clave").val()
								},
								type: 'POST',
								success: function (response) {
									window.location = "../usuarios/lista";
								}
							});

						} else {
							return 0;
						}
					}
				},
				cerrar: {
					text: '<i class="fas fa-times"></i> Cerrar', // text for button
					btnClass: 'btn-danger', // multiple classes.
				}
			}
		});
	});


	$(".cambiastatus").on("click", function () {
		let correo = $(this).attr("id");
		let status = $(this).attr("data-content");
		if (status == 1) {
			status = 0;
		} else if (status == 0) {
			status = 1;
		}

		$.confirm({
			title: '<i class="fas fa-robot"></i> Mensaje del sistema',
			content: "Cambiaras el status del usuario.<br>¿Deseas continuar?",
			draggable: true,
			buttons: {
				aceptar: {
					text: '<i class="far fa-thumbs-up"></i> Aceptar', // text for button
					btnClass: 'btn-success success-modal',
					action: function () {
						$.ajax({
							url: '../update_usuario/' + correo,
							data: {
								status: status
							},
							type: 'POST',
							success: function (response) {
								window.location = "../usuarios/lista";
							}
						});
					}
				},
				cerrar: {
					text: '<i class="fas fa-times"></i> Cerrar', // text for button
					btnClass: 'btn-danger', // multiple classes.
				}
			}
		});
	});


});
