$(document).ready(function () {
	$(".editar").on("click", function () {
		window.location="../prospectos/edicion/"+$(this).attr("data-content");
	});

	$("#fi").on("click", function () {
		$(this).val();
		$(this).css({"background": "white"});
		$("#msj_fi").fadeOut(1500);
	});

	$("#ff").on("click", function () {
		$(this).val();
		$(this).css({"background": "white"});
		$("#msj_ff").fadeOut(1500);
	});

	$("#consultar").on("click", function () {
		let avanza = 1;
		let fi = $("#fi").val();
		let ff = $("#ff").val();

		let fix = fi.split("-");
		fix = fix[1] + "-" + fix[2] + "-" + fix[0];
		let ffx = ff.split("-");
		ffx = ffx[1] + "-" + ffx[2] + "-" + ffx[0];

		if (fi == '') {
			$("#msj_fi").html("Debe ingresar la fecha de cuando iniciara la prueba el prospecto");
			$("#fi").css({"background": "#EFD3D2"});
			$("#msj_fi").show();
			avanza = 0;
		}

		if (ff == '') {
			$("#msj_ff").html("Debe ingresar la fecha de cuando finalizara la prueba el prospecto");
			$("#ff").css({"background": "#EFD3D2"});
			$("#msj_ff").show();
			avanza = 0;
		}
		if (Date.parse(ffx) < Date.parse(fix)) {
			$("#msj_fi").html("La fecha de inicio no puede ser superior a la fecha final");
			$("#fi").css({"background": "#EFD3D2"});
			$("#msj_fi").show();
			avanza = 0;
		}
		if (avanza == 1) {
			window.location = "filtro/"+fi+"/"+ff+"/"+$("#nodetalle").val();
		} else {
			return 0;
		}
	});

	$('#tblgeneral').DataTable({
		"language": {
			"url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
		}
	});

	$(".detalle").on("click", function () {
		window.location = "../prospectos/" + $(this).attr("id");
	});

	$("#regresar").on("click", function () {
		window.location = "../home";
	});

	$(".eliminar").on("click", function () {
		let idprospecto = $(this).attr("data-content");
		$.confirm({
			title: '<i class="fas fa-robot"></i> Mensaje del sistema',
			content: 'Estas apunto de eliminar un prospecto.<br>¿Deseas continuar?',
			draggable: true,
			buttons: {
				aceptar: {
					text: '<i class="far fa-thumbs-up"></i> Aceptar', // text for button
					btnClass: 'btn-success success-modal',
					action: function () {
						$.ajax({
							url: '../delete_prospectos/' + idprospecto,
							data: {
							},
							type: 'POST',
							success: function (response) {
								$.confirm({
									title: '<i class="fas fa-robot"></i> Mensaje del sistema',
									content: 'El prospecto fue eliminado correctamente',
									draggable: true,
									buttons: {
										aceptar: {
											text: '<i class="far fa-thumbs-up"></i> Aceptar', // text for button
											btnClass: 'btn-success success-modal',
											action: function () {
												window.location = "../detalle_dash/"+$("#nodetalle").val();
											}
										}
									}
								});
							}
						});
					}
				},
				cancelar: {
					text: '<i class="fas fa-times"></i> Cerrar', // text for button
					btnClass: 'btn-danger', // multiple classes.
				}
			}
		});
	});
});
