$(document).ready(function () {
	avanzaclave = 1;
	$("#nombre").on("click", function () {
		$(this).val();
		$(this).css({"background": "white"});
		$("#msj_nombre").fadeOut(1500);
	});


	$("#correo").on("click", function () {
		$(this).val();
		$(this).css({"background": "white"});
		$("#msj_correo").fadeOut(1500);
	});

	$("#clave").on("click", function () {
		$(this).val();
		$(this).css({"background": "white"});
		$("#msj_clave").fadeOut(1500);
	});

	$("#clave1").on("click", function () {
		$(this).val();
		$(this).css({"background": "white"});
		$("#msj_clave1").fadeOut(1500);
	});


	$("#clave").keyup(function () {
		console.log($(this).val().length);
		if ($(this).val().length < 6) {
			$("#msj_clave").html('La clave debe ser mayor a 6 caracteres');
			$("#msj_clave").show();
		} else {
			$("#msj_clave").html('');
		}
	});
	$("#clave1").keyup(function () {
		if ($(this).val() != $("#clave").val()) {
			$("#msj_clave1").html('Las claves no coiciden');
			$("#msj_clave1").show();
			avanzaclave = 0;
		} else {
			$("#msj_clave1").html('');
			avanzaclave = 1;
		}
	});

	$("#guardar").on("click", function () {
		let avanza = 1;
		let nombre = $("#nombre").val();
		let correo = $("#correo").val();
		let clave = $("#clave").val();
		let clave1 = $("#clave1").val();

		let idrol = $("#idrol").val();

		if (nombre == '') {
			$("#msj_nombre").html("Debe ingresar el nombre del prospecto");
			$("#nombre").css({"background": "#EFD3D2"});
			$("#msj_nombre").show();
			avanza = 0;
		}
		if (nombre.length <= 2) {
			$("#msj_nombre").html("El nombre ingresado debe tener 3 caracteres o más");
			$("#nombre").css({"background": "#EFD3D2"});
			$("#msj_nombre").show();
			avanza = 0;
		}
		if (correo == '') {
			$("#msj_correo").html("Debe ingresar el correo del prospecto");
			$("#correo").css({"background": "#EFD3D2"});
			$("#msj_correo").show();
			avanza = 0;
		}

		if (clave == '') {
			$("#msj_clave").html("Debe ingresar clave del contacto");
			$("#clave").css({"background": "#EFD3D2"});
			$("#msj_clave").show();
			avanza = 0;
		}

		if (clave1 == '') {
			$("#msj_clave1").html("Debe ingresar clave del contacto");
			$("#clave1").css({"background": "#EFD3D2"});
			$("#msj_clave1").show();
			avanza = 0;
		}

		if (avanza == 1 && avanzaclave == 1) {
			$.confirm({
				title: '<i class="fas fa-robot"></i> Mensaje del sistema',
				content: '¿Estas a punto de ingresar información, deseas continuar?',
				draggable: true,
				buttons: {
					guardar: {
						text: '<i class="far fa-thumbs-up"></i> Continuar', // text for button
						btnClass: 'btn-success success-modal',
						action: function () {
							$.ajax({
								url: '../add_usuario',
								data: {
									nombre: nombre,
									correo: correo,
									clave: clave,
									status: 1,
									idrol: idrol
								},
								type: 'POST',
								success: function (response) {
									if (response != 0) {
										$.confirm({
											title: '<i class="fas fa-robot"></i> Mensaje del sistema',
											content: '¿Deseas guardar otro registro?',
											draggable: true,
											buttons: {
												aceptar: {
													text: '<i class="far fa-thumbs-up"></i> Aceptar', // text for button
													btnClass: 'btn-success success-modal',
													action: function () {
														$("#nombre").val('');
														$("#correo").val('');
														$("#clave").val('');
														$("#clave1").val('');

														$("#msj_nombre").hide();
														$("#msj_correo").hide();
														$("#msj_clave").hide();
														$("#msj_clave1").hide();
														$("#msj_nombre").css({"backgrpund": "white !important"});
														$("#msj_correo").css({"backgrpund": "white"});
														$("#msj_clave").css({"backgrpund": "white"});
														$("#msj_clave1").css({"backgrpund": "white"});
													}
												},
												cerrar: {
													text: '<i class="fas fa-times"></i> Cancelar', // text for button
													btnClass: 'btn-danger', // multiple classes.
													action: function () {
														window.location = "../usuarios/lista";
													}
												}
											}
										});
									} else {

									}
								}
							});
						}
					},
					cerrar: {
						text: '<i class="fas fa-times"></i> Cerrar', // text for button
						btnClass: 'btn-danger', // multiple classes.
					}
				}
			});
		}
	});
});
