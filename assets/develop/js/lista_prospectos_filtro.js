$(document).ready(function () {

	$(".editar").on("click", function () {
		window.location="../../../../prospectos/edicion/"+$(this).attr("data-content");
	});

	$("#regresar").on("click", function () {
		window.location = "../../../"+$("#nodetalle").val();
	});

	$('#tblgeneral').DataTable({
		"language": {
			"url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
		}
	});

	$(".detalle").on("click", function () {
		window.location = "../../../../prospectos/" + $(this).attr("id");
	});

	$(".eliminar").on("click", function () {
		let idprospecto = $(this).attr("data-content");
		$.confirm({
			title: '<i class="fas fa-robot"></i> Mensaje del sistema',
			content: 'Estas apunto de eliminar un prospecto.<br>¿Deseas continuar?',
			draggable: true,
			buttons: {
				aceptar: {
					text: '<i class="far fa-thumbs-up"></i> Aceptar', // text for button
					btnClass: 'btn-success success-modal',
					action: function () {
						$.ajax({
							url: '../../../../delete_prospectos/' + idprospecto,
							data: {
							},
							type: 'POST',
							success: function (response) {
								$.confirm({
									title: '<i class="fas fa-robot"></i> Mensaje del sistema',
									content: 'El prospecto fue eliminado correctamente',
									draggable: true,
									buttons: {
										aceptar: {
											text: '<i class="far fa-thumbs-up"></i> Aceptar', // text for button
											btnClass: 'btn-success success-modal',
											action: function () {
												window.location = "../"+$("#ff").val()+"/"+$("#nodetalle").val();
											}
										}
									}
								});
							}
						});
					}
				},
				cancelar: {
					text: '<i class="fas fa-times"></i> Cerrar', // text for button
					btnClass: 'btn-danger', // multiple classes.
				}
			}
		});
	});
});
