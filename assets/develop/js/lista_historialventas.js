$(document).ready(function () {

	$(".pventa").each(function (){
		$(this).html(accounting.formatMoney($(this).html()));
	});

	$(".pventasis").each(function (){
		$(this).html(accounting.formatMoney($(this).html()));
	});

	$(".diferencia").each(function (){
		$(this).html(accounting.formatMoney($(this).html()));
	});



	$("#anio").on("change", function (){
		$("#anioencabezado").html($(this).val());
		$("#anioencabezadomenos").html($(this).val()-1);

		$("#mes").val(0);
		$("#ventas").val("");
		$("#aniotitulo").html($(this).val());
		$("#ventas").attr("readonly", false);
		$("#ventas").val('');
		let anio = $("#anio").val();
		$.ajax({
			url: "../get_historial_table/"+anio,
			type: "POST",
			data: {},
			success: function (response){
				let respuesta = JSON.parse(response);
				$("#cuerpoTabla").empty();
				$.each(respuesta, function (i,data){
					let ventas = accounting.formatMoney(data.ventas);
					let ventas_pasado = accounting.formatMoney(data.ventas_pasado);
					let diferencia = accounting.formatMoney(data.ventas - data.ventas_pasado);
					let fila = `
						<tr>
							<td id="nmes${i}">${data.nmes}</td>
							<td id="mesventa${data.nmes}">${ventas}</td>
							<td>${ventas_pasado}</td>
							<td>${diferencia}</td>
						</tr>
					`;
					$("#cuerpoTabla").append(fila);
				});
			}
		});
	});

	$("#editar").on("click", function (){
		$(this).hide();
		$("#guardar").show();
		$("#ventas").attr("readonly", false);
	});

	$("#mes").on("change", function (){
		let mes = $(this).val();
		let anio = $("#anio").val();
		if(mes !=0){
			$.ajax({
				url:"../get_where",
				data:{
					mes: mes,
					anio: anio
				},
				type: "POST",
				success: function (response){
					let respuesta = JSON.parse(response);
					if(respuesta.respuesta==1){
						if(respuesta.historial.ventas==0){
							$("#ventas").val('');
							$("#ventas").attr("readonly", false);
							$("#guardar").show();
							$("#editar").hide();
						}else{
							$("#ventas").val(accounting.formatMoney(respuesta.historial.ventas));
							$("#ventas").attr("readonly", true);
							$("#guardar").hide();
							$("#editar").show();
						}
					}else if(respuesta.respuesta==0){
						$("#ventas").attr("readonly", false);
						$("#ventas").val('');
						$("#guardar").show();
						$("#editar").hide();
					}
				}
			});
		}
	});

	$("#ventas").on("blur", function () {
		$(this).val(accounting.formatMoney($(this).val()));
	});

	$("#guardar").on("click", function () {
		let avanza = 1;
		let anio = $("#anio").val();
		let mes = $("#mes").val();
		let nmes = $("#mes option:selected").text();
		let ventas = accounting.unformat($("#ventas").val());
		if (ventas == '') {
			avanza = 0;
		}
		if (avanza == 1) {
			$.ajax({
				url: "../insert",
				type: "POST",
				data: {
					anio: anio,
					mes: mes,
					nmes: nmes,
					ventas: ventas
				},
				success: function (response) {
					if (response !=0) {
						$("#mesventa"+nmes).html(accounting.formatMoney(ventas));
						$("#ventas").val("");
						$("#mes").val(0);
					}
				}
			});
		}
	});
});
