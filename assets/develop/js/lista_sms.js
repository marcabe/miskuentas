$(document).ready(function () {
	$(".btnsmseditar").on("click", function () {
		$(this).hide();
		let idsms = $(this).attr("data-content");
		$("#textareacontent"+idsms).show();
		$("#smstext"+idsms).hide();
		$("#cancelarsms"+idsms).show();
		$("#guardarsms"+idsms).show();
	});

	$(".btnsmscancelar").on("click", function () {
		$(this).hide();
		let idsms = $(this).attr("data-content");
		$("#textareacontent"+idsms).hide();
		$("#smstext"+idsms).show();
		$("#editarsms"+idsms).show();
		$("#guardarsms"+idsms).hide();
	});

	$(".btnsmsguardar").on("click", function () {
		let idsms = $(this).attr("data-content");
		$.confirm({
			title: '<i class="fas fa-robot"></i> Mensaje del sistema',
			content: '¿Esatas a punto de guardar el sms para este día, deseas continuar?',
			draggable: true,
			buttons: {
				guardar: {
					text: '<i class="far fa-thumbs-up"></i> Continuar', // text for button
					btnClass: 'btn-success success-modal',
					action: function () {
						$.ajax({
							url: '../update_sms/'+idsms,
							data: {
								sms: $("#textarea"+idsms).val()
							},
							type: 'POST',
							success: function (response) {
								if(response!=0){
									$.confirm({
										title: '<i class="fas fa-robot"></i> Mensaje del sistema',
										content: `Mensaje editado correctamente`,
										draggable: true,
										buttons: {
											a: {
												text: '<i class="far fa-thumbs-up"></i> Aceptar', // text for button
												btnClass: 'btn-success success-modal',
												action: function () {
													$("#guardarsms"+idsms).hide();
													$("#smstext"+idsms).show();
													$("#editarsms"+idsms).show();
													$("#guardarsms"+idsms).hide();
													$("#textareacontent"+idsms).hide();
													$("#smstext"+idsms).html($("#textarea"+idsms).val());
												}
											}
										}
									});
								}
							}
						});
					}
				},
				cerrar: {
					text: '<i class="fas fa-times"></i> Cerrar', // text for button
					btnClass: 'btn-danger', // multiple classes.
				}
			}
		});
	});

});
