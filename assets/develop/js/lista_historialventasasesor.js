$(document).ready(function () {

	$(".pventa").each(function () {
		$(this).html(accounting.formatMoney($(this).html()));
	});

	$(".pventasis").each(function () {
		$(this).html(accounting.formatMoney($(this).html()));
	});

	$(".diferencia").each(function () {
		$(this).html(accounting.formatMoney($(this).html()));
	});


	$("#anio").on("change", function () {
		$("#mes").val(0);
		$("#aniotitulo").html($(this).val());
		$("#tblgeneral").show();
		$("#tituloFiltro").show();
		$("#ventas").attr("readonly", false);
		$("#ventas").val('');
		let anio = $("#anio").val();
		let asesor = $("#asesor").val();
		if(asesor!=0){
			$.ajax({
				url: "../Historialventasasesor/get_historial_table/" + anio + "/" + asesor,
				type: "POST",
				data: {},
				success: function (response) {
					let respuesta = JSON.parse(response);
					$("#cuerpoTabla").empty();
					$.each(respuesta, function (i, data) {
						let ventas = accounting.formatMoney(data.ventas);
						let ventas_sis = accounting.formatMoney(data.ventas_sistema);
						let diferencia = accounting.formatMoney(data.ventas - data.ventas_sistema);
						let fila = `
						<tr>
							<td id="nmes${i}">${data.nmes}</td>
							<td id="mesventa${data.nmes}">${ventas}</td>
						</tr>
					`;
						$("#cuerpoTabla").append(fila);
					});
				}
			});
		}

	});

	$("#asesor").on("change", function () {
		$("#tblgeneral").show();
		$("#tituloFiltro").show();
		$("#mes").val(0);
		$("#ventas").attr("readonly", false);
		$("#ventas").val('');
		let anio = $("#anio").val();
		let asesor = $("#asesor").val();
		$("#aniotitulo").html(anio);
		$.ajax({
			url: "../Historialventasasesor/get_historial_table/" + anio + "/" + asesor,
			type: "POST",
			data: {},
			success: function (response) {
				let respuesta = JSON.parse(response);
				$("#cuerpoTabla").empty();
				$.each(respuesta, function (i, data) {
					let ventas = accounting.formatMoney(data.ventas);
					let ventas_sis = accounting.formatMoney(data.ventas_sistema);
					let diferencia = accounting.formatMoney(data.ventas - data.ventas_sistema);
					let fila = `
						<tr>
							<td id="nmes${i}">${data.nmes}</td>
							<td id="mesventa${data.nmes}">${ventas}</td>
						</tr>
					`;
					$("#cuerpoTabla").append(fila);
				});
			}
		});
	});

	$("#editar").on("click", function () {
		$(this).hide();
		$("#guardar").show();
		$("#ventas").attr("readonly", false);
	});

	$("#mes").on("change", function () {
		let mes = $(this).val();
		let anio = $("#anio").val();
		let asesor = $("#asesor").val();
		if (mes != 0 && asesor !=0) {
			$.ajax({
				url: "../Historialventasasesor/get_where",
				data: {
					mes: mes,
					anio: anio,
					asesor: asesor
				},
				type: "POST",
				success: function (response) {
					let respuesta = JSON.parse(response);
					if (respuesta.respuesta == 1) {
						if (respuesta.historial.ventas == 0) {
							$("#ventas").val('');
							$("#ventas").attr("readonly", false);
							$("#guardar").show();
							$("#editar").hide();
						} else {
							$("#ventas").val(accounting.formatMoney(respuesta.historial.ventas));
							$("#ventas").attr("readonly", true);
							$("#guardar").hide();
							$("#editar").show();
						}
					} else if (respuesta.respuesta == 0) {
						$("#ventas").attr("readonly", false);
						$("#ventas").val('');
						$("#guardar").show();
						$("#editar").hide();
					}
				}
			});
		}
	});

	$("#ventas").on("blur", function () {
		$(this).val(accounting.formatMoney($(this).val()));
	});

	$("#guardar").on("click", function () {
		let avanza = 1;
		let anio = $("#anio").val();
		let mes = $("#mes").val();
		let nmes = $("#mes option:selected").text();
		let ventas = accounting.unformat($("#ventas").val());
		let asesor = $("#asesor").val();
		if (ventas == '') {
			avanza = 0;
		}
		if (avanza == 1) {
			$.ajax({
				url: "../Historialventasasesor/insert",
				type: "POST",
				data: {
					anio: anio,
					mes: mes,
					nmes: nmes,
					ventas: ventas,
					asesor: asesor
				},
				success: function (response) {
					if (response != 0) {
						$("#mesventa" + nmes).html(accounting.formatMoney(ventas));
						$("#ventas").val("");
						$("#mes").val(0);
					}
				}
			});
		}
	});
});
