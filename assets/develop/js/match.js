$(document).ready(function () {

	$("#match").on("click", function (){
		let anioa = $("#anioa").val();
		let aniob = $("#aniob").val();
		$.ajax({
			url: "get_match",
			type: "POST",
			data: {
				anioa:anioa,
				aniob:aniob
			},
			success: function (response){
				let data = JSON.parse(response);
				console.log(response);
				$("#grafmatch").dxChart({
					dataSource: data,
					commonSeriesSettings: {
						argumentField: "nmes",
						type: "bar",
						hoverMode: "allArgumentPoints",
						selectionMode: "allArgumentPoints",
						label: {
							visible: true,
							format: {
								type: "fixedPoint",
								precision: 0
							}
						}
					},
					series: [
						{ valueField: "anioa", name: anioa },
						{ valueField: "aniob", name: aniob }
					],
					title: "Comparacion anual de ingresos",
					legend: {
						verticalAlignment: "bottom",
						horizontalAlignment: "center"
					},
					"export": {
						enabled: true
					},
					onPointClick: function (e) {
						e.target.select();
					}
				});
			}
		});
	});

});
