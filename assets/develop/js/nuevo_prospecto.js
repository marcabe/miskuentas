$(document).ready(function () {
	$("#telefono").on("blur", function(){
		$.ajax({
			url: 'get_where',
			data: {
				telefono: "+521 "+$("#telefono").val()
			},
			type: 'POST',
			success: function (response) {
				let respuesta = JSON.parse(response);
				if (respuesta.respuesta == 101) {
					$("#guardar").hide();
					$("#alerta").show();
					$("#msj_alerta").html(`El numero de teléfono ya existe en el sistema, es del prospecto: ${respuesta.prospecto[0].nombre} y esta asignado a ${respuesta.prospecto[0].nasesor.nombre}`);
					$("#alerta").fadeOut(20000);
				}else if(respuesta.respuesta==102){
					$("#alerta").fadeOut(1500);
					$("#guardar").show();
				}
			}
		});
	});


	$("#nombre").on("click", function () {
		$(this).val();
		$(this).css({"background": "white"});
		$("#msj_nombre").fadeOut(1500);
	});

	$("#telefono").on("click", function () {
		$(this).val();
		$(this).css({"background": "white"});
		$("#msj_telefono").fadeOut(1500);
	});

	$("#correo").on("click", function () {
		$(this).val();
		$(this).css({"background": "white"});
		$("#msj_correo").fadeOut(1500);
	});

	$("#pruebai").on("click", function () {
		$(this).val();
		$(this).css({"background": "white"});
		$("#msj_pruebai").fadeOut(1500);
	});

	$("#pruebaf").on("click", function () {
		$(this).val();
		$(this).css({"background": "white"});
		$("#msj_pruebaf").fadeOut(1500);
	});
	//Funcion para que ingrese con enter


	$("#guardar").on("click", function () {
		let avanza = 1;
		let idsms = $("#idsms").val();
		let idcorreo = $("#idcorreo").val();
		let nombre = $("#nombre").val();
		let telefono = $("#telefono").val();
		let correo = $("#correo").val();
		let pruebai = $("#pruebai").val();
		let pruebaf = $("#pruebaf").val();
		let idproducto = $("#productos").val();
		let asesor = '';
		let tipo_asesor = '';
		if ($("#rol").val() == 1) {
			asesor = $("#asesor").val();
			//tipo_asesor = "admin";
		} else {
			asesor = $("#asesor").attr("data-content");
		}
		let fix = pruebai.split("-");
		fix = fix[1] + "-" + fix[2] + "-" + fix[0];
		let ffx = pruebaf.split("-");
		ffx = ffx[1] + "-" + ffx[2] + "-" + ffx[0];
		if (nombre == '') {
			$("#msj_nombre").html("Debe ingresar el nombre del prospecto");
			$("#nombre").css({"background": "#EFD3D2"});
			$("#msj_nombre").show();
			avanza = 0;
		}
		if (nombre.length <= 2) {
			$("#msj_nombre").html("El nombre ingresado debe tener 3 caracteres o más");
			$("#nombre").css({"background": "#EFD3D2"});
			$("#msj_nombre").show();
			avanza = 0;
		}
		if (telefono == '') {
			$("#msj_telefono").html("Debe ingresar el telefono del prospecto");
			$("#telefono").css({"background": "#EFD3D2"});
			$("#msj_telefono").show();
			avanza = 0;
		}

		if (telefono.length <= 9) {
			$("#msj_telefono").html("El telefono debe tenerl amenos 10 digitos");
			$("#telefono").css({"background": "#EFD3D2"});
			$("#msj_telefono").show();
			avanza = 0;
		}

		if (correo == '') {
			$("#msj_correo").html("Debe ingresar el correo del prospecto");
			$("#correo").css({"background": "#EFD3D2"});
			$("#msj_correo").show();
			avanza = 0;
		}

		if (pruebai == '') {
			$("#msj_pruebai").html("Debe ingresar la fecha de cuando iniciara la prueba el prospecto");
			$("#pruebai").css({"background": "#EFD3D2"});
			$("#msj_pruebai").show();
			avanza = 0;
		}

		if (pruebaf == '') {
			$("#msj_pruebaf").html("Debe ingresar la fecha de cuando finalizara la prueba el prospecto");
			$("#pruebaf").css({"background": "#EFD3D2"});
			$("#msj_pruebaf").show();
			avanza = 0;
		}

		if (Date.parse(ffx) < Date.parse(fix)) {
			$("#msj_pruebai").html("La fecha de inicio no puede ser superior a la fecha final");
			$("#pruebai").css({"background": "#EFD3D2"});
			$("#msj_pruebai").show();
			avanza = 0;
		}

		if (avanza == 1) {
			$.confirm({
				title: '<i class="fas fa-robot"></i> Mensaje del sistema',
				content: '¿Estas a punto de ingresar información, deseas continuar?',
				draggable: true,
				buttons: {
					guardar: {
						text: '<i class="far fa-thumbs-up"></i> Continuar', // text for button
						btnClass: 'btn-success success-modal',
						action: function () {
							$.ajax({
								url: '../add_prospecto',
								data: {
									nombre: nombre,
									telefono: "+521 " + telefono,
									correo: correo,
									pruebai: pruebai,
									pruebaf: pruebaf,
									idproducto: idproducto,
									asesor: asesor,
									status: 1,
									idsms: idsms,
									idcorreo: idcorreo
								},
								type: 'POST',
								success: function (response) {
									respuesta = JSON.parse(response);
									if (respuesta.respuesta == 1) {
										$.confirm({
											title: '<i class="fas fa-robot"></i> Mensaje del sistema',
											content: '¿Deseas guardar otro registro?',
											draggable: true,
											buttons: {
												aceptar: {
													text: '<i class="far fa-thumbs-up"></i> Aceptar', // text for button
													btnClass: 'btn-success success-modal',
													action: function () {
														$("#nombre").val('');
														$("#telefono").val('');
														$("#correo").val('');
														$("#productos").val(1);
														$("#pruebai").val('');
														$("#pruebaf").val('');

														$("#msj_nombre").hide();
														$("#msj_telefono").hide();
														$("#msj_correo").hide();
														$("#msj_pruebai").hide();
														$("#msj_pruebaf").hide();

														$("#msj_nombre").css({"backgrpund": "white !important"});
														$("#msj_telefono").css({"backgrpund": "white"});
														$("#msj_correo").css({"backgrpund": "white"});
														$("#msj_pruebai").css({"backgrpund": "white"});
														$("#msj_pruebaf").css({"backgrpund": "white"});

													}
												},
												cerrar: {
													text: '<i class="fas fa-times"></i> Cancelar', // text for button
													btnClass: 'btn-danger', // multiple classes.
													action: function () {
														window.location = "../prospectos/lista";
													}
												}
											}
										});
									} else if (respuesta.respuesta == 101) {
										$.confirm({
											title: '<i class="fas fa-robot"></i> Mensaje del sistema',
											content: `El prospecto ya existe y esta asignado al asesor ${respuesta.asesor}`,
											draggable: true,
											buttons: {
												a: {
													text: '<i class="far fa-thumbs-up"></i> Aceptar', // text for button
													btnClass: 'btn-success success-modal',
													action: function () {
													}
												}
											}
										});
									}
								}
							});
						}
					},
					cerrar: {
						text: '<i class="fas fa-times"></i> Cerrar', // text for button
						btnClass: 'btn-danger', // multiple classes.
					}
				}
			});
		}
	});
});
