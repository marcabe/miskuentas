$(document).ready(function () {

	$("#match").on("click", function (){
		let anio = $("#anio").val();
		let asesora = $("#asesora").val();
		let nasesora = $("#asesora option:selected").html();
		let asesorb = $("#asesorb").val();
		let nasesorb = $("#asesorb option:selected").html();

		$.ajax({
			url: "get_match",
			type: "POST",
			data: {
				anio:anio,
				asesora:asesora,
				asesorb:asesorb
			},
			success: function (response){
				let data = JSON.parse(response);
				console.log(response);
				$("#grafmatch").dxChart({
					dataSource: data,
					commonSeriesSettings: {
						argumentField: "nmes",
						type: "bar",
						hoverMode: "allArgumentPoints",
						selectionMode: "allArgumentPoints",
						label: {
							visible: true,
							format: {
								type: "fixedPoint",
								precision: 0
							}
						}
					},
					series: [
						{ valueField: "asesora", name: nasesora },
						{ valueField: "asesorb", name: nasesorb }
					],
					title: "Comparacion anual de ingresos entre asesores",
					legend: {
						verticalAlignment: "bottom",
						horizontalAlignment: "center"
					},
					"export": {
						enabled: true
					},
					onPointClick: function (e) {
						e.target.select();
					}
				});
			}
		});
	});

});
