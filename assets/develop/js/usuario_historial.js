$(document).ready(function () {

	$("#fi").on("click", function () {
		$(this).val();
		$(this).css({"background": "white"});
		$("#msj_fi").fadeOut(1500);
	});

	$("#ff").on("click", function () {
		$(this).val();
		$(this).css({"background": "white"});
		$("#msj_ff").fadeOut(1500);
	});

	$("#consultar").on("click", function () {
		let avanza = 1;
		let fi = $("#fi").val();
		let ff = $("#ff").val();

		let fix = fi.split("-");
		fix = fix[1] + "-" + fix[2] + "-" + fix[0];
		let ffx = ff.split("-");
		ffx = ffx[1] + "-" + ffx[2] + "-" + ffx[0];

		if (fi == '') {
			$("#msj_fi").html("Debe ingresar la fecha de cuando iniciara la prueba el prospecto");
			$("#fi").css({"background": "#EFD3D2"});
			$("#msj_fi").show();
			avanza = 0;
		}

		if (ff == '') {
			$("#msj_ff").html("Debe ingresar la fecha de cuando finalizara la prueba el prospecto");
			$("#ff").css({"background": "#EFD3D2"});
			$("#msj_ff").show();
			avanza = 0;
		}
		if (Date.parse(ffx) < Date.parse(fix)) {
			$("#msj_fi").html("La fecha de inicio no puede ser superior a la fecha final");
			$("#fi").css({"background": "#EFD3D2"});
			$("#msj_fi").show();
			avanza = 0;
		}
		if (avanza == 1) {
			window.location = "filtro/"+fi+"/"+ff+"/"+$("#correo").val();
		} else {
			return 0;
		}
	});
});
