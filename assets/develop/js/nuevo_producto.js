$(document).ready(function () {
	$("#producto").on("click", function () {
		$(this).val();
		$(this).css({"background": "white"});
		$("#msj_producto").fadeOut(1500);
	});

	$("#guardar").on("click", function () {
		let avanza = 1;
		let producto = $("#producto").val();

		if (producto == '') {
			$("#msj_producto").html("Debe ingresar el nombre del producto");
			$("#producto").css({"background": "#EFD3D2"});
			$("#msj_producto").show();
			avanza = 0;
		}

		if (avanza == 1) {
			$.confirm({
				title: '<i class="fas fa-robot"></i> Mensaje del sistema',
				content: '¿Estas a punto de ingresar información, deseas continuar?',
				draggable: true,
				buttons: {
					guardar: {
						text: '<i class="far fa-thumbs-up"></i> Continuar', // text for button
						btnClass: 'btn-success success-modal',
						action: function () {
							$.ajax({
								url: '../add_producto',
								data: {
									nproducto: producto,
									status: 1
								},
								type: 'POST',
								success: function (response) {
									if (response != 0) {
										$.confirm({
											title: '<i class="fas fa-robot"></i> Mensaje del sistema',
											content: '¿Deseas guardar otro registro?',
											draggable: true,
											buttons: {
												aceptar: {
													text: '<i class="far fa-thumbs-up"></i> Aceptar', // text for button
													btnClass: 'btn-success success-modal',
													action: function () {
														$("#producto").val('');

														$("#msj_producto").hide();

														$("#msj_producto").css({"backgrpund": "white !important"});

													}
												},
												cerrar: {
													text: '<i class="fas fa-times"></i> Cancelar', // text for button
													btnClass: 'btn-danger', // multiple classes.
													action: function () {
														window.location = "../productos/lista";
													}
												}
											}
										});
									} else {

									}
								}
							});
						}
					},
					cerrar: {
						text: '<i class="fas fa-times"></i> Cerrar', // text for button
						btnClass: 'btn-danger', // multiple classes.
					}
				}
			});
		}
	});
});
