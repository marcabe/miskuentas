$(document).ready(function () {
	$('#tblgeneral').DataTable({
		"language": {
			"url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
		}
	});

	$(".cambiastatus").on("click", function () {
		let idproducto = $(this).attr("id");
		let status = $(this).attr("data-content");
		if(status == 1){
			status = 0;
		}else if(status == 0){
			status = 1
		}


		$.confirm({
			title: '<i class="fas fa-robot"></i> Mensaje del sistema',
			content: "Cambiaras el status del producto.<br>¿Deseas continuar?",
			draggable: true,
			buttons: {
				aceptar: {
					text: '<i class="far fa-thumbs-up"></i> Aceptar', // text for button
					btnClass: 'btn-success success-modal',
					action: function () {
						$.ajax({
							url: '../update_producto/' + idproducto,
							data: {
								status: status
							},
							type: 'POST',
							success: function (response) {

							}
						});
					}
				},
				cerrar: {
					text: '<i class="fas fa-times"></i> Cerrar', // text for button
					btnClass: 'btn-danger', // multiple classes.
				}
			}
		});
	});


});
