$(document).ready(function () {
	let contadorFiles=0;
	
	$("#guardar").on("click", function () {
		let data = new FormData();
		let idcorreo = $("#idcorreo").val();
		$.ajax({
			url: "../Correos/update/" + idcorreo,
			data: {
				correo: $("#editor-one").html(),
				asunto: $("#asunto").val()
			},
			type: "POST",
			success:function (response) {
				let data = JSON.parse(response);
				if(data.respuesta==101){
					var archivos = document.getElementById("imgcorreo");
					var archivo = archivos.files;
					var archivos = new FormData();
					for(i=0; i<archivo.length; i++){
						archivos.append('archivo'+i,archivo[i]);
					}
					archivos.append('idcorreo', idcorreo);

					console.log(archivos);
					$.ajax({
						url:'../Imgc/insert', //Url a donde la enviaremos
						type:'POST',
						contentType:false,
						data:archivos,
						processData:false,
						cache:false,
						success:function (response) {
							$("#imgcorreo").val('');
							$.confirm({
								title: '<i class="fas fa-robot"></i> Mensaje del sistema',
								content: `Mensaje del correo editado correctamente`,
								draggable: true,
								buttons: {
									a: {
										text: '<i class="far fa-thumbs-up"></i> Aceptar', // text for button
										btnClass: 'btn-success success-modal',
										action: function () {
											window.location = "lista";
										}
									}
								}
							});
						}

					});
				}
			},
			xhr: function () {
				var xhr = $.ajaxSettings.xhr();
				xhr.onloadstart = function (e) {
					$("#cargando").show();
				};
				xhr.onloadend = function (e) {
					$("#cargando").fadeOut(0);
				}
				return xhr;
			}
		});
	});

	$("#cerrar").on("click", function () {

	});


	$(".editarcorreo").on("click", function () {
		let idcorreo = $(this).attr("data-content");
		$("#listaimg").empty();
		$.ajax({
			url: "../Correos/get/"+idcorreo,
			data:{},
			type:"POST",
			success:function (response) {
				$("#listaimg").empty();
				let data = JSON.parse(response);
				if(data.respuesta!=0){
					console.log(data);
					$("#cajacorreo").show();
					$("#editor-one").html(data.correo[0].correo);
					$("#asunto").val(data.correo[0].asunto);
					$("#idcorreo").val(data.correo[0].idcorreo);
					$.each( data.imgcorreos, function( key, value ) {
						console.log(value);
					  	let cuerpoimg = `
					  	<div class="col-md-4 mt-3" id="cajaimg${value.idimg}">
					  		<div class="col-md-12 file-name text-right">
						  		<i class="fa fa-trash-alt eliminar" style="cursor:pointer;color:red !important; border-color:red !important" id="${value.idimg}"></i></label>
							</div>
					  		<a href="#" class="atch-thumb">
								<img src="../${value.ruta}" alt="img" width="100%">
							</a>
						  	
						</div>`;
						$("#listaimg").append(cuerpoimg);
					});
				}
			},
			xhr: function () {
				var xhr = $.ajaxSettings.xhr();
				xhr.onloadstart = function (e) {
					$("#cargando").show();
				};
				xhr.onloadend = function (e) {
					$("#cargando").fadeOut(0);
				}
				return xhr;
			}
		});
	});

	$(document).on("click", ".eliminar", function(e){
		let idimg = $(this).attr("id");
		$.ajax({
			url: "../Imgc/delete",
			data:{
				idimg:idimg
			},
			type:"POST",
			success: function(response){
				let respuesta = JSON.parse(response);
				if(respuesta.respuesta ==101){
					$("#cajaimg"+idimg).empty();
				}
			}
		});
	});

});
