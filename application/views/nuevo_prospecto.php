<?php require 'layaout/head.php'; ?>
<!--  menu -->
<?php require 'layaout/menu.php'; ?>
<!-- /menu -->

<!-- top navigation -->
<?php require 'layaout/cabecera.php'; ?>
<!-- /top navigation -->

<div class="right_col" role="main">
	<div class="">

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12  ">
				<div class="x_panel">
					<div class="x_title">
						<h2>Modulo Prospectos</h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content tituloSistema">
						Nuevo prospecto
					</div>
					<div class="container mt-5">
						<div class="row">
							<div class="col-md-12 text-right">
								<button class="btn btn-success btn-sm text-center" id="guardar"><i
											class="far fa-save"></i> Guardar
								</button>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 text-left">
								<div class="alert alert-danger alert-dismissible " role="alert" id="alerta" style="display: none;">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
									</button>
									<strong><i class="fas fa-exclamation-circle"></i></strong> <b id="msj_alerta"></b>
								</div>
							</div>
						</div>
						<div class="col-md-6 offset-md-2">

							<div class="form-group row text-right">
								<label for="inputPassword" class="col-md-3 col-form-label">Nombre(s): </label>
								<div class="col-md-9">
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="far fa-user"></i></div>
										</div>
										<input type="text" class="form-control form-control-sm" id="nombre"
											   placeholder="Ingresa el nombre">
									</div>
									<div class="text-left">
										<small id="msj_nombre" class="msj_formulario"></small>
									</div>
								</div>
							</div>

							<div class="form-group row text-right">
								<label for="inputPassword" class="col-md-3 col-form-label">Celular: </label>
								<div class="col-md-9">
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="fa fa-phone"></i></div>
										</div>
										<input class="form-control col-md-2" value="+521" readonly>
										<input type="text" class="form-control form-control-sm" id="telefono"
											   data-inputmask="'mask' : '9999999999'">
									</div>
									<div class="text-left">
										<small id="msj_telefono" class="msj_formulario"></small>
									</div>
								</div>
							</div>

							<div class="form-group row text-right">
								<label for="inputPassword" class="col-md-3 col-form-label">Correo: </label>
								<div class="col-md-9">
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="far fa-envelope"></i></div>
										</div>
										<input type="text" class="form-control form-control-sm" id="correo"
											   placeholder="Ingresa el correo">
									</div>
									<div class="text-left">
										<small id="msj_correo" class="msj_formulario"></small>
									</div>
								</div>
							</div>


							<div class="form-group row text-right">
								<label for="inputPassword" class="col-md-3 col-form-label">Producto: </label>
								<div class="col-md-9">
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="fas fa-box-open"></i></div>
										</div>
										<select class="form-control form-control-sm" id="productos">
											<?php foreach ($productos as $p) { ?>
												<option value="<?php echo $p->idproducto; ?>"><?php echo $p->nproducto; ?></option>
											<?php } ?>
										</select>
									</div>
								</div>
							</div>

							<div class="form-group row text-right">
								<label for="inputPassword" class="col-md-3 col-form-label">Asesor asignado: </label>
								<div class="col-md-9">
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="far fa-user"></i></div>
										</div>
										<?php if ($this->session->userdata('rol') == 1) { ?>
											<select class="form-control form-control-sm" id="asesor">
												<option value="<?php echo $this->session->userdata('correo'); ?>"><?php echo $this->session->userdata('usuario'); ?></option>
												<?php foreach ($asesores as $a) { ?>
													<option value="<?php echo $a->correo; ?>"><?php echo $a->nombre; ?></option>
												<?php } ?>
											</select>
										<?php } elseif ($this->session->userdata('rol') == 2) { ?>
											<input readonly type="text"
												   data-content="<?php echo $this->session->userdata('correo'); ?>"
												   class="form-control form-control-sm" id="asesor"
												   value="<?php echo $this->session->userdata('usuario'); ?>">
										<?php } ?>
									</div>
								</div>
							</div>


							<div class="form-group row text-right">
								<label for="inputPassword" class="col-md-3 col-form-label">Inicia prueba: </label>
								<div class="col-md-9">
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
										</div>
										<input type="date" class="form-control form-control-sm" id="pruebai">
									</div>
									<div class="text-left">
										<small id="msj_pruebai" class="msj_formulario"></small>
									</div>
								</div>
							</div>


							<div class="form-group row text-right">
								<label for="inputPassword" class="col-md-3 col-form-label">Finaliza prueba: </label>
								<div class="col-md-9">
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
										</div>
										<input type="date" class="form-control form-control-sm" id="pruebaf">
									</div>
									<div class="text-left">
										<small id="msj_pruebaf" class="msj_formulario"></small>
									</div>
								</div>
							</div>

							<div class="form-group row text-right">
								<label for="inputPassword" class="col-md-3 col-form-label">Mensaje que le corresponde: </label>
								<div class="col-md-9">
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="fas fa-sms"></i></div>
										</div>
										<select class="form-control form-control-sm" id="idsms">
											<option value=1>Día 1</option>
											<option value=2>Día 2</option>
											<option value=3>Día 3</option>
											<option value=4>Día 4</option>
											<option value=5>Día 5</option>
											<option value=6>Día 6</option>
											<option value=7>Día 7</option>
											<option value=8>Día 8</option>
											<option value=9>Día 9</option>
											<option value=10>Día 10</option>
											<option value=11>Día 11</option>
											<option value=12>Día 12</option>
											<option value=13>Día 13</option>
											<option value=14>Día 14</option>
											<option value=15>Día 15</option>
											<option value="100">Sin mensaje</option>
										</select>
									</div>
								</div>
							</div>

							<div class="form-group row text-right">
								<label class="col-md-3 col-form-label">Correo que le corresponde: </label>
								<div class="col-md-9">
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="fas fa-envelope-open-text"></i></div>
										</div>
										<select class="form-control form-control-sm" id="idcorreo">
											<option value=1>Día 1</option>
											<option value=2>Día 2</option>
											<option value=3>Día 3</option>
											<option value=4>Día 4</option>
											<option value=5>Día 5</option>
											<option value=6>Día 6</option>
											<option value=7>Día 7</option>
											<option value=8>Día 8</option>
											<option value=9>Día 9</option>
											<option value=10>Día 10</option>
											<option value=11>Día 11</option>
											<option value=12>Día 12</option>
											<option value=13>Día 13</option>
											<option value=14>Día 14</option>
											<option value=15>Día 15</option>
											<option value="100">Sin correo</option>
										</select>
									</div>
								</div>
							</div>


							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->
<script src="<?php echo base_url(); ?>assets/develop/js/nuevo_prospecto.js"></script>

<?php require 'layaout/footer.php'; ?>
