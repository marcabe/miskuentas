<?php require 'layaout/head.php'; ?>
<!--  menu -->
<?php require 'layaout/menu.php'; ?>
<!-- /menu -->

<!-- top navigation -->
<?php require 'layaout/cabecera.php'; ?>
<!-- /top navigation -->

<!-- page content -->
<style>

</style>
<div class="right_col" role="main">
	<div class="">

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12  ">
				<div class="x_panel">
					<div class="x_title">
						<h2>Modulo Prospectos</h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content tituloSistema">
						Lista de prospectos
						<div class="row mt-2">
							<div class="col-md-4">
								<div class="form-group">
									<label>Fecha de inicio:</label>
									<div class="input-group mb-2">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
										</div>
										<input type="date" class="form-control form-control-sm" id="fi" value="<?php echo $fecha;?>">
									</div>
									<div class="text-left">
										<small id="msj_fi" class="msj_formulario"></small>
									</div>
								</div>
							</div>
							<div class="col-md-8 mt-4 text-right">
								<button class="btn btn-success btn-sm" id="consultar"><i class="fas fa-search"></i>
									Consultar
								</button>
							</div>
						</div>

						<div class="row mt-5">
							<div class="col-md-12">
								<table id="tblgeneral" class="table table-bordered">
									<thead>
									<tr>
										<td><i class="far fa-id-card"></i> Prospecto</td>
										<td><i class="fa fa-phone"></i> Celular</td>
										<td><i class="far fa-envelope"></i> Mensaje</td>
										<td><i class="fas fa-box-open"></i> Fecha</td>
										<td><i class="far fa-user"></i> Horal</td>
										<td><i class="far fa-user"></i> Status</td>

									</tr>
									</thead>
									<tbody>
									<?php foreach ($registros as $r){ ?>
										<tr>
											<td><?php echo $r->prospecto->nombre; ?></td>
											<td><?php echo $r->prospecto->telefono; ?></td>
											<td><?php echo $r->sms->sms; ?></td>
											<td><?php echo $r->fecha; ?></td>
											<td><?php echo $r->hora; ?></td>
											<td><?php echo $r->statusenvio; ?></td>
										</tr>
									<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->
<script src="<?php echo base_url(); ?>assets/develop/js/lista_registrosms.js"></script>
<?php require 'layaout/footer.php'; ?>
