<?php require 'layaout/head.php'; ?>
<!--  menu -->
<?php require 'layaout/menu.php'; ?>
<!-- /menu -->

<!-- top navigation -->
<?php require 'layaout/cabecera.php'; ?>
<!-- /top navigation -->

<!-- page content -->
<style>

</style>
<div class="right_col" role="main">
	<div class="">

		<div class="row">
			<div class="col-md-8">
				<div class="page-title">
					<div class="title_left">
						<h3>Dashboard Principal</h3>
					</div>
				</div>
			</div>

			<div class="col-md-4 text-left">
				<div class="input-group input-group-sm">
					<div class="input-group-prepend">
						<div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
					</div>
					<select class="form-control form-control-sm" id="mes">
						<option value="01">Enero</option>
						<option value="02">Febrero</option>
						<option value="03">Marzo</option>
						<option value="04">Abril</option>
						<option value="05">Mayo</option>
						<option value="06">Junio</option>
						<option value="07">Julio</option>
						<option value="08">Agosto</option>
						<option value="09">Septiembre</option>
						<option value="10">Octubre</option>
						<option value="11">Noviembre</option>
						<option value="12">Diciembre</option>
					</select>
				</div>
			</div>
		</div>

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12">
				<div class="">
					<div class="x_content">
						<div class="row">
							<div class="col-md-2">
								<div class="tile-stats text-center">
									<div class="count text-left">
										<i class="fas fa-id-card"></i> <label
											id="totalprospectos"><?php echo $totalprospectos; ?></label>
									</div>
									<h6 class="mt-1">Prosp. del mes</h6>
									<p class="text-center">
										<button class="btn btn-primary btn-sm" id="1"><i
												class="fas fa-info-circle"></i> Ver detalle
										</button>
									</p>
								</div>
							</div>

							<div class="col-md-2">
								<div class="tile-stats text-center">
									<div class="count text-left">
										<i class="fas fa-trophy"></i> <label
											id="ganados"><?php echo $totalganados; ?></label>
									</div>
									<h6 class="mt-1">Ganados</h6>
									<p class="text-center">
										<button class="btn btn-primary btn-sm" id="3"><i
												class="fas fa-info-circle"></i> Ver detalle
										</button>
									</p>
								</div>
							</div>
							<div class="col-md-2">
								<div class="tile-stats text-center">
									<div class="count text-left">
										<i class="fas fa-heart-broken"></i> <label
											id="perdidos"><?php echo $totalperdidos; ?></label>
									</div>
									<h6 class="mt-1">Perdidos</h6>
									<p class="text-center">
										<button class="btn btn-primary btn-sm" id="6"><i
												class="fas fa-info-circle"></i> Ver detalle
										</button>
									</p>
								</div>
							</div>
							<div class="col-md-2">
								<div class="tile-stats text-center">
									<div class="count text-left">
										<i class="fas fa-sort-amount-down" style="color: red !important;"></i>
										<label id="vencer"><?php echo $totalvencer; ?></label>
									</div>
									<h6 class="mt-1">Prosp. por vencer</h6>
									<p class="text-center">
										<button class="btn btn-primary btn-sm" id="4"><i
												class="fas fa-info-circle"></i> Ver detalle
										</button>
									</p>
								</div>
							</div>
							<div class="col-md-2">
								<div class="tile-stats text-center">
									<div class="count text-left">
										<i class="fas fa-ghost" style="color: red !important;"></i> <label
											id="vencidos"><?php echo $totalvencidos; ?></label>
									</div>
									<h6 class="mt-1">Vencidos</h6>
									<p class="text-center">
										<button class="btn btn-primary btn-sm" id="5"><i
												class="fas fa-info-circle"></i> Ver detalle
										</button>
									</p>
								</div>
							</div>


							<div class="col-md-2">
								<div class="tile-stats text-center">
									<div class="count text-left">
										<i class="fas fa-clock" style="color: red !important;"></i>
										<label><?php echo $totalalertas; ?></label>
									</div>
									<h6 class="mt-1">Pendientes</h6>
									<p class="text-center">
										<button class="btn btn-primary btn-sm" id="2"><i
												class="fas fa-info-circle"></i> Ver detalle
										</button>
									</p>
								</div>
							</div>
						</div>


						<div class="row">
							<div class="col-md-6">
								<div class="x_panel">
									<div class="x_title">
										<h2>Estatus de prospectos del mes</h2>
										<div class="clearfix"></div>
									</div>
									<div class="x_content">
										<div class="row">
											<div class="col-md-12">
												<div id="tprospectos"></div>
											</div>
										</div>
										<div class="divider"></div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="x_panel">
									<div class="x_title">
										<h2>Productos vendidos del mes</h2>
										<div class="clearfix"></div>
									</div>
									<div class="x_content">
										<div class="row">
											<div class="col-md-12">
												<div id="pvendidos"></div>
											</div>
										</div>
										<div class="divider"></div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
</div>
<!-- /page content -->
<?php require 'layaout/footer.php'; ?>
<script>
	$(document).ready(function () {


		$("#msjalerta").html("<?php echo $totalalertas;?>");
		$(".btn-primary").on("click", function () {
			window.location = "detalle_dash/" + $(this).attr("id");
		});

		$("#mes").val("<?php echo $mes;?>");


		console.log(<?php echo $graficaProductos;?>);
		console.log("------------");
		$("#pvendidos").dxChart({
			dataSource: <?php echo $graficaProductos;?>,
			palette: "soft",
			title: {
				text: "Prod. vendidos del mes",
				subtitle: ""
			},
			commonSeriesSettings: {
				type: "bar",
				valueField: "number",
				argumentField: "producto",
				ignoreEmptyPoints: true,
				label: {
					visible: true,
					format: {
						type: "fixedPoint",
						precision: 0
					}
				}
			},
			seriesTemplate: {
				nameField: "producto"
			},
			tooltip: {
				enabled: true,
				location: "edge",
				customizeTooltip: function (arg) {
					return {
						text: arg.seriesName
					};
				}
			},
			legend: {
				visible: false
			}
		});

		$("#tprospectos").dxPieChart({
			palette: "bright",
			dataSource: <?php echo $graficaStatus;?>,
			title: {
				text: "Status de los propsectos del mes",
				subtitle: ""
			},
			legend: {
				orientation: "horizontal",
				itemTextPosition: "right",
				horizontalAlignment: "center",
				verticalAlignment: "bottom",
				columnCount: 4
			},
			onPointClick: function (e) {
				var point = e.target;
				toggleVisibility(point);
			},
			onLegendClick: function (e) {
				var arg = e.target;
				toggleVisibility(this.getAllSeries()[0].getPointsByArg(arg)[0]);
			},
			"export": {
				enabled: true
			},
			series: [{
				argumentField: "status",
				valueField: "tot",
				label: {
					visible: true,
					font: {
						size: 16
					},
					connector: {
						visible: true,
						width: 0.5
					},
					position: "columns",
					customizeText: function (arg) {
						return arg.valueText + " (" + arg.percentText + ")";
					}
				}
			}]
		});

		function toggleVisibility(item) {
			if (item.isVisible()) {
				item.hide();
			} else {
				item.show();
			}
		}

		$("#mes").on("change", function () {
			let mes = $(this).val();
			$.ajax({
				url: "Usuarios/dashboardmensual/" + mes,
				data: {},
				type: "POST",
				success: function (response) {
					let data = JSON.parse(response);
					console.log(data);
					if (data.respuesta == 1) {
						$("#totalprospectos").html(`${data.totalprospectos}`);
						$("#ganados").html(`${data.totalganados}`);
						$("#perdidos").html(`${data.totalperdidos}`);
						$("#pvendidos").dxChart({
							dataSource: data.graficaProductos
						});
						$("#tprospectos").dxPieChart({
							dataSource: data.graficaStatus
						});


					}
				}
			});
		});
	});
</script>
