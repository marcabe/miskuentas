<?php require 'layaout/head.php'; ?>
<!--  menu -->
<?php require 'layaout/menu.php'; ?>
<!-- /menu -->
<!-- top navigation -->
<?php require 'layaout/cabecera.php'; ?>
<!-- /top navigation -->

<!-- page content -->
<style>

</style>
<div class="right_col" role="main">
	<div class="">

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12  ">
				<div class="x_panel">
					<div class="x_title">
						<h2>Prospectos</h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content tituloSistema">
						Prospectos detalle: <?php echo $prospecto[0]->nombre; ?>
						<input value="<?php echo $prospecto[0]->nombre; ?>" hidden id="nprospecto">
						<input value="<?php echo $prospecto[0]->idprospecto; ?>" hidden id="idprospecto">
						<input type="date" value="<?php echo date("Y-m-d"); ?>" hidden id="hoy">
						<input type="text" value="<?php echo $paginaanterior; ?>" hidden id="pa">
						<input type="text" value="<?php echo $this->session->userdata("usuario"); ?>" hidden id="usu">

					</div>
					<div class="container mt-5">
						<div class="row">
							<div class="col-md-6 text-left">
								<button class="btn btn-success btn-sm" id="regresar">
									<i class="far fa-arrow-alt-circle-left"></i> Regresar
								</button>
							</div>
							<?php if ($prospecto[0]->status == 1) { ?>

								<div class="col-md-6 text-right">
									<button class="btn btn-danger btn-sm text-right" id="perdido">
										<i class="far fa-window-close"></i> Perdido
									</button>
									<button class="btn btn-success btn-sm text-right" id="ganado">
										<i class="far fa-save"></i> Ganado
									</button>
								</div>
							<?php } ?>
						</div>


						<div class="x_content">

							<ul class="nav nav-tabs bar_tabs" id="myTab" role="tablist">
								<li class="nav-item">
									<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
									   aria-controls="home" aria-selected="true">Prospecto</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
									   aria-controls="profile" aria-selected="false">Mensajes</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab"
									   aria-controls="contact" aria-selected="false">Asesores asignados</a>
								</li>
								<?php if ($prospecto[0]->status == 2) { ?>
									<li class="nav-item">
										<a class="nav-link" id="prod-tab" data-toggle="tab" href="#adquiridos"
										   role="tab"
										   aria-controls="contact" aria-selected="false">Productos adquiridos</a>
									</li>
								<?php } ?>
							</ul>
							<div class="tab-content" id="myTabContent">
								<div class="tab-pane fade show active" id="home" role="tabpanel"
									 aria-labelledby="home-tab">
									<?php if ($prospecto[0]->fechaalerta == $hoy && $prospecto[0]->status == 1) { ?>
										<div class="row">
											<div class="col-md-12 pl-4">
												<i class="fas fa-phone-volume"></i> <label
														id="alertacontacto"> <?php echo $prospecto[0]->fechaalerta . " contactar al prospecto (" . $prospecto[0]->horaalerta . ")"; ?> </label>
											</div>
										</div>
									<?php } ?>
									<div class="row">
										<div class="col-md-12 pl-4 mt-2">
											<h6><i class="far fa-id-card"></i> <?php echo $prospecto[0]->nombre; ?></h6>
										</div>
										<div class="col-md-12 pl-4 mt-2">
											<h6><i class="fa fa-phone"></i> <?php echo $prospecto[0]->telefono; ?></h6>
										</div>
										<div class="col-md-12 pl-4 mt-2">
											<h6><i class="far fa-envelope"></i> <?php echo $prospecto[0]->correo; ?>
											</h6>
										</div>
										<div class="col-md-12 pl-4 mt-2">
											<?php if ($prospecto[0]->status != 2) { ?>
												<h6><i class="fas fa-box-open"></i><b> Producto de
														interes: </b><?php echo $prospecto[0]->producto->nproducto; ?>
												</h6>
											<?php } ?>
										</div>

									</div>
								</div>
								<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
									<div class="row">
										<div class="x_content">
											<ul class="list-unstyled timeline" id="listamensajes">
												<?php foreach ($comentarios as $c) { ?>
													<li>
														<div class="block">
															<div class="tags">
																<a href="" class="tag">
																	<span><?php echo $c->fecha; ?> </span>
																</a>
															</div>
															<div class="block_content">
																<h2 class="title">
																	<a><?php echo $c->usercomentario->nombre; ?></a>
																</h2>
																<div class="byline">
																	<span>Medio de contacto: </span> <?php echo $c->medio; ?>
																</div>
																<p class="excerpt"><?php echo $c->comentario; ?></p>
															</div>
														</div>
													</li>
												<?php } ?>
											</ul>
										</div>
									</div>
									<?php if ($prospecto[0]->status == 1) { ?>
										<?php if ($prospecto[0]->status != 2) { ?>
											<div class="row mt-3">
												<div class="col-md-12 text-center">
													<div class="col-md-6 offset-md-3">
														<div class="form-check form-check-inline">
															<input class="form-check-input opcioncomentario"
																   type="radio"
																   name="opcioncomentario" id="inlineRadio1" value="1">
															<label class="form-check-label"><i
																		class="fab fa-whatsapp"></i>
																WhatsApp</label>
														</div>
														<div class="form-check form-check-inline">
															<input class="form-check-input opcioncomentario"
																   type="radio"
																   name="opcioncomentario" id="inlineRadio2" value="2">
															<label class="form-check-label"><i class="fa fa-phone"></i>
																Teléfono</label>
														</div>
														<div class="form-check form-check-inline">
															<input class="form-check-input opcioncomentario"
																   type="radio"
																   name="opcioncomentario" id="inlineRadio2" value="3">
															<label class="form-check-label"><i
																		class="far fa-envelope"></i>
																E-mail</label>
														</div>
														<div class="form-check form-check-inline">
															<input class="form-check-input opcioncomentario"
																   type="radio"
																   name="opcioncomentario" id="inlineRadio2" value="4">
															<label class="form-check-label"><i
																		class="far fa-calendar-alt"></i>
																Reagendar</label>
														</div>
													</div>
												</div>
												<div class="col-md-12 text-center">
													<div class="col-md-6 offset-md-3">
														<small id="msj_atencion" class="msj_formulario"></small>
													</div>
												</div>
											</div>
											<div class="row mt-5" style="display: none;" id="fechacontenedor">
												<div class="col-md-12 text-center">
													<div class="col-md-6 offset-md-3">
														<div class="form-group row text-right">
															<label for="inputPassword" class="col-md-3 col-form-label">Fecha
																de
																alerta: </label>
															<div class="col-md-9">
																<div class="input-group input-group-sm">
																	<div class="input-group-prepend">
																		<div class="input-group-text"><i
																					class="far fa-calendar-alt"></i>
																		</div>
																	</div>
																	<input type="date"
																		   class="form-control form-control-sm"
																		   id="fechaalerta">
																</div>
																<div class="text-left">
																	<small id="msj_reagendado"
																		   class="msj_formulario"></small>
																</div>
															</div>
														</div>
													</div>
												</div>

												<div class="col-md-12 text-center">
													<div class="col-md-6 offset-md-3">
														<div class="form-group row text-right">
															<label for="inputPassword" class="col-md-3 col-form-label">Hora: </label>
															<div class="col-md-9">
																<div class="input-group input-group-sm">
																	<div class="input-group-prepend">
																		<div class="input-group-text">
																			<i class="far fa-clock"></i>
																		</div>
																	</div>
																	<select class="form-control form-control-sm"
																			id="horaalerta">
																		<option>09:00 am</option>
																		<option>09:30 am</option>
																		<option>10:00 am</option>
																		<option>10:30 am</option>
																		<option>11:00 am</option>
																		<option>11:30 am</option>
																		<option>12:00 pm</option>
																		<option>12:30 pm</option>
																		<option>01:00 pm</option>
																		<option>01:30 pm</option>
																		<option>02:00 pm</option>
																		<option>02:30 pm</option>
																		<option>03:00 pm</option>
																		<option>03:30 pm</option>
																		<option>04:00 pm</option>
																		<option>04:30 pm</option>
																		<option>05:00 pm</option>
																		<option>05:30 pm</option>
																		<option>06:00 pm</option>
																		<option>06:30 pm</option>
																	</select>
																</div>
																<div class="text-left">
																	<small id="msj_reagendado"
																		   class="msj_formulario"></small>
																</div>
															</div>
														</div>
													</div>
												</div>


											</div>
											<div class="row mt-3">
												<div class="col-md-12 text-center">
													<label><i class="fas fa-comment-dots"></i> Comentarios</label>
												</div>
												<div class="col-md-6 offset-md-3 text-center">
													<textarea rows="5" class="form-control" id="comentario"></textarea>
													<small id="msj_comentario" class="msj_formulario"></small>

												</div>
												<div class="col-md-12 text-center mt-3">
													<button id="gcomentario" class="btn btn-success btn-sm"><i
																class="far fa-save"></i>
														Guardar comentario
													</button>
												</div>
											</div>
										<?php } ?>
									<?php } elseif ($prospecto[0]->status == 2) { ?>
										<div class="row">
											<div class="x_content">
												<label>No es posible ingresar mensaje el prospecto ya fue
													ganado.</label>
											</div>
										</div>
									<?php } elseif ($prospecto[0]->status == 3) { ?>
										<div class="row">
											<div class="x_content">
												<label>No es posible ingresar mensaje el prospecto fue perdido.</label>
											</div>
										</div>
									<?php } ?>
								</div>
								<div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
									<div class="row">
										<div class="col-md-12" id="historialcomentarios">
											<?php foreach ($control as $pc) { ?>
												<div class="mt-1 text-left">
													<label class="historial_asesores"><i
																class="far fa-calendar-alt"></i> <?php echo $pc->fecha . " se agrego a la lista del asesor " . $pc->control->nombre; ?>
													</label>
												</div>
											<?php } ?>
										</div>
									</div>
								</div>
								<div class="tab-pane fade" id="adquiridos" role="tabpanel"
									 aria-labelledby="profile-tab">
									<div class="row">
										<div class="col-md-12 text-right">
											<button class="btn btn-success btn-sm text-right" id="venta">
												<i class="far fa-save"></i> Nueva venta
											</button>
										</div>
										<?php $i = 0;
										foreach ($ventas as $v) { ?>
											<div class="col-md-12 <?php echo ($i >= 1) ? 'mt-4' : 'mt-1'; ?> text-left">
												<div class="col-md-12 pl-4 mt-2">
													<h6><i class="fas fa-dollar-sign"></i><b>
															Producto: </b> <?php echo $v->producto->nproducto; ?>
													</h6>
												</div>
												<div class="col-md-12 pl-4 mt-2">
													<h6><i class="far fa-calendar-alt"></i>
														<b> Fecha :</b> <?php echo $v->fecha; ?>
													</h6>
												</div>
												<div class="col-md-12 pl-4 mt-2">
													<h6><i class="fas fa-dollar-sign"></i><b>
															Costo: </b> <?php echo $v->costo; ?>
													</h6>
												</div>
											</div>
											<?php $i++;
										} ?>
									</div>
								</div>
							</div>
						</div>


					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<input hidden value="<?php echo $prospecto[0]->idprospecto; ?>" id="idprospecto">
<input hidden value="<?php echo $hoy; ?>" id="hoy">

<!-- /page content -->

<?php require 'layaout/footer.php'; ?>
<script src="<?php echo base_url(); ?>assets/develop/js/prospecto_detalle.js"></script>
<script>
	$(document).ready(function () {

		$(document).on("blur", "#costo", function () {
			$(this).val(accounting.formatMoney($("#costo").val()));
		});


		$("#ganado").on("click", function () {
			let contenido = `
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12 text-center">
						Ganado
					</div>
				</div>
				<div class="form-group row text-right mt-3">
					<label for="inputPassword" class="col-md-3 col-form-label">Producto: </label>
					<div class="col-md-9">
						<div class="input-group input-group-sm">
							<div class="input-group-prepend">
								<div class="input-group-text"><i class="fas fa-box-open"></i></div>
							</div>
							<select class="form-control form-control-sm" id="productos">
							<?php foreach ($productos as $p) { ?>
								<option value="<?php echo $p->idproducto; ?>"><?php echo $p->nproducto; ?></option>
							<?php } ?>
							</select>
						</div>
						<div class="text-left">
							<small id="msj_productos" class="msj_productos"></small>
						</div>
					</div>
				</div>
				<div class="form-group row text-right">
					<label for="inputPassword" class="col-md-3 col-form-label">Costo: </label>
					<div class="col-md-9">
						<div class="input-group input-group-sm">
							<div class="input-group-prepend">
								<div class="input-group-text"><i class="fas fa-dollar-sign"></i></div>
							</div>
							<input type="text" class="form-control form-control-sm" id="costo">
						</div>
						<div class="text-left">
							<small id="msj_costo" class="msj_costo"></small>
						</div>
					</div>
				</div>

				<div class="form-group row text-right">
					<label for="inputPassword" class="col-md-3 col-form-label">Fecha: </label>
					<div class="col-md-9">
						<div class="input-group input-group-sm">
							<div class="input-group-prepend">
								<div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
							</div>
							<input type="date" class="form-control form-control-sm" id="fechaganado">
						</div>
						<div class="text-left">
							<small id="msj_fechaganado" class="msj_fechaganado"></small>
						</div>
					</div>
				</div>


			</div>`;
			$.confirm({
				title: '<i class="fas fa-robot"></i> Mensaje del sistema',
				content: contenido,
				draggable: true,
				buttons: {
					aceptar: {
						text: '<i class="far fa-thumbs-up"></i> Aceptar', // text for button
						btnClass: 'btn-success success-modal',
						action: function () {

							let avanza = 1;
							let idproducto = $("#productos").val();
							let costo = $("#costo").val();
							let fechaganado = $("#fechaganado").val();

							if (costo == '') {
								$("#msj_costo").html("Debe ingresar el nombre del prospecto");
								$("#costo").css({"background": "#EFD3D2"});
								$("#msj_costo").show();
								avanza = 0;
							}

							if (fechaganado == '') {
								$("#msj_fechaganado").html("Debe ingresar el nombre del prospecto");
								$("#fechaganado").css({"background": "#EFD3D2"});
								$("#msj_fechaganado").show();
								avanza = 0;
							}
							if (avanza == 1) {
								$.ajax({
									url: '../update_prospecto/' + $("#idprospecto").val(),
									data: {
										status: 2,
										idproducto: idproducto,
										costo: accounting.unformat(costo),
										fechaganado: fechaganado
									},
									type: 'POST',
									success: function (response) {
										$.ajax({
											url: "../Historialventas/insert_venta_sistema",
											data: {
												costo: accounting.unformat(costo),
												fechaganado: fechaganado
											},
											type: "POST",
											success: function (response) {
												$.ajax({
													url: "../Ventas/insert",
													data: {
														idproducto: idproducto,
														costo: accounting.unformat(costo),
														fecha: fechaganado,
														idprospecto: $("#idprospecto").val()
													},
													type: "POST",
													success: function (response) {
														window.location = "../" + $("#pa").val();
													}
												});
											}
										});
									}
								});
							} else {
								return 0;
							}
						}
					},
					cerrar: {
						text: '<i class="fas fa-times"></i> Cerrar', // text for button
						btnClass: 'btn-danger', // multiple classes.
						action: function () {
						}
					}
				}
			});
		});

		$("#venta").on("click", function () {
			let contenido = `
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12 text-center">
						Nueva venta
					</div>
				</div>
				<div class="form-group row text-right mt-3">
					<label for="inputPassword" class="col-md-3 col-form-label">Producto: </label>
					<div class="col-md-9">
						<div class="input-group input-group-sm">
							<div class="input-group-prepend">
								<div class="input-group-text"><i class="fas fa-box-open"></i></div>
							</div>
							<select class="form-control form-control-sm" id="productos">
							<?php foreach ($productos as $p) { ?>
								<option value="<?php echo $p->idproducto; ?>"><?php echo $p->nproducto; ?></option>
							<?php } ?>
							</select>
						</div>
						<div class="text-left">
							<small id="msj_productos" class="msj_productos"></small>
						</div>
					</div>
				</div>
				<div class="form-group row text-right">
					<label for="inputPassword" class="col-md-3 col-form-label">Costo: </label>
					<div class="col-md-9">
						<div class="input-group input-group-sm">
							<div class="input-group-prepend">
								<div class="input-group-text"><i class="fas fa-dollar-sign"></i></div>
							</div>
							<input type="text" class="form-control form-control-sm" id="costo">
						</div>
						<div class="text-left">
							<small id="msj_costo" class="msj_costo"></small>
						</div>
					</div>
				</div>

				<div class="form-group row text-right">
					<label for="inputPassword" class="col-md-3 col-form-label">Fecha: </label>
					<div class="col-md-9">
						<div class="input-group input-group-sm">
							<div class="input-group-prepend">
								<div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
							</div>
							<input type="date" class="form-control form-control-sm" id="fechaganado">
						</div>
						<div class="text-left">
							<small id="msj_fechaganado" class="msj_fechaganado"></small>
						</div>
					</div>
				</div>


			</div>`;
			$.confirm({
				title: '<i class="fas fa-robot"></i> Mensaje del sistema',
				content: contenido,
				draggable: true,
				buttons: {
					aceptar: {
						text: '<i class="far fa-thumbs-up"></i> Aceptar', // text for button
						btnClass: 'btn-success success-modal',
						action: function () {

							let avanza = 1;
							let idproducto = $("#productos").val();
							let costo = $("#costo").val();
							let fechaganado = $("#fechaganado").val();

							if (costo == '') {
								$("#msj_costo").html("Debe ingresar el nombre del prospecto");
								$("#costo").css({"background": "#EFD3D2"});
								$("#msj_costo").show();
								avanza = 0;
							}

							if (fechaganado == '') {
								$("#msj_fechaganado").html("Debe ingresar el nombre del prospecto");
								$("#fechaganado").css({"background": "#EFD3D2"});
								$("#msj_fechaganado").show();
								avanza = 0;
							}
							if (avanza == 1) {
								$.ajax({
									url: "../Ventas/insert",
									data: {
										idproducto: idproducto,
										costo: accounting.unformat(costo),
										fecha: fechaganado,
										idprospecto: $("#idprospecto").val()
									},
									type: "POST",
									success: function (response) {
										window.location = "../" + $("#pa").val();
									}
								});
							} else {
								return 0;
							}
						}
					},
					cerrar: {
						text: '<i class="fas fa-times"></i> Cerrar', // text for button
						btnClass: 'btn-danger', // multiple classes.
						action: function () {
						}
					}
				}
			});
		});

	});
</script>
