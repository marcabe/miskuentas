<?php require 'layaout/head.php'; ?>
<!--  menu -->
<?php require 'layaout/menu.php'; ?>
<!-- /menu -->

<!-- top navigation -->
<?php require 'layaout/cabecera.php'; ?>
<!-- /top navigation -->

<!-- page content -->
<style>

</style>
<div class="right_col" role="main">
	<div class="">

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12  ">
				<div class="x_panel">
					<div class="x_title">
						<h2>Modulo Productos</h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content tituloSistema">
						Lista de productos
						<div class="row mt-4">
							<div class="col-md-12">
								<table id="tblgeneral" class="table table-bordered">
									<thead>
									<tr>
										<td><i class="far fa-id-card"></i> Nombre</td>
										<td><i class="fa fa-phone"></i> Status</td>
										<td></td>
									</tr>
									</thead>
									<tbody>
									<?php foreach ($productos as $p) { ?>
										<tr>
											<td><?php echo $p->nproducto; ?></td>
											<td><?php echo ($p->status == 1) ? "Disponible" : "No diponible"; ?></td>
											<td>
												<div class="dropdown">
													<button class="btn btn-success btn-sm dropdown-toggle configprospectos"
															data-toggle="dropdown" aria-haspopup="true"
															aria-expanded="false">
														<i class="fas fa-cogs"></i> Config
													</button>
													<div class="dropdown-menu listaconfig" aria-labelledby="dropdownMenuButton">
													  <span class="dropdown-item cambiastatus" data-content="<?php echo $p->status; ?>" id="<?php echo $p->idproducto; ?>">
														  <i class="fas fa-toggle-off"></i> Disponible / No Disponible
													  </span>
													</div>
												</div>
											</td>
										</tr>
									<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->
<script src="<?php echo base_url(); ?>assets/develop/js/lista_productos.js"></script>

<?php require 'layaout/footer.php'; ?>

