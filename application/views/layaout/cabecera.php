<div class="top_nav">
	<div class="nav_menu">
		<div class="nav toggle">
			<a id="menu_toggle"><i class="fa fa-bars"></i></a>
		</div>
		<nav class="nav navbar-nav">
			<ul class=" navbar-right">
				<li class="nav-item dropdown open" style="padding-left: 15px;">
					<a href="javascript:;" class="user-profile dropdown-toggle" aria-haspopup="true" id="navbarDropdown"
					   data-toggle="dropdown" aria-expanded="false">
						<?php if ($this->session->userdata('rol') == 1) { ?>
							<img src="<?php echo base_url() ?>assets/develop/images/admin.png" alt="">
						<?php } else { ?>
							<img src="<?php echo base_url() ?>assets/develop/images/user.png" alt="">
						<?php } ?>
						<?php echo $this->session->userdata('usuario'); ?>
					</a>
					<div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="<?php echo base_url() ?>"><i
									class="fa fa-sign-out pull-right"></i> Salir</a>
					</div>
				</li>
				<li role="presentation" class="nav-item dropdown open">
					<label class="dropdown-toggle info-number"  style="cursor: pointer;">
						<i class="fas fa-bell" style="color: #3f4c6b !important;"></i>
						<span id="msjalerta" class="badge bg-green" style="background: red !important; border-color: red !important; font-size: 12px !important;"></span>
					</label class="dropdown-toggle info-number" >
				</li>
			</ul>
		</nav>
	</div>
</div>
