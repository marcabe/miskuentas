<div class="col-md-3 left_col">
	<div class="left_col scroll-view">
		<div class="navbar nav_title" style="border: 0;">
			<a href="index.html" class="site_title"><i class="fas fa-toolbox"></i> Miskuentas<span></span></a>
		</div>
		<div class="clearfix"></div>
		<br/>

		<!-- sidebar menu -->
		<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
			<div class="menu_section">
				<h3>General</h3>
				<ul class="nav side-menu">
					<li><a><i class="fas fa-home"></i> Home <span class="fas fa-chevron-down"></span></a>
						<ul class="nav child_menu">
							<li><a href="<?php echo base_url() ?>home">Dashboard</a></li>
						</ul>
					</li>
					<?php if ($this->session->userdata('rol') == 1) { ?>
						<li><a><i class="fas fa-briefcase"></i></i> Historial de Ventas <span
										class="fas fa-chevron-down"></span></a>
							<ul class="nav child_menu">
								<li><a href="<?php echo base_url() ?>historialventas/lista/<?php echo date('Y');?>">Lista Anual</a></li>
								<li><a href="<?php echo base_url() ?>historialventas/match">Comparación</a></li>
								<li><a href="<?php echo base_url() ?>historialventasasesor/lista">Montos mensuales por Asesor</a></li>
								<li><a href="<?php echo base_url() ?>historialventasasesor/match">Comparación entre asesores</a></li>
							</ul>
						</li>
						<li><a><i class="fas fa-box-open"></i> Productos <span class="fas fa-chevron-down"></span></a>
							<ul class="nav child_menu">
								<li><a href="<?php echo base_url() ?>productos/lista">Lista de productos</a></li>
								<li><a href="<?php echo base_url() ?>productos/nuevo">Nuevo producto</a></li>
							</ul>
						</li>

						<li><a><i class="fas fa-users"></i> Usuarios <span class="fas fa-chevron-down"></span></a>
							<ul class="nav child_menu">
								<li><a href="<?php echo base_url() ?>usuarios/lista">Lista de usuarios</a></li>
								<li><a href="<?php echo base_url() ?>usuarios/nuevo">Nuevo usuario</a></li>
							</ul>
						</li>

						<li><a><i class="fas fa-sms"></i> Sms <span class="fas fa-chevron-down"></span></a>
							<ul class="nav child_menu">
								<li><a href="<?php echo base_url() ?>sms/creacion">Creacion de sms</a></li>
								<li><a href="<?php echo base_url() ?>registrosms/lista/<?php echo date('Y-m-d'); ?>">Lista
										de sms enviados</a></li>
							</ul>
						</li>
						<li><a><i class="fas fa-mail-bulk"></i> Mailing <span class="fas fa-chevron-down"></span></a>
							<ul class="nav child_menu">
								<li><a href="<?php echo base_url() ?>correos/lista">Lista de correos</a></li>
								<li>
									<a href="<?php echo base_url() ?>registrocorreos/lista/<?php echo date('Y-m-d'); ?>">Lista
										de correos enviados</a></li>

							</ul>
						</li>


					<?php } ?>
					<li><a><i class="fas fa-users"></i> Prospectos <span class="fas fa-chevron-down"></span></a>
						<ul class="nav child_menu">
							<li class="active"><a>Lista de prospectos<span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu" style="display: block;">
									<li><a href="<?php echo base_url() ?>prospectos/lista">Lista General</a></li>
									<li><a href="<?php echo base_url() ?>detalle_dash/1">Total del Mes</a></li>
									<li><a href="<?php echo base_url() ?>detalle_dash/2">Pendientes del mes</a></li>
									<li><a href="<?php echo base_url() ?>detalle_dash/3">Ganados del mes</a></li>
									<li><a href="<?php echo base_url() ?>detalle_dash/4">Por vencer del mes</a></li>
									<li><a href="<?php echo base_url() ?>detalle_dash/5">Vencidos del mes</a></li>
									<li><a href="<?php echo base_url() ?>detalle_dash/6">Perdidos del mes</a></li>

								</ul>
							</li>

							<li><a href="<?php echo base_url() ?>prospectos/nuevo">Nuevo prospecto</a></li>
						</ul>
					</li>
				</ul>
			</div>


		</div>
		<!-- /sidebar menu -->

		<!-- /menu footer buttons -->
		<!-- /menu footer buttons -->
	</div>
</div>
