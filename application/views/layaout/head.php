<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Miskuentas - CRM</title>

	<!-- Bootstrap -->
	<link href="<?php echo base_url() ?>assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<!-- Font Awesome -->
	<link href="<?php echo base_url() ?>assets/vendors/fontawesome-f/css/all.css" rel="stylesheet">
	<!-- NProgress -->
	<link href="<?php echo base_url() ?>assets/vendors/nprogress/nprogress.css" rel="stylesheet">
	<!-- Custom Theme Style -->
	<link href="<?php echo base_url() ?>assets/build/css/custom.min.css" rel="stylesheet">
	<!--Css general -->
	<link href="<?php echo base_url() ?>assets/develop/css/general.css" rel="stylesheet">

	<script src="<?php echo base_url() ?>assets/build/js/jquery.min.js"></script>
	<script src="<?php echo base_url() ?>assets/build/js/bootstrap.min.js"></script>

	<link rel="stylesheet" href="<?php echo base_url() ?>assets/build/css/jquery-confirm.min.css">

	<link href="<?php echo base_url() ?>assets/build/css/jquery.dataTables.min.css" rel="stylesheet">




</head>
<script>
	$(window).on("load", function () {
		$(".loader").fadeOut("slow");
	})
</script>
<div class="loader"></div>
<input type="text" id="rol" style="display: none;"
	   value="<?php echo $this->session->userdata('rol') ?>">

<body class="nav-md">
<div class="container body">
	<div class="main_container">
