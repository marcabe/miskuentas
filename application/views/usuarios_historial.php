<?php require 'layaout/head.php'; ?>
<!--  menu -->
<?php require 'layaout/menu.php'; ?>
<!-- /menu -->

<!-- top navigation -->
<?php require 'layaout/cabecera.php'; ?>
<!-- /top navigation -->

<!-- page content -->
<style>

</style>
<div class="right_col" role="main">
  <div class="">

    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12  ">
        <div class="x_panel">
          <div class="x_title">
            <h2>Módulo historial</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content tituloSistema">
            Usuario: <?php echo $usuario[0]->nombre; ?>
			  <input value="<?php echo $usuario[0]->correo;?>" id="correo" hidden>
			  <div class="row mt-4">
				  <div class="col-md-4">
					  <div class="form-group">
						  <label>Fecha de inicio:</label>
						  <div class="input-group mb-2">
							  <div class="input-group-prepend">
								  <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
							  </div>
							  <input type="date" class="form-control form-control-sm" id="fi">
						  </div>
						  <div class="text-left">
							  <small id="msj_fi" class="msj_formulario"></small>
						  </div>
					  </div>
				  </div>

				  <div class="col-md-4">
					  <div class="form-group">
						  <label>Fecha de inicio:</label>
						  <div class="input-group mb-2">
							  <div class="input-group-prepend">
								  <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
							  </div>
							  <input type="date" class="form-control form-control-sm" id="ff">
						  </div>
						  <div class="text-left">
							  <small id="msj_ff" class="msj_formulario"></small>
						  </div>
					  </div>
				  </div>
				  <div class="col-md-4 mt-4 text-right">
					  <button class="btn btn-success btn-sm" id="consultar"><i class="fas fa-search"></i>
						  Consultar
					  </button>
				  </div>
			  </div>
          </div>
			<div class="row mt-4">
				<div class="x_content">
					<ul class="list-unstyled timeline">
						<?php foreach ($movimientos as $m) { ?>
							<li>
								<div class="block">
									<div class="tags">
										<a href="" class="tag">
											<span><?php echo $m->fecha; ?> </span>
										</a>
									</div>
									<div class="block_content">
										<h2 class="title">
											<a><?php echo $m->movimiento; ?></a>
										</h2>
										<div class="byline">
											<span>Hora: </span> <?php echo $m->hora; ?>
										</div>
									</div>
								</div>
							</li>
						<?php } ?>
					</ul>
				</div>
			</div>

        </div>
      </div>
    </div>
  </div>
</div>
<!-- /page content -->
<script src="<?php echo base_url(); ?>assets/develop/js/usuario_historial.js"></script>
<?php require 'layaout/footer.php'; ?>
