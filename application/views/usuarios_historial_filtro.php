<?php require 'layaout/head.php'; ?>
<!--  menu -->
<?php require 'layaout/menu.php'; ?>
<!-- /menu -->

<!-- top navigation -->
<?php require 'layaout/cabecera.php'; ?>
<!-- /top navigation -->

<!-- page content -->
<style>

</style>
<div class="right_col" role="main">
  <div class="">

    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12  ">
        <div class="x_panel">
          <div class="x_title">
            <h2>Módulo historial</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content tituloSistema">
            Usuario: <?php echo $usuario[0]->nombre; ?>
			  <div class="row mt-4">
				  <div class="col-md-12 mt-4 text-left">
					  <button class="btn btn-success btn-sm" id="regresar" onclick="window.location='../../../<?php echo $usuario[0]->correo;?>'"><i class="far fa-arrow-alt-circle-left"></i>
						  Reresar
					  </button>
				  </div>
			  </div>
          </div>
			<div class="row mt-4">
				<div class="x_content">
					<ul class="list-unstyled timeline">
						<?php foreach ($movimientos as $m) { ?>
							<li>
								<div class="block">
									<div class="tags">
										<a href="" class="tag">
											<span><?php echo $m->fecha; ?> </span>
										</a>
									</div>
									<div class="block_content">
										<h2 class="title">
											<a><?php echo $m->movimiento; ?></a>
										</h2>
										<div class="byline">
											<span>Hora: </span> <?php echo $m->hora; ?>
										</div>
									</div>
								</div>
							</li>
						<?php } ?>
					</ul>
				</div>
			</div>

        </div>
      </div>
    </div>
  </div>
</div>
<!-- /page content -->
<script src="<?php echo base_url(); ?>assets/develop/js/usuario_historial.js"></script>
<?php require 'layaout/footer.php'; ?>
