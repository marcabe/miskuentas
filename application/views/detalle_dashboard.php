<?php require 'layaout/head.php'; ?>
<!--  menu -->
<?php require 'layaout/menu.php'; ?>
<!-- /menu -->

<!-- top navigation -->
<?php require 'layaout/cabecera.php'; ?>
<!-- /top navigation -->

<!-- page content -->
<div class="right_col" role="main">
	<input type="text" value="<?php echo $tipoinfo;?>" hidden id="nodetalle">
	<div class="">

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12  ">
				<div class="x_panel">
					<div class="x_title">
						<h2>Modulo Prospectos: <?php echo $titulo; ?></h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content tituloSistema">
						Lista de prospectos
						<div class="row mt-2">
							<div class="col-md-12 text-left">
								<button class="btn btn-success btn-sm" id="regresar"><i class="fas fa-search"></i>
									Regresar
								</button>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>Fecha de inicio:</label>
									<div class="input-group mb-2">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
										</div>
										<input type="date" class="form-control form-control-sm" id="fi">
									</div>
									<div class="text-left">
										<small id="msj_fi" class="msj_formulario"></small>
									</div>
								</div>
							</div>

							<div class="col-md-4">
								<div class="form-group">
									<label>Fecha de inicio:</label>
									<div class="input-group mb-2">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
										</div>
										<input type="date" class="form-control form-control-sm" id="ff">
									</div>
									<div class="text-left">
										<small id="msj_ff" class="msj_formulario"></small>
									</div>
								</div>
							</div>
							<div class="col-md-4 mt-4 text-right">
								<button class="btn btn-success btn-sm" id="consultar"><i class="fas fa-search"></i>
									Consultar
								</button>
							</div>
						</div>

						<div class="row mt-5">
							<div class="col-md-12">
								<table id="tblgeneral" class="table table-bordered">
									<thead>
									<tr>
									<?php if($tipoinfo ==6){ ?>
										<td><i class="far fa-id-card"></i> Nombre</td>
										<td><i class="fa fa-phone"></i> Celular</td>
										<td><i class="far fa-envelope"></i> Correo</td>
										<td><i class="far fa-comment-dots"></i> Comentario</td>
									<?php }else{ ?>
										<td><i class="far fa-id-card"></i> Nombre</td>
										<td><i class="fa fa-phone"></i> Celular</td>
										<td><i class="far fa-envelope"></i> Correo</td>
										<td><i class="fas fa-box-open"></i> Producto</td>
										<!--<td><i class="far fa-calendar-alt"></i> Prueba Ini.</td>
										<td><i class="far fa-calendar-alt"></i> Prueba Fin</td>-->
										<td><i class="far fa-user"></i> Asesor</td>
										<td><i class="fas fa-toggle-off"></i> Status</td>
									<?php } ?>
										<td></td>
									</tr>
									</thead>
									<tbody>
									<?php foreach ($prospectos as $p) { ?>
										<tr>
										<?php if($tipoinfo ==6){ ?>
											<td><?php echo $p->nombre; ?></td>
											<td><?php echo $p->telefono; ?></td>
											<td><?php echo $p->correo; ?></td>
											<td><?php echo $p->motivo; ?></td>
										<?php }else{ ?>
											<td><?php echo $p->nombre; ?></td>
											<td><?php echo $p->telefono; ?></td>
											<td><?php echo $p->correo; ?></td>
											<td><?php echo $p->producto->nproducto; ?></td>
											<!--<td><?php echo $p->pruebai; ?></td>
											<td><?php echo $p->pruebaf; ?></td>-->
											<td><?php echo $p->nasesor->nombre; ?></td>
											<td><?php  if($p->status == 1){ echo "Activo";}elseif($p->status == 2){ echo "Ganado";}elseif($p->status == 3){echo "Perdido";} ?></td>
										<?php } ?>
											<td>
												<div class="dropdown">
													<button class="btn btn-success btn-sm dropdown-toggle configprospectos"
															data-toggle="dropdown" aria-haspopup="true"
															aria-expanded="false">
														<i class="fas fa-cogs"></i> Config
													</button>
													<div class="dropdown-menu listaconfig"
														 aria-labelledby="dropdownMenuButton">
														<?php if ($this->session->userdata('rol') == 1 && $p->status == 1) { ?>
															<span class="dropdown-item detalle"
																  id="<?php echo $p->idprospecto; ?>">
															  <i class="fas fa-eye"></i> Ver detalle
														  	</span>
															<span class="dropdown-item cambiar"
																  data-content="<?php echo $p->idprospecto; ?>">
											  					<i class="fas fa-user-edit"></i> Asignar asesor
										  					</span>
															<span class="dropdown-item eliminar"
																  data-content="<?php echo $p->idprospecto; ?>">
											  					<i class="fas fa-trash-alt"></i> Eliminar prospecto
										  					</span>
															<span class="dropdown-item editar"
																  data-content="<?php echo $p->idprospecto; ?>">
											  					<i class="fas fa-edit"></i> Editar prospecto
										  					</span>
														<?php }elseif ($this->session->userdata('rol') == 1 && ($p->status == 2 || $p->status==3)) { ?>
															<span class="dropdown-item detalle"
																  id="<?php echo $p->idprospecto; ?>">
															  <i class="fas fa-eye"></i> Ver detalle
														  	</span>
														<?php }elseif ($this->session->userdata('rol') == 2 && $p->status == 1) { ?>
															<span class="dropdown-item editar"
																  data-content="<?php echo $p->idprospecto; ?>">
											  					<i class="fas fa-edit"></i> Editar prospecto
										  					</span>
															<span class="dropdown-item detalle"
																  id="<?php echo $p->idprospecto; ?>">
																  <i class="fas fa-eye"></i> Ver detalle
															</span>
														<?php }elseif ($this->session->userdata('rol') == 2 && ($p->status == 2 || $p->status==3)) { ?>
															<span class="dropdown-item detalle"
																  id="<?php echo $p->idprospecto; ?>">
																  <i class="fas fa-eye"></i> Ver detalle
															</span>
														<?php }?>
													</div>
												</div>
											</td>
										</tr>
									<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->
<script src="<?php echo base_url(); ?>assets/develop/js/detalle_dashboard.js"></script>

<?php require 'layaout/footer.php'; ?>
<script>
	$(document).ready(function () {
		$(".cambiar").on("click", function () {
			let idprospecto = $(this).attr("data-content");
			let contenido = `
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12 text-center">
						Asignar nuevo asesor
					</div>
				</div>
					<div class="form-group row mt-3">
						<div class="col-md-8 offset-md-2">
							<div class="input-group input-group-sm">
								<div class="input-group-prepend">
									<div class="input-group-text"><i class="far fa-user"></i></div>
								</div>
								<select class="form-control form-control-sm" id="asesor">
									<option value="<?php echo $this->session->userdata('correo'); ?>"><?php echo $this->session->userdata('usuario'); ?></option>
									<?php foreach ($asesores as $a) { ?>
									<option value="<?php echo $a->correo; ?>"><?php echo $a->nombre; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
					</div>
			</div>`;

			$.confirm({
				title: '<i class="fas fa-robot"></i> Mensaje del sistema',
				content: contenido,
				draggable: true,
				buttons: {
					aceptar: {
						text: '<i class="far fa-thumbs-up"></i> Aceptar', // text for button
						btnClass: 'btn-success success-modal',
						action: function () {
							let idAsesor = $("#asesor").val();
							$.ajax({
								url: '../update_prospecto/' + idprospecto,
								data: {
									asesor: idAsesor
								},
								type: 'POST',
								success: function (response) {
									$.ajax({
										url: '../insert_control/',
										data: {
											idprospecto: idprospecto,
											asesor: idAsesor
										},
										type: 'POST',
										success: function (response) {
											$.confirm({
												title: '<i class="fas fa-robot"></i> Mensaje del sistema',
												content: 'Asesor ingresado correctamente',
												draggable: true,
												buttons: {
													aceptar: {
														text: '<i class="far fa-thumbs-up"></i> Aceptar', // text for button
														btnClass: 'btn-success success-modal',
														action: function () {
															window.location = "../detalle_dash/"+$("#nodetalle").val();
														}
													}
												}
											});
										}
									});
								}
							});
						}
					},
					cerrar: {
						text: '<i class="fas fa-times"></i> Cerrar', // text for button
						btnClass: 'btn-danger', // multiple classes.
					}
				}
			});
		});

	});
</script>
