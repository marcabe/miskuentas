<?php require 'layaout/head.php'; ?>
<!--  menu -->
<?php require 'layaout/menu.php'; ?>
<!-- /menu -->

<!-- top navigation -->
<?php require 'layaout/cabecera.php'; ?>
<!-- /top navigation -->

<!-- page content -->
<style>

</style>
<div class="right_col" role="main">
	<div class="">

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12  ">
				<div class="x_panel">
					<div class="x_title">
						<h2>Modulo Mensajes Sms</h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content tituloSistema">
						Lista de mensajes
						<div class="row mt-4">
							<div class="col-md-12">
								<div id="accordion">

									<?php foreach ($sms as $m){ ?>
									<div class="card mt-3">
										<div class="card-header" id="headingOne">
											<h5 class="mb-0">
												<p class="btn btn-link" data-toggle="collapse"
														data-target="#sms<?php echo $m->idsms;?>" aria-expanded="false"
														aria-controls="collapseOne">
													<i class="fas fa-sms"></i> Mensaje día #<?php echo $m->idsms;?>
												</p>
											</h5>
										</div>
										<div id="sms<?php echo $m->idsms;?>" class="collapse in" aria-labelledby="headingOne"
											 data-parent="#accordion">
											<div class="card-body">
												<div class="row">
													<div class="col-md-12">
														<div class="x_title">
															<div class="row text-right">
																<div class="col-md-12">
																	<label class="btnsms btnsmseditar" id="editarsms<?php echo $m->idsms;?>" data-content="<?php echo $m->idsms;?>">
																		 <i class="fas fa-edit"></i> Editar
																	</label>
																	<label style="display:none;" class="btnsms btnsmsguardar" id="guardarsms<?php echo $m->idsms;?>"  data-content="<?php echo $m->idsms;?>">
																		 <i class="fas fa-sd-card"></i> Guardar
																	</label>
																	<label style="display:none;" class="btnsms btnsmscancelar" id="cancelarsms<?php echo $m->idsms;?>" data-content="<?php echo $m->idsms;?>">
																		 <i class="fas fa-sms"></i> Cancelar
																	</label>
																</div>
															</div>
															<div class="clearfix"></div>
														</div>
													</div>
													<div class="col-md-12">
														<p id="smstext<?php echo $m->idsms;?>"><?php echo ($m->sms!='')? $m->sms:"No se tiene mensaje para este día";?></p>
														<div class="row" style="display: none;" id="textareacontent<?php echo $m->idsms;?>">
															<div class="col-md-12">
																<div class="form-group">
																	<textarea class="form-control" id="textarea<?php echo $m->idsms;?>"><?php echo $m->sms;?></textarea>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<?php } ?>


								</div>

							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->
<script src="<?php echo base_url(); ?>assets/develop/js/lista_sms.js"></script>

<?php require 'layaout/footer.php'; ?>

