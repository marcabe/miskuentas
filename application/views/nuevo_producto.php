<?php require 'layaout/head.php'; ?>
<!--  menu -->
<?php require 'layaout/menu.php'; ?>
<!-- /menu -->

<!-- top navigation -->
<?php require 'layaout/cabecera.php'; ?>
<!-- /top navigation -->

<div class="right_col" role="main">
	<div class="">

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12  ">
				<div class="x_panel">
					<div class="x_title">
						<h2>Modulo Productos</h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content tituloSistema">
						Nuevo Producto
					</div>
					<div class="container mt-5">
						<div class="row">
							<div class="col-md-12 text-right">
								<button class="btn btn-success btn-sm text-center" id="guardar"><i
											class="far fa-save"></i> Guardar
								</button>
							</div>
						</div>
						<div class="col-md-6 offset-md-2">

							<div class="form-group row text-right">
								<label for="inputPassword" class="col-md-3 col-form-label">Producto: </label>
								<div class="col-md-9">
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="fas fa-box-open"></i></div>
										</div>
										<input type="text" class="form-control form-control-sm" id="producto"
											   placeholder="Ingresa el producto">
									</div>
									<div class="text-left">
										<small id="msj_producto" class="msj_formulario"></small>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->
<script src="<?php echo base_url(); ?>assets/develop/js/nuevo_producto.js"></script>

<?php require 'layaout/footer.php'; ?>
