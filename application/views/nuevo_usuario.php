<?php require 'layaout/head.php'; ?>
<!--  menu -->
<?php require 'layaout/menu.php'; ?>
<!-- /menu -->

<!-- top navigation -->
<?php require 'layaout/cabecera.php'; ?>
<!-- /top navigation -->

<div class="right_col" role="main">
	<div class="">

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12  ">
				<div class="x_panel">
					<div class="x_title">
						<h2>Modulo Usuarios</h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content tituloSistema">
						Nuevo usuario
					</div>
					<div class="container mt-5">
						<div class="row">
							<div class="col-md-12 text-right">
								<button class="btn btn-success btn-sm text-center" id="guardar"><i
											class="far fa-save"></i> Guardar
								</button>
							</div>
						</div>
						<div class="col-md-6 offset-md-2">

							<div class="form-group row text-right">
								<label for="inputPassword" class="col-md-3 col-form-label">Nombre(s): </label>
								<div class="col-md-9">
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="far fa-user"></i></div>
										</div>
										<input type="text" class="form-control form-control-sm" id="nombre"
											   placeholder="Ingresa el nombre del usuario">
									</div>
									<div class="text-left">
										<small id="msj_nombre" class="msj_formulario"></small>
									</div>
								</div>
							</div>


							<div class="form-group row text-right">
								<label for="inputPassword" class="col-md-3 col-form-label">Correo: </label>
								<div class="col-md-9">
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="far fa-envelope"></i></div>
										</div>
										<input type="text" class="form-control form-control-sm" id="correo"
											   placeholder="Ingresa el correo">
									</div>
									<div class="text-left">
										<small id="msj_correo" class="msj_formulario"></small>
									</div>
								</div>
							</div>

							<div class="form-group row text-right">
								<label for="inputPassword" class="col-md-3 col-form-label">Contraseña: </label>
								<div class="col-md-9">
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="fa fa-lock"></i></div>
										</div>
										<input type="password" class="form-control form-control-sm" id="clave">
									</div>
									<div class="text-left">
										<small id="msj_clave" class="msj_clave"></small>
									</div>
								</div>
							</div>

							<div class="form-group row text-right">
								<label for="inputPassword" class="col-md-3 col-form-label">Confirmar
									contraseña: </label>
								<div class="col-md-9">
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="fa fa-lock"></i></div>
										</div>
										<input type="password" class="form-control form-control-sm" id="clave1">
									</div>
									<div class="text-left">
										<small id="msj_clave1" class="msj_clave1"></small>
									</div>
								</div>
							</div>


							<div class="form-group row text-right">
								<label for="inputPassword" class="col-md-3 col-form-label">Rol: </label>
								<div class="col-md-9">
									<div class="input-group input-group-sm">
										<div class="input-group-prepend">
											<div class="input-group-text"><i class="fas fa-user-tag"></i></div>
										</div>
										<select class="form-control form-control-sm" id="idrol">
											<option value="1">Administrador</option>
											<option value="2">Asesor</option>
										</select>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->
<script src="<?php echo base_url(); ?>assets/develop/js/nuevo_usuario.js"></script>

<?php require 'layaout/footer.php'; ?>
