<?php require 'layaout/head.php'; ?>
<!--  menu -->
<?php require 'layaout/menu.php'; ?>
<!-- /menu -->

<!-- top navigation -->
<?php require 'layaout/cabecera.php'; ?>
<!-- /top navigation -->

<!-- page content -->
<style>

</style>
<div class="right_col" role="main">
	<div class="">

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12  ">
				<div class="x_panel">
					<div class="x_title">
						<h2>Modulo Historial Ventas</h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content tituloSistema">
						Ingrese las ventas mensuales
						<div class="row mt-3">
							<div class="col-md-3 text-left">
								<div class="input-group input-group-sm">
									<div class="input-group-prepend">
										<div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
									</div>
									<select class="form-control form-control-sm" id="anio">
										<option value="<?php echo date("Y") - 1; ?>"><?php echo date("Y") - 1; ?></option>
										<option value="<?php echo date("Y"); ?>" selected><?php echo date("Y"); ?></option>
										<option value="<?php echo date("Y") + 1; ?>"><?php echo date("Y") + 1; ?></option>
									</select>
								</div>
							</div>
							<div class="col-md-3 text-left">
								<div class="input-group input-group-sm">
									<div class="input-group-prepend">
										<div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
									</div>
									<select class="form-control form-control-sm" id="mes">
										<option value="0">Selecciones mes</option>
										<option value="1">Enero</option>
										<option value="2">Febrero</option>
										<option value="3">Marzo</option>
										<option value="4">Abril</option>
										<option value="5">Mayo</option>
										<option value="6">Junio</option>
										<option value="7">Julio</option>
										<option value="8">Agosto</option>
										<option value="9">Septiembre</option>
										<option value="10">Octubre</option>
										<option value="11">Noviembre</option>
										<option value="12">Diciembre</option>
									</select>
								</div>
							</div>
							<div class="col-md-3">
								<div class="input-group input-group-sm">
									<div class="input-group-prepend">
										<div class="input-group-text"><i class="fas fa-dollar-sign"></i></div>
									</div>
									<input type="text" class="form-control form-control-sm" id="ventas">
								</div>
								<div class="text-left">
									<small id="msj_costo" class="msj_costo"></small>
								</div>
							</div>
							<div class="col-md-3 text-center">
								<button class="btn btn-success btn-sm text-center" id="guardar" style="display: none;">
									<i class="far fa-save"></i> Guardar
								</button>
								<button class="btn btn-success btn-sm text-center" id="editar" style="display: none;">
									<i class="far fa-edit"></i> Editar
								</button>
							</div>
						</div>
						<div class="row mt-4">
							<div class="col-md-12 text-center">
								<h6>Lista anual <label id="aniotitulo"><?php echo date("Y");?></label></h6>
							</div>
						</div>
						<div class="col-md-6 offset-md-3" id="divtable">
							<table id="tblgeneral" class="table table-bordered table-hover">
								<thead>
								<tr>
									<td>Mes</td>
									<td id="anioencabezado"><?php echo date("Y");?></td>
									<td id="anioencabezadomenos"><?php echo date("Y")-1;?></td>
									<td>Diferencia</td>
								</tr>
								</thead>
								<tbody id="cuerpoTabla">
								<?php foreach($ventas as $v){ ?>
										<tr>
											<td id="nmes<?php echo $v->nmes;?>"><?php echo $v->nmes;?></td>
											<td id="mesventa<?php echo $v->nmes;?>" class="pventa"><?php echo $v->ventas;?></td>
											<td  class="pventasis"><?php echo $v->ventas_pasado;?></td>
											<td class="diferencia"><?php echo $v->diferencia;?></td>
										</tr>
								<?php } ?>

								</tbody>
							</table>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->
<script src="<?php echo base_url(); ?>assets/develop/js/lista_historialventas.js"></script>

<?php require 'layaout/footer.php'; ?>

