<?php require 'layaout/head.php'; ?>
<!--  menu -->
<?php require 'layaout/menu.php'; ?>
<!-- /menu -->

<!-- top navigation -->
<?php require 'layaout/cabecera.php'; ?>
<!-- /top navigation -->

<!-- page content -->
<style>

</style>
<div class="right_col" role="main">
	<div class="">

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12  ">
				<div class="x_panel">
					<div class="x_title">
						<h2>Modulo Correos</h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content tituloSistema">
						<div class="row mt-4">
							<div class="col-md-12">
								<div class="x_content">
									<div class="row">
										<div class="col-sm-3 mail_list_column">
											<?php foreach ($correos as $c) { ?>
												<section>
													<div class="mail_list">
														<div class="left">
															<i class="fa fa-circle"></i>
															<i class="fa fa-edit editarcorreo" data-content="<?php echo $c->idcorreo; ?>"></i>
														</div>
														<div class="right">
															<h3>
																Asunto: <?php echo ($c->asunto != '') ? substr($c->asunto, 0, 10)."..." : "Sin asunto"; ?></h3>
															<label><?php echo ($c->correosn != '') ? substr($c->correosn, 0, 40)."..." : "Sin contenido"; ?></label>
														</div>
													</div>
												</section>
											<?php } ?>
										</div>
										<!-- /MAIL LIST -->

										<!-- CONTENT MAIL -->
										<div class="col-md-9 cargando"  id="cargando" style="display: none;">
											<div class="row">
												<div class="col-md-12 text-center" style="margin-top: 100px;">
													<img src="<?php echo base_url() ?>assets/develop/images/xhml.gif" width="4%">
												</div>
												<div class="col-md-12 text-center mt-2">
													<img src="<?php echo base_url() ?>assets/develop/images/cargando.gif" width="10%"></div>
											</div>
										</div>
										<div class="col-sm-9 mail_view" id="cajacorreo" style="display: none;">
											<div class="inbox-body">
												<div class="mail_heading row">
													<div class="col-md-12 text-right">
														<button class="btn btn-success btn-sm text-center" id="guardar">
															<i
																	class="far fa-save"></i> Guardar
														</button>
														<button class="btn btn-danger btn-sm text-center" id="guardar">
															<i
																	class="far fa-close"></i> Cerrar
														</button>
													</div>
													<div class="col-md-12">
														<div class="form-group">
															<label for="exampleInputEmail1">Asunto:</label>
															<input type="text" class="form-control form-control-sm"
																   id="asunto">
															<input id="idcorreo" style="display: none;">
														</div>
													</div>
												</div>

												<div class="row">
													<div class="col-md-12 col-sm-12 ">
														<div class="x_content">
															<div class="btn-toolbar editor" data-role="editor-toolbar"
																 data-target="#editor-one">
																<div class="btn-group">
																	<a class="btn dropdown-toggle"
																	   data-toggle="dropdown" title="Font Size"><i
																				class="fa fa-text-height"></i>&nbsp;<b
																				class="caret"></b></a>
																	<ul class="dropdown-menu">
																		<li>
																			<a data-edit="fontSize 5">
																				<p style="font-size:17px">Huge</p>
																			</a>
																		</li>
																		<li>
																			<a data-edit="fontSize 3">
																				<p style="font-size:14px">Normal</p>
																			</a>
																		</li>
																		<li>
																			<a data-edit="fontSize 1">
																				<p style="font-size:11px">Small</p>
																			</a>
																		</li>
																	</ul>
																</div>

																<div class="btn-group">
																	<a class="btn" data-edit="bold"
																	   title="Bold (Ctrl/Cmd+B)"><i
																				class="fa fa-bold"></i></a>
																	<a class="btn" data-edit="italic"
																	   title="Italic (Ctrl/Cmd+I)"><i
																				class="fa fa-italic"></i></a>
																	<a class="btn" data-edit="strikethrough"
																	   title="Strikethrough"><i
																				class="fa fa-strikethrough"></i></a>
																	<a class="btn" data-edit="underline"
																	   title="Underline (Ctrl/Cmd+U)"><i
																				class="fa fa-underline"></i></a>
																</div>

																<div class="btn-group">
																	<a class="btn" data-edit="insertunorderedlist"
																	   title="Bullet list"><i class="fa fa-list-ul"></i></a>
																	<a class="btn" data-edit="insertorderedlist"
																	   title="Number list"><i class="fa fa-list-ol"></i></a>
																	<a class="btn" data-edit="outdent"
																	   title="Reduce indent (Shift+Tab)"><i
																				class="fa fa-dedent"></i></a>
																	<a class="btn" data-edit="indent"
																	   title="Indent (Tab)"><i class="fa fa-indent"></i></a>
																</div>

																<div class="btn-group">
																	<a class="btn" data-edit="justifyleft"
																	   title="Align Left (Ctrl/Cmd+L)"><i
																				class="fa fa-align-left"></i></a>
																	<a class="btn" data-edit="justifycenter"
																	   title="Center (Ctrl/Cmd+E)"><i
																				class="fa fa-align-center"></i></a>
																	<a class="btn" data-edit="justifyright"
																	   title="Align Right (Ctrl/Cmd+R)"><i
																				class="fa fa-align-right"></i></a>
																	<a class="btn" data-edit="justifyfull"
																	   title="Justify (Ctrl/Cmd+J)"><i
																				class="fa fa-align-justify"></i></a>
																</div>

																<div class="btn-group">
																	<a class="btn dropdown-toggle"
																	   data-toggle="dropdown" title="Hyperlink"><i
																				class="fa fa-link"></i></a>
																	<div class="dropdown-menu input-append">
																		<input class="span2" placeholder="URL"
																			   type="text" data-edit="createLink"/>
																		<button class="btn" type="button">Agregar
																		</button>
																	</div>
																	<a class="btn" data-edit="unlink"
																	   title="Remove Hyperlink"><i
																				class="fa fa-cut"></i></a>
																</div>
																<div class="btn-group">
																	<a class="btn" data-edit="undo"
																	   title="Undo (Ctrl/Cmd+Z)"><i
																				class="fa fa-undo"></i></a>
																	<a class="btn" data-edit="redo"
																	   title="Redo (Ctrl/Cmd+Y)"><i
																				class="fa fa-repeat"></i></a>
																</div>
															</div>

															<div id="editor-one" class="editor-wrapper"></div>

															<textarea id="correo"
																	  style="display:none;"></textarea>

															<br/>

															<div class="ln_solid"></div>

														</div>

													</div>
													<div class="col-md-12">
														<div class="attachment">
															<p>
																<span><i class="fa fa-paperclip"></i> Archivos adjuntos — </span>
															</p>
														</div>
														<div id="listaimg" class="row">
																
														</div>
													</div>
													<div class="col-md-12" id="imagenes">
														<input  multiple="multiple" type="file" class="form-control form-control-sm imgcorreo" id="imgcorreo">
													</div>
												</div>


											</div>

										</div>
										<!-- /CONTENT MAIL -->
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->
<script src="<?php echo base_url(); ?>assets/develop/js/lista_correos.js"></script>

<?php require 'layaout/footer.php'; ?>

