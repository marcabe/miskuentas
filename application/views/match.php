<?php require 'layaout/head.php'; ?>
<!--  menu -->
<?php require 'layaout/menu.php'; ?>
<!-- /menu -->

<!-- top navigation -->
<?php require 'layaout/cabecera.php'; ?>
<!-- /top navigation -->

<!-- page content -->
<style>

</style>
<div class="right_col" role="main">
	<div class="">

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12  ">
				<div class="x_panel">
					<div class="x_title">
						<h2>Modulo Historial Ventas / Match</h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content tituloSistema">
						<div class="row mt-3">
							<div class="col-md-4 text-center">
								<div class="input-group input-group-sm">
									<div class="input-group-prepend">
										<div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
									</div>
									<select class="form-control form-control-sm" id="anioa">
										<option value="<?php echo date("Y") - 1; ?>"><?php echo date("Y") - 1; ?></option>
										<option value="<?php echo date("Y"); ?>" selected><?php echo date("Y"); ?></option>
										<option value="<?php echo date("Y") + 1; ?>"><?php echo date("Y") + 1; ?></option>
									</select>
								</div>
							</div>
							<div class="col-md-4 text-center">
								<div class="input-group input-group-sm">
									<div class="input-group-prepend">
										<div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
									</div>
									<select class="form-control form-control-sm" id="aniob">
										<option value="<?php echo date("Y") - 1; ?>"><?php echo date("Y") - 1; ?></option>
										<option value="<?php echo date("Y"); ?>" selected><?php echo date("Y"); ?></option>
										<option value="<?php echo date("Y") + 1; ?>"><?php echo date("Y") + 1; ?></option>
									</select>
								</div>
							</div>
							<div class="col-md-4 text-center">
								<button class="btn btn-success btn-sm text-center" id="match">
									<i class="far fa-save"></i> Match
								</button>
							</div>
						</div>

						<div class="row mt-3">
							<div class="col-md-12 text-center">
								<div id="grafmatch"></div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->
<script src="<?php echo base_url(); ?>assets/develop/js/match.js"></script>

<?php require 'layaout/footer.php'; ?>

