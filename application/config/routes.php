<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Usuarios/login';

#Rutas para el controlador Usuarios
$route['valida_login'] = 'Usuarios/valida_login';
$route['home'] = 'Usuarios/home';
$route['salir'] = 'Usuarios/logout';
$route['usuarios/nuevo'] = 'Usuarios/nuevo';
$route['usuarios/lista'] = 'Usuarios/index';
$route['add_usuario'] = 'Usuarios/insert';
$route['update_usuario/(:any)'] = 'Usuarios/update/$1';
$route['usuarios/historial/(:any)'] = 'Usuarios/historial/$1';
$route['usuarios/historial/filtro/(:any)/(:any)/(:any)'] = 'Usuarios/historial_filtro/$1/$2/$3';


#Rutas para el controlador prospecto
$route['prospectos/get_where'] = 'Prospectos/get_where';
$route['prospectos/nuevo'] = 'Prospectos/nuevo';
$route['prospectos/edicion/(:any)'] = 'Prospectos/edit/$1';
$route['add_prospecto'] = 'Prospectos/insert';
$route['prospectos/lista'] = 'Prospectos/index';
$route['prospectos/(:any)'] = 'Prospectos/detalle/$1';
$route['update_prospecto/(:any)'] = 'Prospectos/update/$1';
$route['delete_prospectos/(:any)'] = 'Prospectos/delete/$1';
$route['prospectos/lista/filtro/(:any)/(:any)'] = 'Prospectos/filtro/$1/$2';
$route['detalle_dash/filtro/(:any)/(:any)/(:any)'] = 'Prospectos/detalle_dash_filtro/$1/$2/$3';
$route['detalle_dash/(:any)'] = 'Prospectos/detalle_dash/$1';

#Rutas para el sms
$route['sms/creacion'] = 'Sms/index';
$route['update_sms/(:any)'] = 'Sms/update/$1';

#Registro de envio de sms
$route['registrosms/lista/(:any)'] = 'Registrosms/index/$1';

#Registro envio de correos
$route['registrocorreos/lista/(:any)'] = 'Registrocorreos/index/$1';


#Rutas para los correos
$route['correos/lista'] = 'Correos/index';




#Rutas para los comentarios
$route['comentarios/insert'] = 'Pagina/insert';


#Rutas control
$route['insert_control'] = 'Control/insert';

#Rutas para productos
$route['productos/lista'] = 'productos/index';
$route['update_producto/(:any)'] = 'Productos/update/$1';
$route['productos/nuevo'] = 'Productos/nuevo';
$route['add_producto'] = 'Productos/insert';


#Rutas para los movimientos
$route['insert_movimiento'] = 'Historial/insert';


#Rutas para el historial de ventas
$route['historialventas/lista/(:any)'] = 'Historialventas/index/$1';
$route['historialventas/match/'] = 'Historialventas/match';

#Historial ventas asesor
$route['historialventasasesor/lista'] = 'Historialventasasesor/index';
$route['historialventasasesor/match'] = 'Historialventasasesor/match';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
