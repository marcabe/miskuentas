<?php
defined('BASEPATH') or exit('No direct script access allowed');

date_default_timezone_set("America/Mexico_City");


class Prospectos extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->Model('ProductosModel');
		$this->load->Model('UsuariosModel');
		$this->load->Model('UsuariosModel');
		$this->load->Model('ControlModel');
		$this->load->Model('ComentariosModel');
		$this->load->Model("HistorialModel");
		$this->load->Model("SmsModel");
		$this->load->Model("CorreosModel");
		$this->load->Model("ImgcorreosModel");
		$this->load->Model("VentasModel");
		$this->load->Model("RegistrocorreoModel");
		$this->load->Model("RegistrosmsModel");
		$this->load->Model("ProspectosModel");

		include_once('mailer/src/class.phpmailer.php');
		include_once('mailer/src/class.smtp.php');

		$this->hoy = date("Y-m-d");
		$this->finicio = date("Y") . "-" . date("m") . "-" . "01";
		$this->ffinal = date("Y") . "-" . date("m") . "-" . date("t", strtotime("01-" . date("m") . "-" . date("Y")));
		$this->diavencer = date("Y-m-d", strtotime($this->hoy . "+ 3 days"));

	}

	public function index()
	{
		if ($this->session->userdata('log_in') == true) {
			if ($this->session->userdata('rol') == 1) {
				$dataAsesores = $this->UsuariosModel->get_where(array("idrol" => 2, "status" => 1));
				$dataProspectos = $this->ProspectosModel->get();
				foreach ($dataProspectos as $p) {
					$dataProducto = $this->ProductosModel->get_by_id($p->idproducto);
					$p->producto = $dataProducto[0];
					$dataUsuarios = $this->UsuariosModel->get_by_id($p->asesor);
					$p->nasesor = $dataUsuarios[0];

				}
				$data = array(
					"prospectos" => $dataProspectos,
					"asesores" => $dataAsesores
				);
			} else {
				$dataAsesores = $this->UsuariosModel->get_where(array("idrol" => 2, "status" => 1));
				$dataProspectos = $this->ProspectosModel->get_where(array("asesor" => $this->session->userdata("correo")));
				foreach ($dataProspectos as $p) {
					$dataProducto = $this->ProductosModel->get_by_id($p->idproducto);
					$p->producto = $dataProducto[0];
					$dataUsuarios = $this->UsuariosModel->get_by_id($p->asesor);
					$p->nasesor = $dataUsuarios[0];
				}
				$data = array(
					"prospectos" => $dataProspectos,
					"asesores" => $dataAsesores
				);
			}

			$this->load->view('lista_prospectos', $data);

		} else {
			redirect("../");
		}
	}

	public function detalle($idprospecto = 0)
	{
		if ($this->session->userdata('log_in') == true) {
			$pagina_anterior = str_replace(base_url(), "", $_SERVER['HTTP_REFERER']);
			//echo base_url();echo "<br>";
			//echo $pagina_anterior;die;
			if ($idprospecto != 0) {
				$dataInfoProspecto = $this->ProspectosModel->get_by_id($idprospecto);
				foreach ($dataInfoProspecto as $p) {
					$dataProducto = $this->ProductosModel->get_by_id($p->idproducto);
					$p->producto = $dataProducto[0];
				}

				$dataControl = $this->ControlModel->get_where(array("idprospecto" => $idprospecto));
				foreach ($dataControl as $dc) {
					$dataUsuarios = $this->UsuariosModel->get_by_id($dc->asesor);
					$dc->control = $dataUsuarios[0];
				}

				$dataComentarios = $this->ComentariosModel->get_where(array("idprospecto" => $idprospecto));
				foreach ($dataComentarios as $dc) {
					$dataUsuarios = $this->UsuariosModel->get_by_id($dc->asesor);
					$dc->usercomentario = $dataUsuarios[0];
					switch ($dc->medioatencion) {
						case 1:
							$dc->medio = "WhatsApp";
							break;
						case 2:
							$dc->medio = "Teléfono";
							break;
						case 3:
							$dc->medio = "E-mail";
							break;
						case 4:
							$dc->medio = "Reagendado para " . $dataInfoProspecto[0]->fechaalerta . " a las " . $dataInfoProspecto[0]->horaalerta;
							break;
					}
				}
				$dataProductos = $this->ProductosModel->get_where(array("status" => 1, "idproducto!=" => 1));
				$dataVentas = $this->VentasModel->get_where(array("idprospecto" => $idprospecto));
				foreach ($dataVentas as $v) {
					$dataP = $this->ProductosModel->get_where(array("idproducto=" => $v->idproducto));
					$v->producto = $dataP[0];
				}
				$data = array(
					"prospecto" => $dataInfoProspecto,
					"comentarios" => $dataComentarios,
					"control" => $dataControl,
					"hoy" => $this->hoy,
					"productos" => $dataProductos,
					"paginaanterior" => $pagina_anterior,
					"ventas" => $dataVentas
				);
				$this->load->view('prospecto_detalle', $data);
			}
		} else {
			redirect("../");
		}
	}

	public function nuevo()
	{
		if ($this->session->userdata('log_in') == true) {
			$dataProductos = $this->ProductosModel->get_where(array("status" => 1));
			#Obtenemos el nombre de los asesores
			$dataAsesores = $this->UsuariosModel->get_where(array("idrol" => 2, "status" => 1));
			$data = array(
				"asesores" => $dataAsesores,
				"productos" => $dataProductos
			);
			$this->load->view('nuevo_prospecto', $data);
		} else {
			redirect("../");
		}
	}

	public function cambia_alerta()
	{
		$dataProspectos = $this->ProspectosModel->get();
		foreach ($dataProspectos as $p) {
			$fecha1 = new DateTime($this->hoy);
			$fechaalerta = new DateTime($p->fechaalerta);
			$diff = $fecha1->diff($fechaalerta);
			if ($diff->invert == 1 && $diff->days == 1) {
				$update["fechaalerta"] = date("Y-m-d", strtotime($p->fechaalerta . "+ 2 days"));
				$this->ProspectosModel->edit($p->idprospecto, $update);
			}
		}
	}

	public function insert()
	{
		$dataProspecto = $this->ProspectosModel->get_where(array("telefono" => $this->input->post('telefono')));
		if (count($dataProspecto) >= 1) {
			$dataAsesor = $this->UsuariosModel->get_where(array("correo" => $dataProspecto[0]->asesor));
			$response = array(
				"asesor" => $dataAsesor[0]->nombre,
				"respuesta" => 101,
				"status" => $dataProspecto[0]->status
			);
			echo json_encode($response);
		} else {
			$dataResponse = $this->input->post();
			$dataResponse['fechacreate'] = $this->hoy;
			$dataResponse['fechaalerta'] = date("Y-m-d", strtotime($this->hoy . "+ 2 days"));
			$idProspecto = $this->ProspectosModel->insert($dataResponse);
			$dataControl = array(
				"idprospecto" => $idProspecto,
				"asesor" => $dataResponse['asesor'],
				"fecha" => $this->hoy
			);
			$this->HistorialModel->insert(array("movimiento" => "Inserto al usuario: " . $dataResponse['nombre'], "correo" => $this->session->userdata("correo"), "fecha" => $this->hoy, "hora" => date("H:i")));
			$this->ControlModel->insert($dataControl);
			if ($this->input->post('idsms') != 100) {
				$datasms = $this->SmsModel->get_by_id($dataResponse['idsms']);
				$numero = str_replace("+521 ", "", $dataResponse['telefono']);
				$this->msn_send($datasms[0]->sms, $numero);
				$this->ProspectosModel->edit($idProspecto, array("idsms" => ($dataResponse['idsms'] + 1)));
				$response = array(
					"respuesta" => 1
				);
				echo json_encode($response);
			} else {
				$response = array(
					"respuesta" => 1
				);
				echo json_encode($response);
			}

		}

	}

	public function alerta_prospecto($idprospecto = 0)
	{
		$dataProspectos = $this->ProspectosModel->get_by_id($idprospecto);
		$update["fechaalerta"] = date("Y-m-d", strtotime($dataProspectos[0]->fechaalerta . "+ 2 days"));
		$update["horaalerta"] = "09:00";
		$this->ProspectosModel->edit($idprospecto, $update);
		echo 1;
	}

	public function cambia_idsms($idprospecto = 0, $dataUpdate)
	{
		if ($dataUpdate["idsms"] != 100) {
			$dataProspecto = $this->ProspectosModel->get_by_id($idprospecto);
			if ($dataProspecto[0]->idsms != $dataUpdate["idsms"]) {
				$datasms = $this->SmsModel->get_by_id($dataUpdate['idsms']);
				$numero = str_replace("+521 ", "", $dataUpdate['telefono']);
				$this->msn_send($datasms[0]->sms, $numero);
				if (($dataUpdate['idsms'] + 1) <= 15) {
					$this->ProspectosModel->edit($idprospecto, array("idsms" => ($dataUpdate['idsms'] + 1)));
				} elseif (($dataUpdate['idsms'] + 1) >= 16) {
					$this->ProspectosModel->edit($idprospecto, array("idsms" => 1));
				}
			}
		} elseif ($dataUpdate["idsms"] == 100) {
			$this->ProspectosModel->edit($idprospecto, array("idsms" => $dataUpdate['idsms']));
		}
	}

	public function cambia_correo($idprospecto = 0, $dataUpdate)
	{
		if ($dataUpdate["idcorreo"] != 100) {
			$dataProspecto = $this->ProspectosModel->get_by_id($idprospecto);
			if ($dataProspecto[0]->idcorreo != $dataUpdate["idcorreo"]) {
				$adjuntos = array();
				$datacorreo = $this->CorreosModel->get_by_id($dataUpdate['idcorreo']);
				$dataAdjunto = $this->ImgcorreosModel->get_where(array("idcorreo" => $datacorreo[0]->idcorreo));
				foreach ($dataAdjunto as $c) {
					$adjuntos[] = $c->ruta;
				}
				if (count($adjuntos) > 0) {
					$this->enviarcorreo($dataProspecto[0]->nombre, $dataProspecto[0]->correo, $datacorreo[0]->asunto, $datacorreo[0]->correo, $adjuntos);
				} else {
					$this->enviarcorreo($dataProspecto[0]->nombre, $dataProspecto[0]->correo, $datacorreo[0]->asunto, $datacorreo[0]->correo);
				}
				if (($dataUpdate['idcorreo'] + 1) <= 15) {
					$this->ProspectosModel->edit($idprospecto, array("idcorreo" => ($dataUpdate['idcorreo'] + 1)));
				} elseif (($dataUpdate['idcorreo'] + 1) >= 16) {
					$this->ProspectosModel->edit($idprospecto, array("idcorreo" => 1));

				}
			}
		} elseif ($dataUpdate["idcorreo"] == 100) {
			$this->ProspectosModel->edit($idprospecto, array("idcorreo" => $dataUpdate['idcorreo']));

		}

	}

	public function update($idprospecto = 0)
	{
		$dataUpdate = $this->input->post();
		if (isset($dataUpdate['movi'])) {
			unset($dataUpdate['movi']);
			echo "Funcion 1";
			$this->cambia_idsms($idprospecto, $dataUpdate);
			echo "Funcion 2";
			$this->cambia_correo($idprospecto, $dataUpdate);
			unset($dataUpdate['idcorreo']);
			unset($dataUpdate['idsms']);
			$this->ProspectosModel->edit($idprospecto, $dataUpdate);
			$dataProspecto = $this->ProspectosModel->get_by_id($idprospecto);
			$this->HistorialModel->insert(array("movimiento" => "Se edito el prospecto: " . $dataProspecto[0]->nombre, "correo" => $this->session->userdata("correo"), "fecha" => $this->hoy, "hora" => date("H:i")));

			//echo 1;
		} else {
			$this->ProspectosModel->edit($idprospecto, $dataUpdate);
			$dataProspecto = $this->ProspectosModel->get_by_id($idprospecto);
			$this->HistorialModel->insert(array("movimiento" => "Se edito el prospecto: " . $dataProspecto[0]->nombre, "correo" => $this->session->userdata("correo"), "fecha" => $this->hoy, "hora" => date("H:i")));
			echo 1;
		}

	}

	public function delete($idprospecto = 0)
	{
		#Para borrar el usuario debemos eleiminar primero los comentarios y el control de los asesores
		$this->ControlModel->delete_where(array("idprospecto" => $idprospecto));
		$this->ComentariosModel->delete_where(array("idprospecto" => $idprospecto));
		$this->RegistrocorreoModel->delete_where(array("idprospecto" => $idprospecto));
		$this->RegistrosmsModel->delete_where(array("idprospecto" => $idprospecto));
		$this->ProspectosModel->delete_where(array("idprospecto" => $idprospecto));
		echo 1;
	}

	public function detalle_dash($tipoInfo = 0)
	{
		if ($this->session->userdata('log_in') == true) {
			$mes = date("m");
			$dataAsesores = $this->UsuariosModel->get_where(array("idrol" => 2, "status" => 1));
			switch ($tipoInfo) {
				case 1:
					#Todos los prospectos activos sin eliminar, sin ganado
					if ($this->session->userdata('rol') == 1) {
						$dataProspectos = $this->ProspectosModel->get_where(array("MONTH(fechacreate)" => $mes, "status" => 1));
					} else {
						$dataProspectos = $this->ProspectosModel->get_where(array("MONTH(fechacreate)" => $mes, "status" => 1, "asesor" => $this->session->userdata("correo")));
					}
					$titulo = "Total Gral. Prospectos";
					break;
				case 2:
					#Todos los prospectos que vencen hoy que sean activos
					if ($this->session->userdata('rol') == 1) {
						$dataProspectos = $this->ProspectosModel->get_where(array("fechaalerta" => $this->hoy, "status" => 1));
					} else {
						$dataProspectos = $this->ProspectosModel->get_where(array("fechaalerta" => $this->hoy, "status" => 1, "asesor" => $this->session->userdata("correo")));

					}
					$titulo = "Prospectos pendientes";
					break;
				case 3:
					#Mostramos los prospectos que fueron ganados en el mes
					if ($this->session->userdata('rol') == 1) {
						$dataProspectos = $this->ProspectosModel->get_where(array("fechaganado >=" => $this->finicio, "fechaganado <=" => $this->ffinal, "status" => 2));
					} else {
						$dataProspectos = $this->ProspectosModel->get_where(array("fechaganado >=" => $this->finicio, "fechaganado <=" => $this->ffinal, "status" => 2, "asesor" => $this->session->userdata("correo")));
					}
					$titulo = "Prospectos ganados en el mes";
					break;
				case 4:
					#datosporvencer
					if ($this->session->userdata('rol') == 1) {
						$dataProspectos = $this->ProspectosModel->get_where(array("pruebaf" => $this->diavencer));
					} else {
						$dataProspectos = $this->ProspectosModel->get_where(array("pruebaf" => $this->diavencer, "asesor" => $this->session->userdata("correo")));
					}
					$titulo = "Prospectos por vencer";
					break;
				case 5:
					if ($this->session->userdata('rol') == 1) {
						$dataProspectos = $this->ProspectosModel->get_where(array("pruebaf <" => $this->hoy));
					} else {
						$dataProspectos = $this->ProspectosModel->get_where(array("pruebaf <" => $this->hoy, "asesor" => $this->session->userdata("correo")));
					}
					$titulo = "Prospectos vencidos";
					break;
				case 6:
					#Todos los prospectos activos sin eliminar, sin ganado
					if ($this->session->userdata('rol') == 1) {
						$dataProspectos = $this->ProspectosModel->get_where(array("MONTH(fechaperdido)" => $mes, "status" => 3));
						foreach ($dataProspectos as $p) {
							$dataComentarios = $this->ComentariosModel->get_where(array("idProspecto" => $p->idprospecto));
							$tam = count($dataComentarios);
							if ($tam > 0) {
								$comentariofinal = $dataComentarios[$tam - 1]->comentario;
								$p->comentario = $comentariofinal;
							} else {
								$p->comentario = '';
							}

						}
					} else {
						$dataProspectos = $this->ProspectosModel->get_where(array("MONTH(fechaperdido)" => $mes, "status" => 3, "asesor" => $this->session->userdata("correo")));
						foreach ($dataProspectos as $p) {
							$dataComentarios = $this->ComentariosModel->get_where(array("idProspecto" => $p->idprospecto));
							$tam = count($dataComentarios);
							if ($tam > 0) {
								$comentariofinal = $dataComentarios[$tam - 1]->comentario;
								$p->comentario = $comentariofinal;
							} else {
								$p->comentario = '';
							}

						}
					}
					$titulo = "Total Prospectos Perdidos";
					break;
			}
			foreach ($dataProspectos as $p) {
				$dataProducto = $this->ProductosModel->get_by_id($p->idproducto);
				$p->producto = $dataProducto[0];
				$dataUsuarios = $this->UsuariosModel->get_by_id($p->asesor);
				$p->nasesor = $dataUsuarios[0];
			}
			$data = array(
				"prospectos" => $dataProspectos,
				"titulo" => $titulo,
				"asesores" => $dataAsesores,
				"tipoinfo" => $tipoInfo
			);
			$this->load->view('detalle_dashboard', $data);
		} else {
			redirect("../");
		}


	}

	public function filtro($fi, $ff)
	{
		if ($this->session->userdata('log_in') == true) {
			if ($this->session->userdata('rol') == 1) {
				$dataAsesores = $this->UsuariosModel->get_where(array("idrol" => 2, "status" => 1));
				$dataProspectos = $this->ProspectosModel->get_where(array("fechacreate>=" => $fi, "fechacreate<=" => $ff));
				foreach ($dataProspectos as $p) {
					$dataProducto = $this->ProductosModel->get_by_id($p->idproducto);
					$p->producto = $dataProducto[0];
					$dataUsuarios = $this->UsuariosModel->get_by_id($p->asesor);
					$p->nasesor = $dataUsuarios[0];

				}
				$data = array(
					"prospectos" => $dataProspectos,
					"asesores" => $dataAsesores
				);
			} else {
				$dataAsesores = $this->UsuariosModel->get_where(array("idrol" => 2, "status" => 1));
				$dataProspectos = $this->ProspectosModel->get_where(array("fechacreate>=" => $fi, "fechacreate<=" => $ff, "asesor" => $this->session->userdata("correo")));
				foreach ($dataProspectos as $p) {
					$dataProducto = $this->ProductosModel->get_by_id($p->idproducto);
					$p->producto = $dataProducto[0];
					$dataUsuarios = $this->UsuariosModel->get_by_id($p->asesor);
					$p->nasesor = $dataUsuarios[0];
				}
				$data = array(
					"prospectos" => $dataProspectos,
					"asesores" => $dataAsesores
				);
			}
			$this->load->view('lista_prospectos_filtro', $data);
		} else {
			redirect("../");
		}
	}


	public function detalle_dash_filtro($fi, $ff, $tipo)
	{


		if ($this->session->userdata('log_in') == true) {
			$mes = date("m");
			$dataAsesores = $this->UsuariosModel->get_where(array("idrol" => 2, "status" => 1));
			switch ($tipo) {
				case 1:
					#Todos los prospectos activos sin eliminar, sin ganado
					if ($this->session->userdata('rol') == 1) {
						$dataProspectos = $this->ProspectosModel->get_where(array("MONTH(fechacreate)" => $mes, "fechacreate>=" => $fi, "fechacreate<=" => $ff));
					} else {
						$dataProspectos = $this->ProspectosModel->get_where(array("MONTH(fechacreate)" => $mes, "asesor" => $this->session->userdata("correo"), "fechacreate>=" => $fi, "fechacreate<=" => $ff));
					}
					$titulo = "Total Gral. Prospectos";
					break;
				case 2:
					#Todos los prospectos que vencen hoy que sean activos
					if ($this->session->userdata('rol') == 1) {
						$dataProspectos = $this->ProspectosModel->get_where(array("fechaalerta" => $this->hoy, "status" => 1, "fechacreate>=" => $fi, "fechacreate<=" => $ff));
					} else {
						$dataProspectos = $this->ProspectosModel->get_where(array("fechaalerta" => $this->hoy, "status" => 1, "asesor" => $this->session->userdata("correo"), "fechacreate>=" => $fi, "fechacreate<=" => $ff));

					}
					$titulo = "Prospectos pendientes";
					break;
				case 3:
					#Mostramos los prospectos que fueron ganados en el mes
					if ($this->session->userdata('rol') == 1) {
						$dataProspectos = $this->ProspectosModel->get_where(array("fechaganado >=" => $this->finicio, "fechaganado <=" => $this->ffinal, "status" => 2, "fechacreate>=" => $fi, "fechacreate<=" => $ff));
					} else {
						$dataProspectos = $this->ProspectosModel->get_where(array("fechaganado >=" => $this->finicio, "fechaganado <=" => $this->ffinal, "status" => 2, "asesor" => $this->session->userdata("correo"), "fechacreate>=" => $fi, "fechacreate<=" => $ff));
					}
					$titulo = "Prospectos ganados en el mes";
					break;
				case 4:
					#datosporvencer
					if ($this->session->userdata('rol') == 1) {
						$dataProspectos = $this->ProspectosModel->get_where(array("pruebaf" => $this->diavencer, "fechacreate>=" => $fi, "fechacreate<=" => $ff));
					} else {
						$dataProspectos = $this->ProspectosModel->get_where(array("pruebaf" => $this->diavencer, "asesor" => $this->session->userdata("correo"), "fechacreate>=" => $fi, "fechacreate<=" => $ff));
					}
					$titulo = "Prospectos por vencer";
					break;
				case 5:
					if ($this->session->userdata('rol') == 1) {
						$dataProspectos = $this->ProspectosModel->get_where(array("pruebaf <" => $this->hoy, "fechacreate>=" => $fi, "fechacreate<=" => $ff));
					} else {
						$dataProspectos = $this->ProspectosModel->get_where(array("pruebaf <" => $this->hoy, "asesor" => $this->session->userdata("correo"), "fechacreate>=" => $fi, "fechacreate<=" => $ff));
					}
					$titulo = "Prospectos vencidos";
					break;

				case 6:
					#Todos los prospectos activos sin eliminar, sin ganado
					if ($this->session->userdata('rol') == 1) {
						$dataProspectos = $this->ProspectosModel->get_where(array("status" => 3, "fechaperdido>=" => $fi, "fechaperdido<=" => $ff));

						foreach ($dataProspectos as $p) {
							$dataComentarios = $this->ComentariosModel->get_where(array("idProspecto" => $p->idprospecto));
							$tam = count($dataComentarios);
							if ($tam > 0) {
								$comentariofinal = $dataComentarios[$tam - 1]->comentario;
								$p->comentario = $comentariofinal;
							} else {
								$p->comentario = '';
							}

						}
					} else {
						$dataProspectos = $this->ProspectosModel->get_where(array("fechaperdido>=" => $fi, "fechaperdido<=" => $ff, "status" => 3, "asesor" => $this->session->userdata("correo")));
						foreach ($dataProspectos as $p) {
							$dataComentarios = $this->ComentariosModel->get_where(array("idProspecto" => $p->idprospecto));
							$tam = count($dataComentarios);
							if ($tam > 0) {
								$comentariofinal = $dataComentarios[$tam - 1]->comentario;
								$p->comentario = $comentariofinal;
							} else {
								$p->comentario = '';
							}

						}
					}
					$titulo = "Total Prospectos Perdidos";
					break;
			}
			foreach ($dataProspectos as $p) {
				$dataProducto = $this->ProductosModel->get_by_id($p->idproducto);
				$p->producto = $dataProducto[0];
				$dataUsuarios = $this->UsuariosModel->get_by_id($p->asesor);
				$p->nasesor = $dataUsuarios[0];
			}
			$data = array(
				"prospectos" => $dataProspectos,
				"titulo" => $titulo,
				"asesores" => $dataAsesores,
				"tipoinfo" => $tipo,
				"fi" => $fi,
				"ff" => $ff
			);
			$this->load->view('detalle_dash_filtro', $data);
		} else {
			redirect("../");
		}


	}

	public function edit($idusuario)
	{


		if ($this->session->userdata('log_in') == true) {
			$pagina_anterior = str_replace(base_url(), "", $_SERVER['HTTP_REFERER']);
			$dataProductos = $this->ProductosModel->get_where(array("status" => 1));
			#Obtenemos el nombre de los asesores
			$dataAsesores = $this->UsuariosModel->get_where(array("idrol" => 2, "status" => 1));
			$dataProspectos = $this->ProspectosModel->get_by_id($idusuario);
			foreach ($dataProspectos as $p) {
				$dataProducto = $this->ProductosModel->get_by_id($p->idproducto);
				$p->producto = $dataProducto[0];
				$dataUsuarios = $this->UsuariosModel->get_by_id($p->asesor);
				$p->nasesor = $dataUsuarios[0];
			}

			$dataProspectos[0]->telefono = substr($dataProspectos[0]->telefono, 4);

			$data = array(
				"prospecto" => $dataProspectos,
				"asesores" => $dataAsesores,
				"productos" => $dataProductos,
				"paginaanterior" => $pagina_anterior
			);
			$this->load->view("prospecto_edicion", $data);
		}


	}

	public function get_where()
	{
		$dataWhere = $this->input->post();
		$dataProspecto = $this->ProspectosModel->get_where($dataWhere);
		if (count($dataProspecto) >= 1) {
			$dataUsuarios = $this->UsuariosModel->get_by_id($dataProspecto[0]->asesor);
			$dataProspecto[0]->nasesor = $dataUsuarios[0];
			$respuesta = array(
				"respuesta" => 101,
				"prospecto" => $dataProspecto
			);
		} else {
			$respuesta = array(
				"respuesta" => 102
			);
		}

		echo json_encode($respuesta);
	}

	public function enviarcorreo($nombre = '', $correo = '', $asunto = "", $cuerpo = "", $adjuntos = null)
	{
		$mail = new PHPMailer(); // create a new object
		$mail->IsSMTP(); // enable SMTP
		$mail->CharSet = "UTF-8";
		$mail->SMTPAuth = true;
		$mail->SMTPSecure = 'ssl';
		$mail->Host = 'mail.miskuentascrm.com';
		$mail->Port = 465;
		$mail->Username = 'noreply@miskuentascrm.com';
		$mail->Password = 'obiwankenobi2020';
		$mail->SMTPAuth = true;

		if ($adjuntos != null) {
			for ($i = 0; $i < count($adjuntos); $i++) {
				$mail->addAttachment($adjuntos[$i]);
			}
		}
		$mail->setFrom('info@miskuentascrm.com', 'Miskuentas');
		$mail->AddAddress($correo, $nombre);
		$mail->IsHTML(true);
		$mail->Subject = $asunto;
		$mensaje = '
        	<body>
    			<div class="container">' . $cuerpo . '
    			</div>
			</body>';
		$mail->MsgHTML($mensaje);
		$mail->send();
	}

	public function msn_send($mensaje, $numero)
	{
		$auth_basic = base64_encode("francisco@miskuentas.com:arellano1");
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api.labsmobile.com/json/send",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => '{"message":"' . $mensaje . '", "tpoa":"MISKUENTAS","recipient":[{"msisdn":"' . $numero . '"}]}',
			CURLOPT_HTTPHEADER => array(
				"Authorization: Basic " . $auth_basic,
				"Cache-Control: no-cache",
				"Content-Type: application/json"
			),
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		/*if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			echo $response;
		}*/
	}

	public function reinicia_envios()
	{
		$dataInfo = $this->ProspectosModel->get_where(array("status" => 1));
		foreach ($dataInfo as $p) {
			$this->ProspectosModel->edit($p->idprospecto, array("smsenviado" => 0, "correoenviado" => 0));
		}
	}
	//--------------------------------------------------------------------

}
