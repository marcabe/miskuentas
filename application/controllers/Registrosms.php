<?php
defined('BASEPATH') or exit('No direct script access allowed');

date_default_timezone_set("America/Mexico_City");


class Registrosms extends CI_Controller
{
	public function __construct()
	{

		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->hoy = date("Y-m-d");
		$this->load->library('session');
		$this->hoy = date("Y-m-d");
		$this->load->Model('RegistrosmsModel');
		$this->load->Model('UsuariosModel');
		$this->load->Model('SmsModel');
		$this->load->Model('ProspectosModel');

	}


	public function index($fecha='')
	{

		$dataRegSms = $this->RegistrosmsModel->get_where(array("fecha"=>$fecha));
		foreach ($dataRegSms as $rs){
			$dataProspecto = $this->ProspectosModel->get_where(array("idprospecto"=>$rs->idprospecto));
			$rs->prospecto = $dataProspecto[0];

			$dataSms = $this->SmsModel->get_where(array("idsms"=>$rs->idsms));
			$rs->sms = $dataSms[0];
		}
		$data = array(
			"registros"=>$dataRegSms,
			"fecha"=>$fecha
		);
		$this->load->view("lista_registrosms",$data);

	}


	//--------------------------------------------------------------------

}
