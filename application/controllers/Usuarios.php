<?php
defined('BASEPATH') or exit('No direct script access allowed');

date_default_timezone_set("America/Mexico_City");


class Usuarios extends CI_Controller
{
	public function __construct()
	{

		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->Model('UsuariosModel');
		$this->load->Model('UsuariosModel');
		$this->load->Model('HistorialModel');
		$this->load->Model('ProductosModel');
		$this->load->Model('ProspectosModel');

		$this->hoy = date("Y-m-d");
		$this->key = "obiwakenobi2020";
		$this->finicio = date("Y") . "-" . date("m") . "-" . "01";
		$this->ffinal = date("Y") . "-" . date("m") . "-" . date("t", strtotime("01-" . date("m") . "-" . date("Y")));
		$this->diavencer = date("Y-m-d", strtotime($this->hoy . "+ 3 days"));
	}

	public function home()
	{
		if ($this->session->userdata('log_in') == true) {
			$mes = date("m");
			$graficaProductos = array();
			$graficaStatus = array();
			if ($this->session->userdata('rol') == 1) {
				$dataTotalProspectos = $this->ProspectosModel->get_where(array("MONTH(fechacreate)" => $mes, "status" => 1));
				$dataAlertas = $this->ProspectosModel->get_where(array("fechaalerta" => $this->hoy, "status" => 1));
				$dataGanados = $this->ProspectosModel->get_where(array("fechaganado >=" => $this->finicio, "fechaganado <=" => $this->ffinal, "status" => 2));
				$dataPorVencer = $this->ProspectosModel->get_where(array("pruebaf" => $this->diavencer, "status" => 1));
				$dataVencidos = $this->ProspectosModel->get_where(array("pruebaf <" => $this->hoy, "status" => 1));
				$dataPerdidos = $this->ProspectosModel->get_where(array("MONTH(fechaperdido)" => $mes, "status" => 3));
				$dataProductos = $this->ProductosModel->get();
				foreach ($dataProductos as $p) {
					$dataProspecto = $this->ProspectosModel->get_where(array("idproducto" => $p->idproducto, "fechaganado >=" => $this->finicio, "fechaganado <=" => $this->ffinal, "status" => 2));
					if (count($dataProspecto) > 0) {
						$graficaProductos[] = array("producto" => $p->nproducto, "number" => count($dataProspecto));
					} else {
						$graficaProductos[] = array("producto" => $p->nproducto, "number" => 0);
					}
				}
				if (count($dataTotalProspectos) > 0) {
					$graficaStatus[] = array("status" => "Activos", "tot" => count($dataTotalProspectos));
				} else {
					$graficaStatus[] = array("status" => "Activos", "tot" => 0);
				}
				if (count($dataGanados) > 0) {
					$graficaStatus[] = array("status" => "Ganado", "tot" => count($dataGanados));
				} else {
					$graficaStatus[] = array("status" => "Ganado", "tot" => 0);
				}

				if (count($dataPerdidos) > 0) {
					$graficaStatus[] = array("status" => "Perdido", "tot" => count($dataPerdidos));
				} else {
					$graficaStatus[] = array("status" => "Perdido", "tot" => 0);
				}
			} else {
				$correo = $this->session->userdata("correo");
				$dataTotalProspectos = $this->ProspectosModel->get_where(array("MONTH(fechacreate)" => $mes, "asesor" => $correo, "status" => 1));
				$dataAlertas = $this->ProspectosModel->get_where(array("fechaalerta" => $this->hoy, "status" => 1, "asesor" => $correo));
				$dataGanados = $this->ProspectosModel->get_where(array("fechaganado >=" => $this->finicio, "fechaganado <=" => $this->ffinal, "status" => 2, "asesor" => $correo));
				$dataPorVencer = $this->ProspectosModel->get_where(array("pruebaf" => $this->diavencer, "status" => 1, "asesor" => $correo));
				$dataVencidos = $this->ProspectosModel->get_where(array("pruebaf <" => $this->hoy, "status" => 1, "asesor" => $correo));
				$dataPerdidos = $this->ProspectosModel->get_where(array("MONTH(fechaperdido)" => $mes, "asesor" => $correo, "status" => 3));
				$dataProductos = $this->ProductosModel->get();

				foreach ($dataProductos as $p) {
					$dataProspecto = $this->ProspectosModel->get_where(array("idproducto" => $p->idproducto, "fechaganado >=" => $this->finicio, "fechaganado <=" => $this->ffinal, "status" => 2, "asesor" => $correo));
					if (count($dataProspecto) > 0) {
						$graficaProductos[] = array("producto" => $p->nproducto, "number" => count($dataProspecto));
					} else {
						$graficaProductos[] = array("producto" => $p->nproducto, "number" => 0);
					}
				}


				$dataProspectosActivos = $this->ProspectosModel->get_where(array("status" => 1, "MONTH(fechacreate)" => $mes, "asesor" => $correo));
				if (count($dataProspectosActivos) > 0) {
					$graficaStatus[] = array("status" => "Activos", "tot" => count($dataProspectosActivos));
				} else {
					$graficaStatus[] = array("status" => "Activos", "tot" => 0);

				}
				$dataProspectosPerdidos = $this->ProspectosModel->get_where(array("status" => 2, "fechaganado >=" => $this->finicio, "fechaganado <=" => $this->ffinal, "asesor" => $correo));
				if (count($dataProspectosPerdidos) > 0) {
					$graficaStatus[] = array("status" => "Ganado", "tot" => count($dataProspectosPerdidos));
				} else {
					$graficaStatus[] = array("status" => "Ganado", "tot" => 0);

				}
				$dataProspectosGanados = $this->ProspectosModel->get_where(array("status" => 3, "MONTH(fechacreate)" => $mes, "asesor" => $correo));
				if (count($dataProspectosGanados) > 0) {
					$graficaStatus[] = array("status" => "Perdido", "tot" => count($dataProspectosGanados));
				} else {
					$graficaStatus[] = array("status" => "Perdido", "tot" => 0);

				}

			}


			$data = array(
				"mes"=> $mes,
				"totalprospectos" => count($dataTotalProspectos),
				"totalalertas" => count($dataAlertas),
				"totalganados" => count($dataGanados),
				"totalvencer" => count($dataPorVencer),
				"totalvencidos" => count($dataVencidos),
				"totalperdidos" => count($dataPerdidos),
				"graficaProductos" => json_encode($graficaProductos),
				"graficaStatus" => json_encode($graficaStatus)

			);
			$this->load->view('dashboard', $data);
		} else {
			redirect("../");
		}
	}

	public function dashboardmensual($mes){
		$finicio = date("Y") . "-" . $mes . "-" . "01";
		$ffinal = date("Y") . "-" . $mes . "-" . date("t", strtotime("01-" . $mes . "-" . date("Y")));
		$graficaProductos = array();
		$graficaStatus = array();
		if ($this->session->userdata('rol') == 1) {
			$dataTotalProspectos = $this->ProspectosModel->get_where(array("MONTH(fechacreate)" => $mes, "status" => 1));
			$dataGanados = $this->ProspectosModel->get_where(array("fechaganado >=" => $finicio, "fechaganado <=" => $ffinal, "status" => 2));
			$dataPerdidos = $this->ProspectosModel->get_where(array("MONTH(fechaperdido)" => $mes, "status" => 3));
			$dataProductos = $this->ProductosModel->get();
			foreach ($dataProductos as $p) {
				$dataProspecto = $this->ProspectosModel->get_where(array("idproducto" => $p->idproducto, "fechaganado >=" => $finicio, "fechaganado <=" => $ffinal, "status" => 2));
				if (count($dataProspecto) > 0) {
					$graficaProductos[] = array("producto" => $p->nproducto, "number" => count($dataProspecto));
				} else {
					$graficaProductos[] = array("producto" => $p->nproducto, "number" => 0);
				}
			}
			if (count($dataTotalProspectos) > 0) {
				$graficaStatus[] = array("status" => "Activos", "tot" => count($dataTotalProspectos));
			} else {
				$graficaStatus[] = array("status" => "Activos", "tot" => 0);
			}
			if (count($dataGanados) > 0) {
				$graficaStatus[] = array("status" => "Ganado", "tot" => count($dataGanados));
			} else {
				$graficaStatus[] = array("status" => "Ganado", "tot" => 0);
			}
			if (count($dataPerdidos) > 0) {
				$graficaStatus[] = array("status" => "Perdido", "tot" => count($dataPerdidos));
			} else {
				$graficaStatus[] = array("status" => "Perdido", "tot" => 0);
			}
		}else{
			$correo = $this->session->userdata("correo");

			$dataTotalProspectos = $this->ProspectosModel->get_where(array("MONTH(fechacreate)" => $mes, "status" => 1, "asesor" => $correo));
			$dataGanados = $this->ProspectosModel->get_where(array("fechaganado >=" => $finicio, "fechaganado <=" => $ffinal, "status" => 2, "asesor" => $correo));
			$dataPerdidos = $this->ProspectosModel->get_where(array("MONTH(fechaperdido)" => $mes, "status" => 3, "asesor" => $correo));
			$dataProductos = $this->ProductosModel->get();
			foreach ($dataProductos as $p) {
				$dataProspecto = $this->ProspectosModel->get_where(array("idproducto" => $p->idproducto, "fechaganado >=" => $finicio, "fechaganado <=" => $ffinal, "status" => 2, "asesor" => $correo));
				if (count($dataProspecto) > 0) {
					$graficaProductos[] = array("producto" => $p->nproducto, "asesor" => $correo, "number" => count($dataProspecto));
				} else {
					$graficaProductos[] = array("producto" => $p->nproducto, "number" => 0, "asesor" => $correo);
				}
			}
			if (count($dataTotalProspectos) > 0) {
				$graficaStatus[] = array("status" => "Activos", "tot" => count($dataTotalProspectos));
			} else {
				$graficaStatus[] = array("status" => "Activos", "tot" => 0);
			}
			if (count($dataGanados) > 0) {
				$graficaStatus[] = array("status" => "Ganado", "tot" => count($dataGanados));
			} else {
				$graficaStatus[] = array("status" => "Ganado", "tot" => 0);
			}
			if (count($dataPerdidos) > 0) {
				$graficaStatus[] = array("status" => "Perdido", "tot" => count($dataPerdidos));
			} else {
				$graficaStatus[] = array("status" => "Perdido", "tot" => 0);
			}
		}

		$data = array(
			"mes"=> $mes,
			"totalprospectos" => count($dataTotalProspectos),
			"totalganados" => count($dataGanados),
			"totalperdidos" => count($dataPerdidos),
			"graficaProductos" => $graficaProductos,
			"graficaStatus" => $graficaStatus,
			"respuesta"=>1
		);

		echo json_encode($data);
	}

	public function login()
	{
		if ($this->session->userdata('log_in') == true) {
			$this->HistorialModel->insert(array("movimiento" => "Salio del sistea", "correo" => $this->session->userdata("correo"), "fecha" => $this->hoy, "hora" => date("H:i")));
			$this->session->sess_destroy();
			$this->load->view('login');
		} else {
			$this->session->sess_destroy();
			$this->load->view('login');

		}
	}

	public function index()
	{
		if ($this->session->userdata('log_in') == true) {
			$dataUsuarios = $this->UsuariosModel->get();
			$data = array(
				"usuarios" => $dataUsuarios
			);
			$this->load->view('lista_usuarios', $data);
		} else {
			redirect("../");
		}
	}

	public function historial_filtro($fi, $ff, $correo)
	{
		$dataMovimientos = $this->HistorialModel->get_where(array("fecha>=" => $fi, "fecha<=" => $ff, "correo" => $correo));
		$dataUsuario = $this->UsuariosModel->get_by_id($correo);
		$data = array(
			"movimientos" => $dataMovimientos,
			"usuario" => $dataUsuario
		);
		$this->load->view("usuarios_historial_filtro", $data);
	}

	public function update($correo = 0)
	{
		$dataUpdate = $this->input->post();
		if (isset($dataUpdate['clave'])) {
			$dataUpdate['clave'] = md5($dataUpdate['clave']);
			$mensaje = "Se edito la clave del usuario: ";
		} elseif (isset($dataUpdate['status'])) {
			$mensaje = "Se edito el status del usuario: ";
		}
		$this->UsuariosModel->edit($correo, $dataUpdate);
		$this->HistorialModel->insert(array("movimiento" => $mensaje . $correo, "correo" => $this->session->userdata("correo"), "fecha" => $this->hoy, "hora" => date("H:i")));
	}


	public function nuevo()
	{
		if ($this->session->userdata('log_in') == true) {
			#Obtenemos el nombre de los asesores
			$this->load->view('nuevo_usuario');
		} else {
			redirect("../");
		}
	}


	public function insert()
	{
		$dataResponse = $this->input->post();
		$dataResponse['clave'] = md5($dataResponse['clave']);
		$this->UsuariosModel->insert($dataResponse);
		$this->HistorialModel->insert(array("movimiento" => "Ingreso al usuairo" . $dataResponse['correo'], "correo" => $this->session->userdata("correo"), "fecha" => $this->hoy, "hora" => date("H:i")));

		echo 1;
	}


	public function valida_login()
	{
		$data = $this->input->post();
		$where = array(
			"correo" => $data['correo']
		);
		$datosUsua = $this->UsuariosModel->get_where($where);
		if (count($datosUsua) > 0 && $datosUsua[0]->status == 1) {
			$data['clave'] = md5($data['clave']);
			$response = $this->UsuariosModel->get_where($data);
			if (count($response) > 0) {
				$ip = $this->ObtenerIP();
				$arraydata = array(
					'usuario' => $response[0]->nombre,
					'correo' => $response[0]->correo,
					'rol' => $response[0]->idrol,
					'ip' => $ip,
					'log_in' => true
				);
				//$this->session->sess_expiration = '1200'; // La sesión termina en 20 minutos
				$this->session->set_userdata($arraydata);
				$this->HistorialModel->insert(array("movimiento" => "Ingreso al sistea desde la ip $ip", "correo" => $response[0]->correo, "fecha" => $this->hoy, "hora" => date("H:i")));
				echo 2;
			} else {
				#La clave no esta bien
				echo 1;
			}
		} else {
			#El usuario no existe
			echo 0;
		}
	}

	function ObtenerIP()
	{
		if (isset($_SERVER)) {
			if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
				$ip = $_SERVER['HTTP_CLIENT_IP'];
			} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
				$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
			} else {
				$ip = $_SERVER['REMOTE_ADDR'];
			}
		} else {
			if (getenv('HTTP_CLIENT_IP')) {
				$ip = getenv('HTTP_CLIENT_IP');
			} elseif (getenv('HTTP_X_FORWARDED_FOR')) {
				$ip = getenv('HTTP_X_FORWARDED_FOR');
			} else {
				$ip = getenv('REMOTE_ADDR');
			}
		}

		// En algunos casos muy raros la ip es devuelta repetida dos veces separada por coma

		if (strstr($ip, ',')) {
			$ip = array_shift(explode(',', $ip));
		}
		return $ip;
	}

	public function logout()
	{
		$this->HistorialModel->insert(array("movimiento" => "Salio del sistea desde la ip $ip", "correo" => $this->session->userdata("correo"), "fecha" => $this->hoy, "hora" => date("H:i")));
		$this->load->view('login');

	}

	public function historial($usuario = 0)
	{
		$dataMovimientos = $this->HistorialModel->get_where(array("correo" => $usuario));
		$dataUsuario = $this->UsuariosModel->get_by_id($usuario);
		$data = array(
			"movimientos" => $dataMovimientos,
			"usuario" => $dataUsuario
		);
		$this->load->view("usuarios_historial", $data);
	}


	public function obtiene_mes($mes){
		switch ($mes){
			case 1:
				return "Enero";
				break;
			case 2:
				return "Febrero";
				break;
			case 3:
				return "Marzo";
				break;
			case 4:
				return "Abril";
				break;
			case 5:
				return "Mayo";
				break;
			case 6:
				return "Junio";
				break;
			case 7:
				return "Julio";
				break;
			case 8:
				return "Agosto";
				break;
			case 9:
				return "Septiembre";
				break;
			case 10:
				return "Octubre";
				break;
			case 11:
				return "Noviembre";
				break;
			case 12:
				return "Diciembre";
				break;
		}

	}


}
