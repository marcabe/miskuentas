<?php
defined('BASEPATH') or exit('No direct script access allowed');

date_default_timezone_set("America/Mexico_City");


class Sms extends CI_Controller
{
	public function __construct()
	{

		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->hoy = date("Y-m-d");
		$this->load->library('session');
		$this->hoy = date("Y-m-d");
		$this->load->Model('SmsModel');
		$this->load->Model('UsuariosModel');
		$this->load->Model('RegistrosSmsModel');
		$this->load->Model('ProspectosModel');

	}

	public function index()
	{
		if ($this->session->userdata('log_in') == true) {
			$datasms = $this->SmsModel->get();
			$data = array(
				"sms" => $datasms
			);
			$this->load->view("lista_sms", $data);

		}
	}

	public function update($idsms = 0)
	{
		$dataUpdate = $this->input->post();
		$this->SmsModel->edit($idsms, $dataUpdate);
		echo 1;
	}


	public function masivo()
	{
		$ok = 0;
		$err = 0;
		$i = 0;
		$dataInfo = $this->ProspectosModel->get_where(array("status" => 1, "smsenviado" => 0));
		foreach ($dataInfo as $p) {
			if ($p->idsms != '') {
				$datasms = $this->SmsModel->get_by_id($p->idsms);
				$numero = str_replace("+521 ", "", $p->telefono);
				$respuesta = $this->msn_send($datasms[0]->sms, $numero);
				if ($respuesta["status"] == 1) {
					$ok++;
				} elseif ($respuesta["status"] == 0) {
					$err++;
				}
				$this->RegistrosSmsModel->insert(array("idprospecto" => $p->idprospecto, "idsms" => $p->idsms, "fecha" => $this->hoy, "hora" => date("H:i"), "statusenvio" => $respuesta["mensaje"]));
				if(($p->idsms + 1)<=15) {
                    $this->ProspectosModel->edit($p->idprospecto, array("idsms" => ($p->idsms + 1), "smsenviado" => 1));
                }elseif(($p->idsms + 1)==15) {
					$this->ProspectosModel->edit($p->idprospecto, array("idsms" => 1, "smsenviado" => 1));
				}
			} else {
				$date1 = new DateTime($p->fechacreate);
				$date2 = new DateTime($this->hoy);
				$diff = $date1->diff($date2);
				$datasms = $this->SmsModel->get_by_id($diff->days);
				if (count($datasms) > 0) {
					if ($datasms[0]->sms != '') {
						$respuesta = $this->msn_send($datasms[0]->sms, $numero);
						if ($respuesta["status"] == 1) {
							$ok++;
						} elseif ($respuesta["status"] == 0) {
							$err++;
						}
						$this->RegistrosSmsModel->insert(array("idprospecto" => $p->idprospecto, "idsms" => $diff->days, "fecha" => $this->hoy, "hora" => data("H:i"), "statusenvio" => $respuesta["mensaje"]));
						if(($diff->days + 1)<=15) {
                            $this->ProspectosModel->edit($p->idprospecto, array("idsms" => ($diff->days + 1), "smsenviado" => 1));
                        }elseif(($diff->days + 1)==16){
							$this->ProspectosModel->edit($p->idprospecto, array("idsms" => 1, "smsenviado" => 1));
						}
					}
				}
			}
			$i++;
		}

		//$this->msn_send("Mensjae interno del sistema CRM: se enviaron ".$ok." mensaje(s) correctamente y ".$err." mensaje(s) erroneos el dia de hoy", "2221346270");
		//$this->msn_send("Mensjae interno del sistema CRM: se enviaron ".$ok." mensaje(s) correctamente y ".$err." mensaje(s) erroneos el dia de hoy", "3323684401");

	}

	public function msn_send($mensaje, $numero)
	{
		$auth_basic = base64_encode("francisco@miskuentas.com:arellano1");
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api.labsmobile.com/json/send",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => '{"message":"' . $mensaje . '", "tpoa":"MISKUENTAS","recipient":[{"msisdn":"' . $numero . '"}]}',
			CURLOPT_HTTPHEADER => array(
				"Authorization: Basic " . $auth_basic,
				"Cache-Control: no-cache",
				"Content-Type: application/json"
			),
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			return array("mensaje" => "cURL Error #:" . $err, "status" => 0);
		} else {
			return array("mensaje" => "Mensaje enviado correctamente", "status" => 1);
		}
	}
	//--------------------------------------------------------------------
}
