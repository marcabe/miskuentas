<?php
defined('BASEPATH') or exit('No direct script access allowed');

date_default_timezone_set("America/Mexico_City");


class Historialventasasesor extends CI_Controller
{
	public function __construct()
	{

		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->hoy = date("Y-m-d");
		$this->load->library('session');
		$this->hoy = date("Y-m-d");
		$this->load->Model('HistorialventasasesoresModel');
		$this->load->Model('UsuariosModel');
		$this->load->Model('ProspectosModel');

	}

	public function index()
	{
		$dataUsuarios = $this->UsuariosModel->get_where(array("status"=>1));
		$data = array(
			"usuarios"=>$dataUsuarios
		);

		$this->load->view("lista_historialventasasesor", $data);
	}


	public function match()
	{
		$dataUsuarios = $this->UsuariosModel->get_where(array("status"=>1));
		$data = array(
			"usuarios"=>$dataUsuarios
		);
		$this->load->view("match_asesores", $data);
	}

	public function get_match()
	{
		$dataMatch = array();
		for ($i = 1; $i <= 12; $i++) {
			$info = new \stdClass;

			$dataVentaA = $this->HistorialventasasesoresModel->get_where(array("anio" => $this->input->post('anio'),"asesor" => $this->input->post('asesora'), "mes" => $i));
			$dataVentaB = $this->HistorialventasasesoresModel->get_where(array("anio" => $this->input->post('anio'),"asesor" => $this->input->post('asesorb'), "mes" => $i));
			switch ($i) {
				case 1:
					$info->nmes = "Enero";
					if(count($dataVentaA)<=0){
						$info->asesora = 0;
					}else{
						$info->asesora = floatval($dataVentaA[0]->ventas);
					}
					if(count($dataVentaB)<=0){
						$info->asesorb = 0;
					}else{
						$info->asesorb = floatval($dataVentaB[0]->ventas);
					}
					break;
				case 2:
					$info->nmes = "Febrero";
					if(count($dataVentaA)<=0){
						$info->asesora = 0;
					}else{
						$info->asesora = floatval($dataVentaA[0]->ventas);
					}
					if(count($dataVentaB)<=0){
						$info->asesorb = 0;
					}else{
						$info->asesorb = floatval($dataVentaB[0]->ventas);
					}
					break;
				case 3:
					$info->nmes = "Marzo";
					if(count($dataVentaA)<=0){
						$info->asesora = 0;
					}else{
						$info->asesora = floatval($dataVentaA[0]->ventas);
					}
					if(count($dataVentaB)<=0){
						$info->asesorb = 0;
					}else{
						$info->asesorb = floatval($dataVentaB[0]->ventas);
					}
					break;
				case 4:
					$info->nmes = "Abril";
					if(count($dataVentaA)<=0){
						$info->asesora = 0;
					}else{
						$info->asesora = floatval($dataVentaA[0]->ventas);
					}
					if(count($dataVentaB)<=0){
						$info->asesorb = 0;
					}else{
						$info->asesorb = floatval($dataVentaB[0]->ventas);
					}
					break;
				case 5:
					$info->nmes = "Mayo";
					if(count($dataVentaA)<=0){
						$info->asesora = 0;
					}else{
						$info->asesora = floatval($dataVentaA[0]->ventas);
					}
					if(count($dataVentaB)<=0){
						$info->asesorb = 0;
					}else{
						$info->asesorb = floatval($dataVentaB[0]->ventas);
					}
					break;
				case 6:
					$info->nmes = "Junio";
					if(count($dataVentaA)<=0){
						$info->asesora = 0;
					}else{
						$info->asesora = floatval($dataVentaA[0]->ventas);
					}
					if(count($dataVentaB)<=0){
						$info->asesorb = 0;
					}else{
						$info->asesorb = floatval($dataVentaB[0]->ventas);
					}
					break;
				case 7:
					$info->nmes = "Julio";
					if(count($dataVentaA)<=0){
						$info->asesora = 0;
					}else{
						$info->asesora = floatval($dataVentaA[0]->ventas);
					}
					if(count($dataVentaB)<=0){
						$info->asesorb = 0;
					}else{
						$info->asesorb = floatval($dataVentaB[0]->ventas);
					}
					break;
				case 8:
					$info->nmes = "Agosto";
					if(count($dataVentaA)<=0){
						$info->asesora = 0;
					}else{
						$info->asesora = floatval($dataVentaA[0]->ventas);
					}
					if(count($dataVentaB)<=0){
						$info->asesorb = 0;
					}else{
						$info->asesorb = floatval($dataVentaB[0]->ventas);
					}
					break;
				case 9:
					$info->nmes = "Septiembre";
					if(count($dataVentaA)<=0){
						$info->asesora = 0;
					}else{
						$info->asesora = floatval($dataVentaA[0]->ventas);
					}
					if(count($dataVentaB)<=0){
						$info->asesorb = 0;
					}else{
						$info->asesorb = floatval($dataVentaB[0]->ventas);
					}
					break;
				case 10:
					$info->nmes = "Octubre";
					if(count($dataVentaA)<=0){
						$info->asesora = 0;
					}else{
						$info->asesora = floatval($dataVentaA[0]->ventas);
					}
					if(count($dataVentaB)<=0){
						$info->asesorb = 0;
					}else{
						$info->asesorb = floatval($dataVentaB[0]->ventas);
					}
					break;
				case 11:
					$info->nmes = "Nomviembre";
					if(count($dataVentaA)<=0){
						$info->asesora = 0;
					}else{
						$info->asesora = floatval($dataVentaA[0]->ventas);
					}
					if(count($dataVentaB)<=0){
						$info->asesorb = 0;
					}else{
						$info->asesorb = floatval($dataVentaB[0]->ventas);
					}
					break;
				case 12:
					$info->nmes = "Diciembre";
					if(count($dataVentaA)<=0){
						$info->asesora = 0;
					}else{
						$info->asesora = floatval($dataVentaA[0]->ventas);
					}
					if(count($dataVentaB)<=0){
						$info->asesorb = 0;
					}else{
						$info->asesorb = floatval($dataVentaB[0]->ventas);
					}
					break;
			}
			$dataMatch[] = $info;
		}

		echo json_encode($dataMatch);

	}

	public function insert()
	{
		$dataResponse = $this->input->post();
		$dataHistorial = $this->HistorialventasasesoresModel->get_where(array("anio" => $dataResponse['anio'], "mes" => $dataResponse['mes']));
		if (count($dataHistorial) <= 0) {
			echo $this->HistorialventasasesoresModel->insert($dataResponse);
		} else {
			$this->HistorialventasasesoresModel->edit($dataHistorial[0]->idhistorial, array("ventas" => $dataResponse['ventas']));
			echo 1;
		}

	}


	public function get_where()
	{
		$dataWhere = $this->input->post();
		$dataHistorial = $this->HistorialventasasesoresModel->get_where($dataWhere);
		if (count($dataHistorial) > 0) {
			$respuesta = array(
				"historial" => $dataHistorial[0],
				"respuesta" => 1
			);
		} else {
			$respuesta = array(
				"historial" => null,
				"respuesta" => 0
			);
		}

		echo json_encode($respuesta);
	}

	public function get_historial_table($anio,$asesor='')
	{
		$dataAnio = array();
		for ($i = 1; $i <= 12; $i++) {
			$dataVenta = $this->HistorialventasasesoresModel->get_where(array("anio" => $anio, "mes" => $i, "asesor"=>$asesor));
			if (count($dataVenta) <= 0) {
				$info = new \stdClass;
				$info->anio = $anio;
				$info->ventas = 0;
				switch ($i) {
					case 1:
						$info->nmes = "Enero";
						break;
					case 2:
						$info->nmes = "Febrero";
						break;
					case 3:
						$info->nmes = "Marzo";
						break;
					case 4:
						$info->nmes = "Abril";
						break;
					case 5:
						$info->nmes = "Mayo";
						break;
					case 6:
						$info->nmes = "Junio";
						break;
					case 7:
						$info->nmes = "Julio";
						break;
					case 8:
						$info->nmes = "Agosto";
						break;
					case 9:
						$info->nmes = "Septiembre";
						break;
					case 10:
						$info->nmes = "Octubre";
						break;
					case 11:
						$info->nmes = "Nomviembre";
						break;
					case 12:
						$info->nmes = "Diciembre";
						break;
				}
				$dataAnio[] = $info;
			} else {
				$dataAnio[] = $dataVenta[0];
			}
		}
		echo json_encode($dataAnio);
	}




	//--------------------------------------------------------------------

}
