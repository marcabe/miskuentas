<?php
defined('BASEPATH') or exit('No direct script access allowed');

date_default_timezone_set("America/Mexico_City");


class Productos extends CI_Controller
{
	public function __construct()
	{

		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->hoy = date("Y-m-d");
		$this->load->library('session');
		$this->hoy = date("Y-m-d");
		$this->load->Model('ProductosModel');
		$this->load->Model('ProspectosModel');

	}


	public function index()
	{
		if ($this->session->userdata('log_in') == true) {
			$dataProductos = $this->ProductosModel->get();
			$data = array(
				"productos"=>$dataProductos
			);
			$this->load->view('lista_productos', $data);
		}else{
			redirect("../");
		}
	}

	public function update($idproducto = 0)
	{
		$dataUpdate = $this->input->post();
		$this->ProductosModel->edit($idproducto, $dataUpdate);

	}

	public function nuevo(){
		if ($this->session->userdata('log_in') == true) {
			$this->load->view('nuevo_producto');
		} else {
			redirect("../");
		}
	}

	public function insert()
	{
		$dataResponse = $this->input->post();
		echo  $this->ProductosModel->insert($dataResponse);

	}


	//--------------------------------------------------------------------

}
