<?php
defined('BASEPATH') or exit('No direct script access allowed');

date_default_timezone_set("America/Mexico_City");


class Imgc extends CI_Controller
{
	public function __construct()
	{

		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->hoy = date("Y-m-d");
		$this->load->library('session');
		$this->hoy = date("Y-m-d");
		$this->load->model("ImgcorreosModel");
		$this->load->Model('ProspectosModel');

	}

	function index()
	{
		# code...
	}

	public function insert()
	{
		$ruta = 'assets/imgcorreos'; //Decalaramos una variable con la ruta en donde almacenaremos los archivos
		$idcorreo = $this->input->post("idcorreo");
		foreach ($_FILES as $key) //Iteramos el arreglo de archivos
		{
			if($key['error'] == UPLOAD_ERR_OK )//Si el archivo se paso correctamente Ccontinuamos 
			{
					$nombreOriginal = $key['name'];//Obtenemos el nombre original del archivo
					$temporal = $key['tmp_name']; //Obtenemos la ruta Original del archivo
					$destino = $ruta."/".basename($nombreOriginal);	//Creamos una ruta de destino con la variable ruta y el nombre original del archivo	
					move_uploaded_file($temporal, $destino); //Movemos el archivo temporal a la ruta especificada	
					$data = array(
						'ruta' => $destino,
						'idcorreo'=> $idcorreo
					);
					$this->ImgcorreosModel->insert($data);
			}	
		}
	}

	public function delete(){
		$deleteWhere = $this->input->post();
		$this->ImgcorreosModel->delete_where($deleteWhere);
		echo json_encode(array('respuesta' => 101 ));
	}

}
