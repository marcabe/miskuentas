<?php
defined('BASEPATH') or exit('No direct script access allowed');

date_default_timezone_set("America/Mexico_City");


class Correos extends CI_Controller
{
	public function __construct()
	{

		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->hoy = date("Y-m-d");
		$this->load->library('session');
		$this->hoy = date("Y-m-d");
		$this->load->model("CorreosModel");
		$this->load->model("ImgcorreosModel");
		$this->load->model("UsuariosModel");
		$this->load->model("RegistrocorreoModel");
		$this->load->Model('ProspectosModel');

		include_once('mailer/src/class.phpmailer.php');
		include_once('mailer/src/class.smtp.php');

	}


	public function index()
	{
		$dataCorreos = $this->CorreosModel->get();
		foreach ($dataCorreos as $c) {
			$c->correosn = strip_tags($c->correo);

		}
		$data = array(
			"correos" => $dataCorreos
		);
		$this->load->view("lista_correos", $data);
	}

	public function get($idcorreo = 0)
	{
		if ($idcorreo != 0) {
			$dataCorreo = $this->CorreosModel->get_by_id($idcorreo);
			$dataImgCorreo = $this->ImgcorreosModel->get_where(array("idcorreo" => $idcorreo));
			$data = array(
				"correo" => $dataCorreo,
				"imgcorreos" => $dataImgCorreo,
				"respuesta" => 101
			);
			echo json_encode($data);
		}
	}

	public function update($idcorreo = 0)
	{
		if ($idcorreo != 0) {
			$this->CorreosModel->edit($idcorreo, $this->input->post());
			$data = array(
				"idcorreo" => $idcorreo,
				"respuesta" => 101
			);
			echo json_encode($data);
		}
	}


	public function masivo()
	{
		//$this->enviarcorreo("Marco Antonio", "youshimatz405@gmail.com", "Asuntillo","Cuepecillo");
		$dataInfo = $this->ProspectosModel->get_where(array("status" => 1, "correoenviado" => 0));
		foreach ($dataInfo as $p) {
			if ($p->idcorreo != '') {
				$adjuntos = array();
				$datacorreo = $this->CorreosModel->get_by_id($p->idcorreo);
				$dataAdjunto = $this->ImgcorreosModel->get_where(array("idcorreo" => $datacorreo[0]->idcorreo));
				foreach ($dataAdjunto as $c) {
					$adjuntos[] = $c->ruta;
				}
				if (count($adjuntos) > 0) {
					$respuesta = $this->enviarcorreo($p->nombre, $p->correo, $datacorreo[0]->asunto, $datacorreo[0]->correo, $adjuntos);
				} else {
					$respuesta = $this->enviarcorreo($p->nombre, $p->correo, $datacorreo[0]->asunto, $datacorreo[0]->correo);
				}

				$this->RegistrocorreoModel->insert(array("idprospecto" => $p->idprospecto, "idcorreo" => $p->idcorreo, "fecha" => $this->hoy, "hora" => date("H:i"), "statusenvio" => $respuesta["mensaje"]));
				if(($p->idcorreo+1)<=15) {
                    $this->ProspectosModel->edit($p->idprospecto, array("idcorreo" => ($p->idcorreo + 1), "correoenviado" => 1));
                }elseif(($p->idcorreo+1)==15) {
					$this->ProspectosModel->edit($p->idprospecto, array("idcorreo" => 1, "correoenviado" => 1));
				}
			} else {
				$date1 = new DateTime($p->fechacreate);
				$date2 = new DateTime($this->hoy);
				$diff = $date1->diff($date2);
				$adjuntos = array();
				$datacorreo = $this->CorreosModel->get_by_id($diff->days);
				if (count($datacorreo) > 0) {
					$dataAdjunto = $this->ImgcorreosModel->get_where(array("idcorreo" => $datacorreo[0]->idcorreo));
					foreach ($dataAdjunto as $c) {
						$adjuntos[] = $c->ruta;
					}
					if (count($adjuntos) > 0) {
						$respuesta = $this->enviarcorreo($p->nombre, $p->correo, $datacorreo[0]->asunto, $datacorreo[0]->correo, $adjuntos);

					} else {
						$respuesta = $this->enviarcorreo($p->nombre, $p->correo, $datacorreo[0]->asunto, $datacorreo[0]->correo);
					}
					$this->RegistroCorreoModel->insert(array("idprospecto" => $p->idprospecto, "idcorreo" => $diff->days, "fecha" => $this->hoy, "hora" => data("H:i"), "statusenvio" => $respuesta["mensaje"]));

                    if(($diff->days+1)<=15) {
                        $this->ProspectosModel->edit($p->idprospecto, array("idcorreo" => ($diff->days + 1), "correoenviado" => 1));
                    }elseif(($diff->days+1)==15) {
						$this->ProspectosModel->edit($p->idprospecto, array("idcorreo" =>1, "correoenviado" => 1));
					}
				}

			}
		}
	}

	public function enviarcorreo($nombre = '', $correo = '', $asunto = "", $cuerpo = "", $adjuntos = null)
	{
		try {
			$mail = new PHPMailer(); // create a new object
			$mail->IsSMTP(); // enable SMTP
			$mail->CharSet = "UTF-8";
			$mail->SMTPAuth = true;
			$mail->SMTPSecure = 'ssl';
			$mail->Host = 'mail.miskuentascrm.com';
			$mail->Port = 465;
			$mail->Username = 'noreply@miskuentascrm.com';
			$mail->Password = 'obiwankenobi2020';
			$mail->SMTPAuth = true;

			if ($adjuntos != null) {
				for ($i = 0; $i < count($adjuntos); $i++) {
					$mail->addAttachment($adjuntos[$i]);
				}
			}
			$mail->setFrom('informacion@miskuentascrm.com', 'Miskuentas');
			$mail->AddAddress($correo, $nombre);
			$mail->IsHTML(true);
			$mail->Subject = $asunto;
			$mensaje = '
        	<body>
    			<div class="container">' . $cuerpo . '
    			</div>
			</body>';
			$mail->MsgHTML($mensaje);
			$mail->send();
			return array("mensaje" => "Mensaje enviado correctamente", "status" => 1);
		} catch (Exception $e) {
			return array("mensaje" => "cURL Error #:" . $mail->ErrorInfo, "status" => 0);
		}
	}

}
