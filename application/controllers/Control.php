<?php
defined('BASEPATH') or exit('No direct script access allowed');

date_default_timezone_set("America/Mexico_City");


class Control extends CI_Controller
{
	public function __construct()
	{

		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->hoy = date("Y-m-d");
		$this->load->library('session');
		$this->hoy = date("Y-m-d");
		$this->load->Model('ControlModel');
		$this->load->Model('ProspectosModel');

	}


	public function insert()
	{
		$dataResponse = $this->input->post();
		echo $this->ControlModel->insert(array("idprospecto" => $dataResponse['idprospecto'], "asesor" => $dataResponse['asesor'], "fecha" => $this->hoy));


	}


	//--------------------------------------------------------------------

}
