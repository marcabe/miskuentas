<?php
defined('BASEPATH') or exit('No direct script access allowed');

date_default_timezone_set("America/Mexico_City");


class Comentarios extends CI_Controller
{
	public function __construct()
	{

		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->hoy = date("Y-m-d");
		$this->load->library('session');
		$this->hoy = date("Y-m-d");
		$this->load->Model('ComentariosModel');
		$this->load->Model('HistorialModel');
		$this->load->Model('ProspectosModel');

	}


	public function insert()
	{
		$dataResponse = $this->input->post();
		$dataResponse['fecha'] = $this->hoy;
		$dataResponse['asesor'] = $this->session->userdata("correo");

		$this->HistorialModel->insert(array("movimiento" => "Se ingreso comentario para el usuario " . $dataResponse['nombre'], "correo" => $this->session->userdata("correo"), "fecha" => $this->hoy, "hora" => date("H:i")));
		unset($dataResponse['nombre']);
		echo $this->ComentariosModel->insert($dataResponse);

	}


	//--------------------------------------------------------------------

}
