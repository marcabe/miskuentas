<?php
defined('BASEPATH') or exit('No direct script access allowed');

date_default_timezone_set("America/Mexico_City");


class Historialventas extends CI_Controller
{
	public function __construct()
	{

		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->hoy = date("Y-m-d");
		$this->load->library('session');
		$this->hoy = date("Y-m-d");
		$this->load->Model('HistorialventasModel');
		$this->load->Model('ProspectosModel');


	}


	public function match()
	{
		$this->load->view("match");
	}

	public function get_match()
	{
		$dataMatch = array();
		for ($i = 1; $i <= 12; $i++) {
			$info = new \stdClass;

			$dataVentaA = $this->HistorialventasModel->get_where(array("anio" => $this->input->post('anioa'), "mes" => $i));
			$dataVentaB = $this->HistorialventasModel->get_where(array("anio" => $this->input->post('aniob'), "mes" => $i));

			switch ($i) {
				case 1:
					$info->nmes = "Enero";
					if (count($dataVentaA) <= 0) {
						$info->anioa = 0;
					} else {
						$info->anioa = floatval($dataVentaA[0]->ventas);
					}
					if (count($dataVentaB) <= 0) {
						$info->aniob = 0;
					} else {
						$info->aniob = floatval($dataVentaB[0]->ventas);
					}
					break;
				case 2:
					$info->nmes = "Febrero";
					if (count($dataVentaA) <= 0) {
						$info->anioa = 0;
					} else {
						$info->anioa = floatval($dataVentaA[0]->ventas);
					}
					if (count($dataVentaB) <= 0) {
						$info->aniob = 0;
					} else {
						$info->aniob = floatval($dataVentaB[0]->ventas);
					}
					break;
				case 3:
					$info->nmes = "Marzo";
					if (count($dataVentaA) <= 0) {
						$info->anioa = 0;
					} else {
						$info->anioa = floatval($dataVentaA[0]->ventas);
					}
					if (count($dataVentaB) <= 0) {
						$info->aniob = 0;
					} else {
						$info->aniob = floatval($dataVentaB[0]->ventas);
					}
					break;
				case 4:
					$info->nmes = "Abril";
					if (count($dataVentaA) <= 0) {
						$info->anioa = 0;
					} else {
						$info->anioa = floatval($dataVentaA[0]->ventas);
					}
					if (count($dataVentaB) <= 0) {
						$info->aniob = 0;
					} else {
						$info->aniob = floatval($dataVentaB[0]->ventas);
					}
					break;
				case 5:
					$info->nmes = "Mayo";
					if (count($dataVentaA) <= 0) {
						$info->anioa = 0;
					} else {
						$info->anioa = floatval($dataVentaA[0]->ventas);
					}
					if (count($dataVentaB) <= 0) {
						$info->aniob = 0;
					} else {
						$info->aniob = floatval($dataVentaB[0]->ventas);
					}
					break;
				case 6:
					$info->nmes = "Junio";
					if (count($dataVentaA) <= 0) {
						$info->anioa = 0;
					} else {
						$info->anioa = floatval($dataVentaA[0]->ventas);
					}
					if (count($dataVentaB) <= 0) {
						$info->aniob = 0;
					} else {
						$info->aniob = floatval($dataVentaB[0]->ventas);
					}
					break;
				case 7:
					$info->nmes = "Julio";
					if (count($dataVentaA) <= 0) {
						$info->anioa = 0;
					} else {
						$info->anioa = floatval($dataVentaA[0]->ventas);
					}
					if (count($dataVentaB) <= 0) {
						$info->aniob = 0;
					} else {
						$info->aniob = floatval($dataVentaB[0]->ventas);
					}
					break;
				case 8:
					$info->nmes = "Agosto";
					if (count($dataVentaA) <= 0) {
						$info->anioa = 0;
					} else {
						$info->anioa = floatval($dataVentaA[0]->ventas);
					}
					if (count($dataVentaB) <= 0) {
						$info->aniob = 0;
					} else {
						$info->aniob = floatval($dataVentaB[0]->ventas);
					}
					break;
				case 9:
					$info->nmes = "Septiembre";
					if (count($dataVentaA) <= 0) {
						$info->anioa = 0;
					} else {
						$info->anioa = floatval($dataVentaA[0]->ventas);
					}
					if (count($dataVentaB) <= 0) {
						$info->aniob = 0;
					} else {
						$info->aniob = floatval($dataVentaB[0]->ventas);
					}
					break;
				case 10:
					$info->nmes = "Octubre";
					if (count($dataVentaA) <= 0) {
						$info->anioa = 0;
					} else {
						$info->anioa = floatval($dataVentaA[0]->ventas);
					}
					if (count($dataVentaB) <= 0) {
						$info->aniob = 0;
					} else {
						$info->aniob = floatval($dataVentaB[0]->ventas);
					}
					break;
				case 11:
					$info->nmes = "Nomviembre";
					if (count($dataVentaA) <= 0) {
						$info->anioa = 0;
					} else {
						$info->anioa = floatval($dataVentaA[0]->ventas);
					}
					if (count($dataVentaB) <= 0) {
						$info->aniob = 0;
					} else {
						$info->aniob = floatval($dataVentaB[0]->ventas);
					}
					break;
				case 12:
					$info->nmes = "Diciembre";
					if (count($dataVentaA) <= 0) {
						$info->anioa = 0;
					} else {
						$info->anioa = floatval($dataVentaA[0]->ventas);
					}
					if (count($dataVentaB) <= 0) {
						$info->aniob = 0;
					} else {
						$info->aniob = floatval($dataVentaB[0]->ventas);
					}
					break;
			}
			$dataMatch[] = $info;
		}

		echo json_encode($dataMatch);

	}

	public function insert()
	{
		$dataResponse = $this->input->post();
		$dataHistorial = $this->HistorialventasModel->get_where(array("anio" => $dataResponse['anio'], "mes" => $dataResponse['mes']));
		if (count($dataHistorial) <= 0) {
			echo $this->HistorialventasModel->insert($dataResponse);
		} else {
			$this->HistorialventasModel->edit($dataHistorial[0]->idhistorial, array("ventas" => $dataResponse['ventas']));
			echo 1;
		}

	}

	public function insert_venta_sistema()
	{
		$data = $this->input->post();
		$fecha = explode("-", $data['fechaganado']);
		$anio = $fecha[0];
		$mes = $fecha[1];
		$ventasistema = $data['costo'];
		$dataHistorial = $this->HistorialventasModel->get_where(array("anio" => $anio, "mes" => $mes));
		if (count($dataHistorial) > 0) {
			#si existe y solo hacemos update en ventas_sistema
			$ventasistema += $dataHistorial[0]->ventas_sistema;
			$this->HistorialventasModel->edit($dataHistorial[0]->idhistorial, array("ventas_sistema" => $ventasistema));
		} else {
			switch ($mes) {
				case 1:
					$nmes = "Enero";
					$mes = 1;
					break;
				case 2:
					$nmes = "Febrero";
					$mes = 2;
					break;
				case 3:
					$nmes = "Marzo";
					$mes = 3;
					break;
				case 4:
					$nmes = "Abril";
					$mes = 4;
					break;
				case 5:
					$nmes = "Mayo";
					$mes = 5;
					break;
				case 6:
					$nmes = "Junio";
					$mes = 6;
					break;
				case 7:
					$nmes = "Julio";
					$mes = 7;
					break;
				case 8:
					$nmes = "Agosto";
					$mes = 8;
					break;
				case 9:
					$nmes = "Septiembre";
					$mes = 9;
					break;
				case 10:
					$nmes = "Octubre";
					$mes = 10;
					break;
				case 11:
					$nmes = "Nomviembre";
					$mes = 11;
					break;
				case 12:
					$nmes = "Diciembre";
					$mes = 12;
					break;
			}
			$this->HistorialventasModel->insert(array("anio" => $anio, "mes" => $mes, "ventas_sistema" => $ventasistema, "nmes" => $nmes));
		}
	}

	public function get_where()
	{
		$dataWhere = $this->input->post();
		$dataHistorial = $this->HistorialventasModel->get_where($dataWhere);
		if (count($dataHistorial) > 0) {
			$respuesta = array(
				"historial" => $dataHistorial[0],
				"respuesta" => 1
			);
		} else {
			$respuesta = array(
				"historial" => null,
				"respuesta" => 0
			);
		}

		echo json_encode($respuesta);
	}

	public function get_historial_table($anio, $call = 0)
	{
		$dataAnio = array();
		for ($i = 1; $i <= 12; $i++) {
			$dataVenta = $this->HistorialventasModel->get_where(array("anio" => $anio, "mes" => $i));
			$dataVentaPasado = $this->HistorialventasModel->get_where(array("anio" => $anio - 1, "mes" => $i));
			$info = new \stdClass;
			switch ($i) {
				case 1:
					$info->nmes = "Enero";
					break;
				case 2:
					$info->nmes = "Febrero";
					break;
				case 3:
					$info->nmes = "Marzo";
					break;
				case 4:
					$info->nmes = "Abril";
					break;
				case 5:
					$info->nmes = "Mayo";
					break;
				case 6:
					$info->nmes = "Junio";
					break;
				case 7:
					$info->nmes = "Julio";
					break;
				case 8:
					$info->nmes = "Agosto";
					break;
				case 9:
					$info->nmes = "Septiembre";
					break;
				case 10:
					$info->nmes = "Octubre";
					break;
				case 11:
					$info->nmes = "Nomviembre";
					break;
				case 12:
					$info->nmes = "Diciembre";
					break;
			}
			if (count($dataVenta) <= 0 && count($dataVentaPasado) <= 0) {
				$info->anio = $anio;
				$info->ventas = 0;
				$info->ventas_pasado = 0;
				$info->diferencia = 0;
				$dataAnio[] = $info;
			} elseif (count($dataVenta) > 0 && count($dataVentaPasado) > 0) {
				$info->anio = $anio;
				$info->ventas = $dataVenta[0]->ventas;
				$info->ventas_pasado = $dataVentaPasado[0]->ventas;
				$info->diferencia = $dataVenta[0]->ventas - $dataVentaPasado[0]->ventas;
				$dataAnio[] = $info;
			} elseif (count($dataVenta) > 0 && count($dataVentaPasado) <= 0) {
				$info->anio = $anio;
				$info->ventas = $dataVenta[0]->ventas;
				$info->ventas_pasado = 0;
				$info->diferencia = $dataVenta[0]->ventas - 0;
				$dataAnio[] = $info;
			} elseif (count($dataVenta) <= 0 && count($dataVentaPasado) > 0) {
				$info->anio = $anio;
				$info->ventas = 0;
				$info->ventas_pasado = $dataVentaPasado[0]->ventas;
				$info->diferencia = 0 - $dataVentaPasado[0]->ventas;
				$dataAnio[] = $info;
			}
		}
		if ($call == 1) {
			return $dataAnio;
		} else {
			echo json_encode($dataAnio);
		}

	}

	public function index($anio)
	{
		$dataAnio = $this->get_historial_table($anio, 1);
		$data = array(
			"ventas" => $dataAnio
		);
		$this->load->view("lista_historialventas", $data);
	}


	//--------------------------------------------------------------------

}
