/*
Navicat MySQL Data Transfer

Source Server         : CreandoPixelesServidor
Source Server Version : 50733
Source Host           : 174.136.37.107:3306
Source Database       : creando4_miskuentas

Target Server Type    : MYSQL
Target Server Version : 50733
File Encoding         : 65001

Date: 2021-02-22 22:34:12
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for comentarios
-- ----------------------------
DROP TABLE IF EXISTS `comentarios`;
CREATE TABLE `comentarios` (
  `idcomentario` int(9) NOT NULL AUTO_INCREMENT,
  `comentario` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `idprospecto` int(9) NOT NULL,
  `asesor` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fecha` date DEFAULT NULL,
  `medioatencion` int(1) NOT NULL,
  PRIMARY KEY (`idcomentario`) USING BTREE,
  KEY `fk_comentarios_asesor` (`asesor`) USING BTREE,
  KEY `fk_comentarios_prospectos` (`idprospecto`) USING BTREE,
  CONSTRAINT `fk_comentarios_asesor` FOREIGN KEY (`asesor`) REFERENCES `usuarios` (`correo`) ON UPDATE CASCADE,
  CONSTRAINT `fk_comentarios_prospectos` FOREIGN KEY (`idprospecto`) REFERENCES `prospectos` (`idprospecto`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5537 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of comentarios
-- ----------------------------

-- ----------------------------
-- Table structure for control_asesores
-- ----------------------------
DROP TABLE IF EXISTS `control_asesores`;
CREATE TABLE `control_asesores` (
  `idcontrol` int(255) NOT NULL AUTO_INCREMENT,
  `idprospecto` int(9) NOT NULL,
  `asesor` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fecha` date DEFAULT NULL,
  PRIMARY KEY (`idcontrol`) USING BTREE,
  KEY `fk_control_prospecto` (`idprospecto`) USING BTREE,
  KEY `fk_control_usuario` (`asesor`) USING BTREE,
  CONSTRAINT `fk_control_prospecto` FOREIGN KEY (`idprospecto`) REFERENCES `prospectos` (`idprospecto`) ON UPDATE CASCADE,
  CONSTRAINT `fk_control_usuario` FOREIGN KEY (`asesor`) REFERENCES `usuarios` (`correo`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2339 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of control_asesores
-- ----------------------------

-- ----------------------------
-- Table structure for correos
-- ----------------------------
DROP TABLE IF EXISTS `correos`;
CREATE TABLE `correos` (
  `idcorreo` int(10) NOT NULL AUTO_INCREMENT,
  `correo` varchar(1200) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) NOT NULL,
  `asunto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idcorreo`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of correos
-- ----------------------------

-- ----------------------------
-- Table structure for historial
-- ----------------------------
DROP TABLE IF EXISTS `historial`;
CREATE TABLE `historial` (
  `idhistorial` int(255) NOT NULL AUTO_INCREMENT,
  `movimiento` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `correo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  PRIMARY KEY (`idhistorial`) USING BTREE,
  KEY `fk_historial_usuarios` (`correo`) USING BTREE,
  CONSTRAINT `fk_historial_usuarios` FOREIGN KEY (`correo`) REFERENCES `usuarios` (`correo`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11616 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of historial
-- ----------------------------
INSERT INTO `historial` VALUES ('11613', 'Ingreso al sistea desde la ip 201.130.5.175', 'marcabe', '2021-02-22', '18:15:00');
INSERT INTO `historial` VALUES ('11614', 'Salio del sistea', 'marcabe', '2021-02-22', '18:15:00');
INSERT INTO `historial` VALUES ('11615', 'Ingreso al sistea desde la ip 201.130.5.175', 'marcabe', '2021-02-22', '18:26:00');

-- ----------------------------
-- Table structure for historial_ventas
-- ----------------------------
DROP TABLE IF EXISTS `historial_ventas`;
CREATE TABLE `historial_ventas` (
  `idhistorial` int(10) NOT NULL AUTO_INCREMENT,
  `anio` int(4) NOT NULL,
  `mes` varchar(2) NOT NULL,
  `nmes` varchar(10) NOT NULL,
  `ventas` decimal(10,2) NOT NULL,
  `ventas_sistema` decimal(10,2) NOT NULL,
  PRIMARY KEY (`idhistorial`)
) ENGINE=InnoDB AUTO_INCREMENT=174 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of historial_ventas
-- ----------------------------

-- ----------------------------
-- Table structure for historial_ventas_asesor
-- ----------------------------
DROP TABLE IF EXISTS `historial_ventas_asesor`;
CREATE TABLE `historial_ventas_asesor` (
  `idhistorial` int(10) NOT NULL AUTO_INCREMENT,
  `anio` int(4) NOT NULL,
  `mes` varchar(2) NOT NULL,
  `nmes` varchar(10) NOT NULL,
  `ventas` decimal(10,2) NOT NULL,
  `asesor` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idhistorial`),
  KEY `fk_ventas_asesor` (`asesor`),
  CONSTRAINT `fk_ventas_asesor` FOREIGN KEY (`asesor`) REFERENCES `usuarios` (`correo`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of historial_ventas_asesor
-- ----------------------------

-- ----------------------------
-- Table structure for img_correo
-- ----------------------------
DROP TABLE IF EXISTS `img_correo`;
CREATE TABLE `img_correo` (
  `idimg` int(10) NOT NULL AUTO_INCREMENT,
  `ruta` varchar(2500) NOT NULL,
  `idcorreo` int(10) NOT NULL,
  PRIMARY KEY (`idimg`),
  KEY `fk_images_correos` (`idcorreo`),
  CONSTRAINT `fk_images_correos` FOREIGN KEY (`idcorreo`) REFERENCES `correos` (`idcorreo`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of img_correo
-- ----------------------------

-- ----------------------------
-- Table structure for productos
-- ----------------------------
DROP TABLE IF EXISTS `productos`;
CREATE TABLE `productos` (
  `idproducto` int(9) NOT NULL AUTO_INCREMENT,
  `nproducto` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`idproducto`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of productos
-- ----------------------------

-- ----------------------------
-- Table structure for prospectos
-- ----------------------------
DROP TABLE IF EXISTS `prospectos`;
CREATE TABLE `prospectos` (
  `idprospecto` int(9) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `correo` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `idproducto` int(9) NOT NULL,
  `pruebai` date NOT NULL,
  `pruebaf` date NOT NULL,
  `asesor` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) NOT NULL,
  `fechacreate` date DEFAULT NULL,
  `fechaalerta` date DEFAULT NULL,
  `fechaganado` date DEFAULT NULL,
  `costo` decimal(10,2) DEFAULT NULL,
  `motivo` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fechaperdido` date DEFAULT NULL,
  `horaalerta` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idsms` int(10) DEFAULT NULL,
  `idcorreo` int(10) DEFAULT NULL,
  `smsenviado` int(1) NOT NULL,
  `correoenviado` int(1) NOT NULL,
  PRIMARY KEY (`idprospecto`) USING BTREE,
  KEY `fk_prospecto_aseor` (`asesor`) USING BTREE,
  KEY `fk_prospecto_producto` (`idproducto`) USING BTREE,
  KEY `fk_prospecto_sms` (`idsms`),
  KEY `fk_prospecto_correo` (`idcorreo`),
  CONSTRAINT `fk_prospecto_aseor` FOREIGN KEY (`asesor`) REFERENCES `usuarios` (`correo`) ON UPDATE CASCADE,
  CONSTRAINT `fk_prospecto_correo` FOREIGN KEY (`idcorreo`) REFERENCES `correos` (`idcorreo`) ON UPDATE CASCADE,
  CONSTRAINT `fk_prospecto_producto` FOREIGN KEY (`idproducto`) REFERENCES `productos` (`idproducto`) ON UPDATE CASCADE,
  CONSTRAINT `fk_prospecto_sms` FOREIGN KEY (`idsms`) REFERENCES `sms` (`idsms`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2316 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of prospectos
-- ----------------------------

-- ----------------------------
-- Table structure for registros_correo
-- ----------------------------
DROP TABLE IF EXISTS `registros_correo`;
CREATE TABLE `registros_correo` (
  `idregistro` int(10) NOT NULL AUTO_INCREMENT,
  `idprospecto` int(10) NOT NULL,
  `idcorreo` int(10) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `statusenvio` varchar(250) NOT NULL,
  PRIMARY KEY (`idregistro`),
  KEY `fk_resgistros_prospectos` (`idprospecto`),
  KEY `fk_registros_sms` (`idcorreo`),
  CONSTRAINT `registros_correo_ibfk_1` FOREIGN KEY (`idcorreo`) REFERENCES `correos` (`idcorreo`) ON UPDATE CASCADE,
  CONSTRAINT `registros_correo_ibfk_2` FOREIGN KEY (`idprospecto`) REFERENCES `prospectos` (`idprospecto`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=218418 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of registros_correo
-- ----------------------------

-- ----------------------------
-- Table structure for registros_sms
-- ----------------------------
DROP TABLE IF EXISTS `registros_sms`;
CREATE TABLE `registros_sms` (
  `idregistro` int(10) NOT NULL AUTO_INCREMENT,
  `idprospecto` int(10) NOT NULL,
  `idsms` int(10) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `statusenvio` varchar(250) NOT NULL,
  PRIMARY KEY (`idregistro`),
  KEY `fk_resgistros_prospectos` (`idprospecto`),
  KEY `fk_registros_sms` (`idsms`),
  CONSTRAINT `fk_registros_sms` FOREIGN KEY (`idsms`) REFERENCES `sms` (`idsms`) ON UPDATE CASCADE,
  CONSTRAINT `fk_resgistros_prospectos` FOREIGN KEY (`idprospecto`) REFERENCES `prospectos` (`idprospecto`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=359356 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of registros_sms
-- ----------------------------

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `idrol` int(9) NOT NULL AUTO_INCREMENT,
  `rol` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idrol`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('1', 'Administrador');
INSERT INTO `roles` VALUES ('2', 'Asesor');

-- ----------------------------
-- Table structure for sms
-- ----------------------------
DROP TABLE IF EXISTS `sms`;
CREATE TABLE `sms` (
  `idsms` int(10) NOT NULL AUTO_INCREMENT,
  `sms` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`idsms`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sms
-- ----------------------------

-- ----------------------------
-- Table structure for status_prospectos
-- ----------------------------
DROP TABLE IF EXISTS `status_prospectos`;
CREATE TABLE `status_prospectos` (
  `idstatus` int(255) NOT NULL AUTO_INCREMENT,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idstatus`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of status_prospectos
-- ----------------------------

-- ----------------------------
-- Table structure for usuarios
-- ----------------------------
DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `correo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `clave` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `idrol` int(9) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`correo`) USING BTREE,
  KEY `fk_usuario_rol` (`idrol`) USING BTREE,
  CONSTRAINT `fk_usuario_rol` FOREIGN KEY (`idrol`) REFERENCES `roles` (`idrol`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of usuarios
-- ----------------------------
INSERT INTO `usuarios` VALUES ('marcabe', 'Marco Cardona', '530a797d5ac3b3d28c1202a2eee36425', '1', '1');

-- ----------------------------
-- Table structure for ventas
-- ----------------------------
DROP TABLE IF EXISTS `ventas`;
CREATE TABLE `ventas` (
  `idventa` int(10) NOT NULL AUTO_INCREMENT,
  `idprospecto` int(10) NOT NULL,
  `idproducto` int(10) NOT NULL,
  `fecha` date NOT NULL,
  `costo` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`idventa`)
) ENGINE=InnoDB AUTO_INCREMENT=241 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Records of ventas
-- ----------------------------
